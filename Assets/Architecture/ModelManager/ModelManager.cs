﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Architecture.ModelManager
{
    public class ModelManager : MonoBehaviour
    {
        public List<AModel> ListModels;

        [SerializeField]
        private ControllerManager.ControllerManager controllerManager;
        
        
        public void Collect()
        {
            ListModels = FindObjectsOfType<AModel>().ToList(); 
        }

        public void Initialize()
        {
            foreach (var aModel in ListModels)
            {
                aModel.Initialize();
            }
        }

        public void Inject()
        {
            foreach (var controller in controllerManager.Controllers)
            {
                var controllerType = controller.GetType();
                var interfaces = controllerType.GetInterfaces();

                if(interfaces.Length == 0)
                    continue;

                foreach (var interfaceObject in interfaces)
                {
                    if (interfaceObject.IsGenericType)
                    {
                        var providerType = interfaceObject.GetGenericTypeDefinition();
                        if(providerType != typeof(IModelProvider<>))
                            return;
                        
                        var args = interfaceObject.GetGenericArguments();
                        var modelType = args[0];

                        var model = ListModels.Single(x => x.GetType() == modelType);
                        var property = interfaceObject.GetMethod("SetModel");
                        property.Invoke(controller,new []{model});
                    }
                }
            }
        }

        public void Subscribe()
        {
            foreach (var aModel in ListModels)
            {
                aModel.Subscribe();
            }
        }

        public void Unsubscribe()
        {
            foreach (var aModel in ListModels)
            {
                aModel.Unsubscribe();
            }
        }
    }
}