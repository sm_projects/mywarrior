﻿namespace Architecture.ModelManager
{
    
    public interface IModelProvider<T> where T: AModel
    {
        public void SetModel(T model);
    }
}