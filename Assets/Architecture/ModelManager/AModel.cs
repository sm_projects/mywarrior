﻿using UnityEngine;

namespace Architecture.ModelManager
{
    public class AModel : MonoBehaviour
    {
        public virtual void  Initialize()
        {
            
        }
        
        public virtual void  Subscribe()
        {
            
        }
        
        public virtual void  Unsubscribe()
        {
            
        }
    }
}