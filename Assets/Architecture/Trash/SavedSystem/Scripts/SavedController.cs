﻿using Architecture.ControllerManager;
using Architecture.MCOV;
using UnityEngine;

namespace Architecture.SavedSystem
{
    public class SavedController : AController
    {


        



        [ContextMenu("ClearSaved")]
        private void ClearSaved()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}