﻿using System;
using UnityEngine;

namespace Architecture.MCOV
{
    [Obsolete]
    public class AModelOld : MonoBehaviour
    {


        private string GenerateObjectKey<T>(int idHero) => $"{typeof(T).Name}_Object_{idHero}";
        
        private string GenerateObjectKey(int idHero, Type type) => $"{type.Name}_Object_{idHero}";
        
        private string GeneratePlayerKey<T>() => $"{typeof(T).Name}_Player_{0}";

        private string GeneratePlayerKey(Type type) => $"{type.Name}_Player_{0}";
        
        #region Save

        protected void SaveObjectInfo<T>(int idHero, T data)
        {
            SaveData(GenerateObjectKey<T>(idHero), data);
        }
        
        protected void SavePlayerInfo<T>(T data)
        {
            SaveData(GeneratePlayerKey<T>(), data);
        }
        
        private void SaveData<T>(string key, T data)
        {
            string json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(key, json);
        }

        #endregion


        #region Load

        protected T LoadObjectInfo<T>(int idHero)where T: class
        {
            return LoadData<T>(GenerateObjectKey<T>(idHero));
        }

        
        
        protected T LoadPlayerInfo<T>()where T: class
        {
            return LoadData<T>(GeneratePlayerKey<T>());
        }
        
        private T LoadData<T>(string key) where T: class
        {
            string json = PlayerPrefs.GetString(key, "");
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            T result = JsonUtility.FromJson<T>(json);
            return result;
            
        }

        #endregion


        #region Remove

        protected void RemoveObjectData(int id, Type type)
        {
            PlayerPrefs.DeleteKey(GenerateObjectKey(id, type));
        }
        
        protected void RemovePlayerData(Type type)
        {
            PlayerPrefs.DeleteKey(GeneratePlayerKey(type));
        }

        #endregion
       
        
        
        
    }
}