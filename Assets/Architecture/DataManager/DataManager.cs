﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Architecture.DataManager
{
    public class DataManager : MonoBehaviour
    {
        public static DataManager Manager;

        [SerializeField]
        private ModelManager.ModelManager ModelManager;
        
        [SerializeReference]
        private List<object> listAData = new List<object> ();

        private List<DataProvider> _providersMono = new List<DataProvider>();

        
        [SerializeField]
        private List<WorldData> listWorldData = new List<WorldData>();

        [SerializeField]
        private WorldData currentWorld = new WorldData();
        
        private string PathDataManager => Application.dataPath + "/DataManager";

        private string PathWorld(string id) => $"{Application.dataPath}/DataManager/{id}.txt";
        
        
        private void OnApplicationQuit()
        {
            SaveCurrentWorld();
        }

        public async void SaveCurrentWorld()
        {
            if (string.IsNullOrEmpty(currentWorld.WorldId))
                return;

            foreach (var provider in _providersMono)
            {
                provider.SaveData();
            }


            var pathWorld = PathWorld(currentWorld.WorldId);

            var isHaveWorld = Directory.Exists(pathWorld);
            if (!isHaveWorld)
            {
                var a = File.Create(pathWorld);
                a.Dispose();
            }

            using (StreamWriter writer = new StreamWriter(pathWorld))
            {
                var text = JsonUtility.ToJson(currentWorld);
                await writer.WriteLineAsync(text);
            }


        }

        public List<WorldData> GetListWorlds()
        {
            List<WorldData> result = new List<WorldData>();
            
            foreach (var world in listWorldData)
            {
                if (!string.IsNullOrEmpty(world.WorldId))
                {
                    result.Add(world);
                }
            }

            return result;
        }
        
        public void RemoveWorld(string worldId)
        {
            foreach (var world in listWorldData)
            {
                if (world.WorldId == worldId)
                {
                    listWorldData.Remove(world);
                    break;
                }
            }

            var pathWorld = PathWorld(worldId); 
            File.Delete(pathWorld);
            currentWorld = new WorldData();
            currentWorld.WorldSave = new List<DataSave>();
        }

        public  WorldData CreateNewWorld(string nameWorld)
        {
            var path = Application.dataPath + "/DataManager";

            var isHaveSave = Directory.Exists(path);
            if (!isHaveSave)
            {
                Directory.CreateDirectory(path);
            }

            WorldData newWorld = new WorldData();

            
            var worldId = $"{nameWorld}_{DateTime.Now}";//SystemInfo.deviceUniqueIdentifier
            
            worldId = worldId.Replace('.', '_');
            worldId = worldId.Replace(',', '_');
            worldId = worldId.Replace(';', '_');
            worldId = worldId.Replace('\\', '_');
            worldId = worldId.Replace(' ', '_');
            worldId = worldId.Replace(':', '_');
            worldId = worldId.Replace('/', '_');
            
            var pathWorld = $"{path}/{worldId}.txt";
            var a = File.Create(pathWorld);
            a.Dispose();
            
            
            newWorld.WorldId = worldId;
            newWorld.WorldSave = new List<DataSave>();
            listWorldData.Add(newWorld);
            return newWorld;
        }
        

        
        public WorldData GetSelectedWorld()
        {
            return currentWorld;
        }

        public void SelectedWorld(string worldId)
        {
            SaveCurrentWorld();
            var world = listWorldData.SingleOrDefault(x => x.WorldId == worldId);
            if (world != null)
            {
                currentWorld = world;
            }
        }

        public async Task Initialize()
        {
            Manager = this;
            currentWorld = new WorldData();
            currentWorld.WorldSave = new List<DataSave>();

            

            var isHaveFolder = Directory.Exists(PathDataManager);
            if (!isHaveFolder)
            {
                return;
            }
            
            var listSavePath = Directory.GetFiles(PathDataManager);
            if(listSavePath.Length==0)
                return;

            listWorldData = new List<WorldData>();

            foreach (var savePath in listSavePath)
            {

                if(savePath.Contains(".meta"))
                    continue;
                
                using (StreamReader reader = new StreamReader(savePath))
                {
                    var text = await reader.ReadToEndAsync();
                    var listData = JsonUtility.FromJson<WorldData>(text);

                    listWorldData.Add(listData);
                }

                
            }

            currentWorld = listWorldData.First();


        }
        
        public void Inject()
        {
            //listAData = new List<object>();
            _providersMono = new List<DataProvider>();
            
            foreach (var aModel in ModelManager.ListModels)
            {
                var modelType = aModel.GetType();
                var interfaces = modelType.GetInterfaces();

                if(interfaces.Length == 0)
                    continue;

                foreach (var interfaceObject in interfaces)
                {
                    if (interfaceObject.IsGenericType)
                    {
                        var providerType = interfaceObject.GetGenericTypeDefinition();
                        if(providerType != typeof(IDataProvider<>))
                            return;
                        
                        var args = interfaceObject.GetGenericArguments();
                        var dataType = args[0];

                        var provider = _providersMono.SingleOrDefault(x => x.DataType == dataType);
                        if (provider == null)
                        {
                            Type classType = typeof(DataProvider<>);
                            Type[] typeParams = new Type[] { dataType };   
                            Type constructedType = classType.MakeGenericType(typeParams);
                            provider = (DataProvider) Activator.CreateInstance(constructedType); 
                            provider.SetDataType(dataType);
                            _providersMono.Add(provider);
                        }

                        var setProviderMethod = interfaceObject.GetMethod("SetDataProvider");
                        setProviderMethod.Invoke(aModel, new[] { provider });
                        
                        var dataObject = listAData.SingleOrDefault(x => x.GetType() == dataType);
                        if (dataObject == null)
                        {
                            var providerObjectType = provider.GetType();
                            var method = providerObjectType.GetMethod("LoadData");
                            dataObject =  method.Invoke(provider, null);
                            listAData.Add(dataObject); 
                        }
                        

                    }
                }
            }
        }

        public T GetData<T>() where T: AData
        {
            var typeData = typeof(T);
            var data = listAData.Single(x => x.GetType() == typeData);
            return (T)data;
        }
        
        public T LoadData<T>() where T: AData, new()
        {
            var nameType = typeof(T).FullName;
            var dataSave = currentWorld.WorldSave.SingleOrDefault(x => x.NameType == nameType);
            if (dataSave == null)
            {
                var data = new T();
                data.SetNameData(nameType);
                return data;
                
            }

            var json = dataSave.Json;
            if (string.IsNullOrEmpty(json))
            {
                var data = new T();
                data.SetNameData(nameType);
                return data;
            }

            T result = JsonUtility.FromJson<T>(json);
            
            return result;
        }
        
        public void SaveData<T>(T data)
        {
            string json = JsonUtility.ToJson(data);
            
            var dataSave = currentWorld.WorldSave.SingleOrDefault(x => x.NameType == typeof(T).FullName);
            if (dataSave == null)
            {
                dataSave = new DataSave();
                dataSave.NameType = typeof(T).FullName;
                dataSave.Json = json;
                currentWorld.WorldSave.Add(dataSave);
            }
            else
            {
                dataSave.Json = json;
            }

        }

    }

    [Serializable]
    public class WorldData
    {
        public string WorldId;
        public List<DataSave> WorldSave;
    }

    [Serializable]
    public class DataSave
    {
        public string NameType;
        public string Json;
    }

}