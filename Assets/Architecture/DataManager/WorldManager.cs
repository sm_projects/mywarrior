﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Architecture.DataManager
{
    public class WorldManager : MonoBehaviour
    {
        public static WorldManager Manager;

        public event Action OnChangeWorlds;
        public event Action OnChangeSelectedWorld;
        
        [SerializeField]
        private DataManager dataManager;

        private void Awake()
        {
            Manager = this;
        }

        public void RemoveWorld(string worldName)
        {
            dataManager.RemoveWorld(worldName);
            OnChangeWorlds?.Invoke();
        }

        public WorldData CreateNewWorld(string nameWorld)
        {
            var result = dataManager.CreateNewWorld(nameWorld);
            OnChangeWorlds?.Invoke();

            return result;
        }
        
        public WorldData GetSelectedWorld()
        {
            return dataManager.GetSelectedWorld();
        }

        public void SelectedWorld(string idWorld)
        {
            dataManager.SelectedWorld(idWorld);
            OnChangeSelectedWorld?.Invoke();
        }

        public List<WorldData> GetListWorlds()
        {
            return dataManager.GetListWorlds();
        }
    }
}