﻿
namespace Architecture.DataManager
{
    public interface IDataProvider<T>  where T : AData, new()
    {
        void SetDataProvider(DataProvider<T> provider);
    }
}