﻿using System;
using Architecture.ModelManager;

namespace Architecture.DataManager
{
    [Serializable]
    public class DataProvider
    {
        public Type DataType => _dataType;

        private Type _dataType;
        
        public void SetDataType(Type dataType)
        {
            _dataType = dataType;
        }

        public virtual void SaveData()
        {
            
        }
    }

    [Serializable]
    public class DataProvider<T> : DataProvider  where T: AData, new()
    {
        public event Action<T> OnChange;

        public T GetData => Architecture.DataManager.DataManager.Manager.GetData<T>();

        
        public override void SaveData()
        {
            DataManager.Manager.SaveData(GetData);
        }
        
        public T LoadData()
        {
            return DataManager.Manager.LoadData<T>();
        }

        public void Change() 
        {
            OnChange?.Invoke(GetData);
        }

        

    }


    [Serializable]
    public class Data1: AData
    {
        public int Count;
    }
    
}