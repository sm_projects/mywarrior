﻿using UnityEngine;

namespace Architecture
{
    public class ArchitectureInitializer : MonoBehaviour
    {
        public AssetsManager.AssetsManager assetsManager;


        public DataManager.DataManager dataManager;


        public ModelManager.ModelManager modelManager;

       
        public ControllerManager.ControllerManager controllerManager;

       
        public ObjectManager.ObjectManager objectManager;
        
        [ContextMenu("Collect")]
        private void Collect()
        {
            controllerManager.Collect();
            modelManager.Collect();
            assetsManager.Collect();
        }

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
            
           
             Initialize();
        }

        private async void Initialize()
        {
            objectManager.Initialize();
            assetsManager.Initialize();
            
            
            await dataManager.Initialize();
            dataManager.Inject();
                    
            
            modelManager.Initialize();
            modelManager.Inject();
                    
            
            controllerManager.Initialize();
                
            

            controllerManager.ArchitectureInitialized();
            
            modelManager.Subscribe();
            controllerManager.Subscribe(); 
        }
        

        private void OnDisable()
        {
            controllerManager.Unsubscribe();
            modelManager.Unsubscribe();
            
        }
    }
}