﻿using Architecture.ControllerManager;
using UnityEngine;

namespace Architecture.ObjectManager
{
    public class ObjectManager : MonoBehaviour
    {
        public static ObjectManager Manager;

        [SerializeField]
        private ControllerManager.ControllerManager controllerManager;

        public void Initialize()
        {
            Manager = this;
        }
        
        

        public void InitializeObject<T>(AObject<T> aObject) where T : AController
        {
            foreach (var controller in controllerManager.Controllers)
            {
                if (controller is T controllerResult)
                {
                    aObject.Controller = controllerResult;
                    aObject.Initialize();
                    aObject.Subscribe();
                }
            }
        }
    }
}