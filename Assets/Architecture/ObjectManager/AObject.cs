﻿using System;
using Architecture.AssetsManager;
using Architecture.ControllerManager;
using UnityEngine;

namespace Architecture.ObjectManager
{
    public class AObject<T> : MonoBehaviour where T : AController
    {
        private ObjectManager ObjectManager => ObjectManager.Manager;

        private AssetsManager.AssetsManager AssetsManager => Architecture.AssetsManager.AssetsManager.Instance;

        public T Controller { get; set; }

        private void Start()
        {
            Inject();
        }

        private async void Inject()
        {
            ObjectManager.InitializeObject<T>(this);
        }

        private void OnDisable()
        {
            Unsubscribe();
        }


        public virtual void Initialize()
        {
        }
        
        

        public virtual void Subscribe()
        {
            
        }
        
        public virtual void Unsubscribe()
        {
            
        }

        protected TViewAsset GetViewAsset<TViewAsset>() where TViewAsset : AViewAsset
        {
            return (TViewAsset)AssetsManager.GetViewAssets<TViewAsset>();
        }
        
    }
}