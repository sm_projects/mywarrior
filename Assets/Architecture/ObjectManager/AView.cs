﻿using UnityEngine;

namespace Architecture.ObjectManager
{
    public class AView : MonoBehaviour
    {
        protected virtual void OnEnable()
        {
            Subscribe();
        }
        
        protected virtual void OnDisable()
        {
            Unsubscribe();
        }

        protected virtual void Subscribe()
        {
            
        }
        
        protected virtual void Unsubscribe()
        {
            
        }
    }
}