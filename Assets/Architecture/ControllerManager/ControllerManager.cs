﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Architecture.ControllerManager
{
    public class ControllerManager : MonoBehaviour
    {
        public static ControllerManager Manager;
        
        
        public List<AController> Controllers;
        


        public void Collect()
        {
            Controllers = FindObjectsOfType<AController>().ToList(); 
        }
        
        

        
        public void Initialize()
        {
            Manager = this;

            foreach (var controller in Controllers)
            {
                controller.Initialize();
            }
        }

        public void Subscribe()
        {
            foreach (var controller in Controllers)
            {
                controller.Subscribe();
            }
        }

        public void Unsubscribe()
        {
            foreach (var controller in Controllers)
            {
                controller.Unsubscribe();
            } 
        }


        public void ArchitectureInitialized()
        {
            foreach (var controller in Controllers)
            {
                controller.ArchitectureInitialized();
            } 
        }
    }
}


