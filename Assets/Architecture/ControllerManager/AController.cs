﻿using Architecture.AssetsManager;
using UnityEngine;

namespace Architecture.ControllerManager
{
    public abstract class AController : MonoBehaviour
    {

        public virtual void Initialize()
        {
            Subscribe();
        }
        


        public virtual void Subscribe()
        {
            
        }
        
        public virtual void Unsubscribe()
        {
            
        }
        

        protected T GetConfig<T>() where T : AConfigAsset
        { 
            return (T)AssetsManager.AssetsManager.Instance.GetConfigAssets<T>();
        }

        public virtual void ArchitectureInitialized()
        {
            
        }
    }
}