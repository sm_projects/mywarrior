﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Architecture.AssetsManager
{
    public class AssetsManager : MonoBehaviour
    {
        public static AssetsManager Instance;
        
        [SerializeField] private List<AViewAsset> listViewAsset;
        [SerializeField] private List<AConfigAsset> listConfigAsset;

        public void Initialize()
        {
            Instance = this;
        }
        
        public void Collect()
        {
            CollectAsset();
        }

        public T GetViewAssets<T>() where T : AViewAsset
        {
            var result = listViewAsset.SingleOrDefault(x => x.GetType() == typeof(T));
            return (T)result;
        }

        public T GetConfigAssets<T>() where T : AConfigAsset
        {
            var result = listConfigAsset.SingleOrDefault(x => x.GetType() == typeof(T));
            return (T)result;
        }
        
        private void CollectAsset()
        {
            listViewAsset = new List<AViewAsset>();
            var viewPath = AssetDatabase.FindAssets("t: AViewAsset");
            foreach (var guid in viewPath)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var view = AssetDatabase.LoadAssetAtPath(path, typeof(AViewAsset)) as AViewAsset;
               listViewAsset.Add(view);
               Debug.Log($"View: добавлен обьект {view.name}");
            }
            
            listConfigAsset = new List<AConfigAsset>();
            var configPath = AssetDatabase.FindAssets("t: AConfigAsset");
            foreach (var guid in configPath)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                var config = AssetDatabase.LoadAssetAtPath(path, typeof(AConfigAsset)) as AConfigAsset;
                listConfigAsset.Add(config);
                Debug.Log($"config: добавлен обьект {config.name}");
            }
        }
    }
}