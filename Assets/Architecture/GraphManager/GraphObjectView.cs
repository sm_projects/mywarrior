﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Architecture.GraphManager
{
    public class GraphObjectView : MonoBehaviour
    {

        [SerializeField]
        private Text text;

        [SerializeField]
        private Image bg;

        [SerializeField]
        private LineRenderer lineRenderer;

        [SerializeField]
        private List<GraphObjectView> _listChild = new List<GraphObjectView>();
        

        private GraphController _controller;
        
        public void Initialize(string nameView, Color32 color32, GraphController controller)
        {
            _controller = controller;
            text.text = nameView;
            bg.color = color32;
            
            lineRenderer.positionCount = 0;
        }

        [ContextMenu("Selected")]
        public void Selected()
        {
            _controller.HideAll();

            SetActive(true, true);

            _controller.Group();
            Repaint();
        }

        public void SetActive(bool isActive, bool andRef = false)
        {
            gameObject.SetActive(isActive);

            if (andRef)
            {
                foreach (var view in _listChild)
                {
                    view.SetActive(isActive, true);
                }
            }
        }
        

        public void Repaint()
        {
            lineRenderer.positionCount = _listChild.Count*2;
            for (int i = 0; i < _listChild.Count *2; i++)
            {
                if (i % 2 == 0)
                {
                    lineRenderer.SetPosition(i, transform.position);
                }
                else
                {
                    lineRenderer.SetPosition(i, _listChild[i/2].transform.position);
                }
            }

            foreach (var view in _listChild)
            {
                view.Repaint();
            }
        }
        
        public void AddChild(GraphObjectView viewTransform)
        {
            _listChild.Add(viewTransform);
            //Repaint();
        }

       
    }
}