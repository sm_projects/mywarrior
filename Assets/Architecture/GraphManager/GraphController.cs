﻿using System;
using System.Collections.Generic;
using System.Linq;
using Architecture.DataManager;
using Architecture.ModelManager;
using UnityEngine;

namespace Architecture.GraphManager
{
    public class GraphController : MonoBehaviour
    {
        
        public ArchitectureInitializer ArchitectureInitializer;

        
        
        [SerializeField]
        private Transform content;

        [SerializeField]
        private GraphObjectView viewPrefab;
        
        [SerializeField]
        private float deltaX;
        
        [SerializeField]
        private float deltaY;
        
        
        [SerializeField]
        private Color32 colorController;
        
        [SerializeField]
        private Color32 colorMode;
        
        [SerializeField]
        private Color32 colorData;

        private Dictionary<Type, GraphObjectView> _controllersView = new Dictionary<Type, GraphObjectView>();

        private Dictionary<Type, GraphObjectView> _modelsView = new Dictionary<Type, GraphObjectView>();
        
        private Dictionary<Type, GraphObjectView> _dataView = new Dictionary<Type, GraphObjectView>();
        
        [ContextMenu("Repaint")]
        public void Repaint()
        {
            Clear();
            
            
            ArchitectureInitializer = FindObjectOfType<ArchitectureInitializer>();

            RepaintModel();
            RepaintControllers();
            RepaintData();

            foreach (var view in _dataView.Values)
            {
                view.Repaint();
            }




        }

        //после моделей
        private void RepaintControllers()
        {
            _controllersView.Clear();
            var controllersCount = ArchitectureInitializer.controllerManager.Controllers.Count / 2;

            for (int i = 0; i < ArchitectureInitializer.controllerManager.Controllers.Count; i++)
            {
                var controller = ArchitectureInitializer.controllerManager.Controllers[i];
                var view = Instantiate(viewPrefab, content);
                view.Initialize(controller.GetType().Name, colorController, this);
                
                var pos =  (Vector2)content.position;
                pos.y += deltaY;
                pos.x -= (controllersCount - i) * deltaX ;
                view.transform.position = pos;

                _controllersView.Add(controller.GetType(), view);
                
                var controllerType = controller.GetType();
                var interfaces = controllerType.GetInterfaces();

                if(interfaces.Length == 0)
                    continue;

                foreach (var interfaceObject in interfaces)
                {
                    if (interfaceObject.IsGenericType)
                    {
                        var providerType = interfaceObject.GetGenericTypeDefinition();
                        if(providerType != typeof(IModelProvider<>))
                            return;
                        
                        var args = interfaceObject.GetGenericArguments();
                        var modelType = args[0];

                        _modelsView[modelType].AddChild(view);
                    }
                }
                
            }
        }


        //первое
        private void RepaintModel()
        {
            _modelsView.Clear();
            
            var modelsCount = ArchitectureInitializer.modelManager.ListModels.Count / 2;

            for (int i = 0; i < ArchitectureInitializer.modelManager.ListModels.Count; i++)
            {
                var model = ArchitectureInitializer.modelManager.ListModels[i];
                var view = Instantiate(viewPrefab, content);
                view.Initialize(model.GetType().Name, colorMode, this);
                
                var pos = (Vector2)content.position;
                pos.x -= (modelsCount - i) * deltaX ;
                view.transform.position = pos;

                _modelsView.Add(model.GetType(), view);
            }
        }

        //после можелией
        private void RepaintData()
        {
            _dataView.Clear();
            foreach (var aModel in ArchitectureInitializer.modelManager.ListModels)
            {
                var modelType = aModel.GetType();
                var interfaces = modelType.GetInterfaces();

                if (interfaces.Length == 0)
                    continue;

                foreach (var interfaceObject in interfaces)
                {
                    if (interfaceObject.IsGenericType)
                    {
                        var providerType = interfaceObject.GetGenericTypeDefinition();
                        if (providerType != typeof(IDataProvider<>))
                            return;

                        var args = interfaceObject.GetGenericArguments();
                        var dataType = args[0];
                        
                        if (!_dataView.ContainsKey(dataType))
                        {
                            var view = Instantiate(viewPrefab, content);
                            view.Initialize(dataType.Name,colorData,this);
                            
                            _dataView.Add(dataType, view);
                        }
                        _dataView[dataType].AddChild(_modelsView[modelType]);
                        
                    }
                }
            }

            
            
            for (int i = 0; i < _dataView.Count; i++)
            {
                var view = _dataView.Values.ToList()[i];
                var pos = (Vector2)content.position;
                pos.y -= deltaY;
                pos.x -= (_dataView.Count/2 - i) * deltaX ;
                view.transform.position = pos;
            }
            
        }

        [ContextMenu("Clear")]
        public void Clear()
        {
            for (int i = 0; i < content.childCount; i++)
            {
                DestroyImmediate(content.GetChild(i).gameObject);
            }
        }

        public void HideAll()
        {
            foreach (var view in _modelsView.Values)
            {
                view.SetActive(false);
            }
            
            foreach (var view in _controllersView.Values)
            {
                view.SetActive(false);
            }
        }

        private void GroupData()
        {
            var active = _dataView.Values.Where(x => x.gameObject.activeSelf).ToList();
            var leng = active.Count / 2;
            for (int i = 0; i < active.Count; i++)
            {
                var view = active[i];
                
                var pos = (Vector2)content.position;
                pos.x -= (leng - i) * deltaX;
                pos.y -= deltaY;
                view.transform.position = pos;
            }
        }

        private void GroupModel()
        {
            var active = _modelsView.Values.Where(x => x.gameObject.activeSelf).ToList();

            var lengM = active.Count / 2;
            for (int i = 0; i < active.Count; i++)
            {
                var view = active[i];
                
                var pos =  (Vector2)content.position;
                pos.x -= (lengM - i) * deltaX ;
                view.transform.position = pos;
            }
        }

        private void GroupModelController()
        {
            var active = _controllersView.Values.Where(x => x.gameObject.activeSelf).ToList();

            var leng = active.Count / 2;
            for (int i = 0; i < active.Count; i++)
            {
                var view = active[i];
                
                var pos =  (Vector2)content.position;
                pos.x -= (leng - i) * deltaX ;
                pos.y += deltaY ;
                view.transform.position = pos;
            } 
        }

        public void Group()
        {
            GroupData();
            GroupModel();
            GroupModelController();




        }
    }
}