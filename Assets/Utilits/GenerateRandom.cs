﻿using System;
using System.Collections.Generic;
namespace Utilits
{
    public class GenerateRandom
    {
        private static CountObject<T> GenerateObject<T>(ObjectChance<T>[] listCounts)
        {
            var rand = new Random();
            int sum = 0;
            foreach (var countChance in listCounts)
            {
                if(countChance != null)
                    sum = sum +  countChance.Weight;
            }

            int randomInt = rand.Next(sum);
            ObjectChance<T> currentObject = new ObjectChance<T>();
            foreach (var countChance in listCounts)
            {
                if (countChance != null)
                {
                    randomInt = randomInt - countChance.Weight;
                    if (randomInt <= 0)
                    {
                        currentObject = countChance;
                        break;
                    }
                }
            }

            var countObject = GenerateCount(currentObject.CountsChances);

            var result = new CountObject<T>();
            result.Count = countObject;
            result.Object = currentObject.Object;
            return result;
        }
        
        private static int GenerateCount(CountChance[] listCounts)
        {
            var rand = new Random();
            int sum = 0;
            foreach (var countChance in listCounts)
            {
                if(countChance != null)
                    sum = sum +  countChance.Weight;
            }

            int randomInt = rand.Next(sum);
            foreach (var countChance in listCounts)
            {
                if(countChance == null)
                    continue;
                
                randomInt = randomInt - countChance.Weight;
                if (randomInt <= 0)
                {
                    return countChance.Count;
                }
            }
            
            return 0;
        }

        public static CountObject<T>[] GenerateCountObject<T>(GenerateObject<T> gnerateObject)
        {
            if (gnerateObject == null)
                return null;

            if (gnerateObject.CountChance == null || gnerateObject.CountChance.Length == 0)
                return null;
            
            if (gnerateObject.ObjectChance == null || gnerateObject.ObjectChance.Length == 0)
                return null;
            
            
            var countResultObjects = GenerateCount(gnerateObject.CountChance);
            var result = new CountObject<T>[countResultObjects];
            var buffer = gnerateObject.ObjectChance;
            for (int i = 0; i < countResultObjects; i++)
            {
                var currentObjectCount = GenerateObject(buffer);

                var o2 = currentObjectCount.Object.GetHashCode();
                for (int j = 0; j < buffer.Length; j++)
                {
                    if(buffer[j] == null)
                        continue;
                    
                    var o1 = buffer[j].Object.GetHashCode();
                    if (o1 == o2)
                    {
                        buffer[j] = null;
                    }
                }

                result[i] = currentObjectCount;
            }

            return result;

        }
        
        
    }

    [Serializable]
    public class CountChance
    {
        public int Count;
        public int Weight = 100;
    }
    
    [Serializable]
    public class ObjectChance<T>
    {
        public T Object;
        public int Weight = 100;
        
        public CountChance[] CountsChances;
    }
    
    
    [Serializable]
    public class CountObject<T>
    {
        public T Object;
        public int Count;
    }

    [Serializable]
    public class GenerateObject<T>
    {
        public CountChance[] CountChance;
        public ObjectChance<T>[] ObjectChance;
    }
}