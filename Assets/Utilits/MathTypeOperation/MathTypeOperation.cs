﻿using System.Collections.Generic;

namespace Utilits
{
    public static class MathTypeOperation
    {
        public static float GetResult(float value1, MathType mathType, float value2)
        {
            switch (mathType)
            {
                case MathType.Multiply:
                {
                    return  value1 * value2;
                }
                case MathType.Add:
                {
                    return value1 + value2;
                }
                            
                case MathType.Subtraction:
                {
                    return value1 - value2;
                }
                            
                case MathType.Division:
                {
                    return value1 / value2;
                }
                            
                case MathType.SubtractionPercent:
                {
                    return value1 - value1 * value2;
                }
                            
                case MathType.AddPercent:
                {
                    return value1 + value1 * value2;
                }
            }

            return value1;

        }

        public static float GetResult(float value, List<MathTypePara> mathTypePara)
        {
            var result = value;
            foreach (var para in mathTypePara)
            {
                if (para.MathType == MathType.Add || para.MathType == MathType.Subtraction)
                {
                    result = GetResult(result, para.MathType, para.Value);
                }
            }
            
            foreach (var para in mathTypePara)
            {
                if (para.MathType == MathType.Multiply || para.MathType == MathType.Division)
                {
                    result = GetResult(result, para.MathType, para.Value);
                }
                
            }
            
            foreach (var para in mathTypePara)
            {
                if (para.MathType == MathType.AddPercent || para.MathType == MathType.SubtractionPercent)
                {
                    result = GetResult(result, para.MathType, para.Value);
                }
            }

            return result;
        }
        
        public static string GetChar(MathType mathType)
        {
            switch (mathType)
            {
                case MathType.Add:
                    return "+";
                case MathType.Division:
                    return "/";
                case MathType.Multiply:
                    return "*";
                case MathType.Subtraction:
                    return "-";
                case MathType.AddPercent:
                    return "+%";
                case MathType.SubtractionPercent:
                    return "-%";
                
            }

            return "";
        }
    }
}