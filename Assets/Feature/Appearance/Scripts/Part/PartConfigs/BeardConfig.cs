﻿using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Character/Appearance/Part/BeardConfig")]
    public class BeardConfig : PartConfig
    {
        [SerializeField]
        private GameObject beard;
        
        public GameObject Beard => beard;
    }
}