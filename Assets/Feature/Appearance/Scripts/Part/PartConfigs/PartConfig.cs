﻿using UnityEngine;

namespace DefaultNamespace
{
    public class PartConfig: ScriptableObject
    {
        [SerializeField]
        private string namePart;
        
        public string NamePart=>namePart;
    }
}