﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Character/Appearance/Part/SkinConfig")]
    public class SkinConfig : PartConfig
    {
        public GameObject Head;
        public GameObject  Body;
        public GameObject  HandL;
        public GameObject  HandR;
        public GameObject  Foot;

    }
    

}