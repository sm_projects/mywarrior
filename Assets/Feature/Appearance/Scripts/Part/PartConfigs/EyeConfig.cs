﻿using UnityEngine;
using UnityEngine.Serialization;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Character/Appearance/Part/EyeConfig")]
    public class EyeConfig: PartConfig
    {
        [SerializeField]
        private GameObject eye;
        
        public GameObject Eye => eye;
    }
}