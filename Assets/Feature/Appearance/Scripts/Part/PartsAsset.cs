﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Character/Appearance/PartsAsset")]
    public class PartsAsset : ScriptableObject
    {
        [SerializeField]
        private int defaultPart;

        public int DefaultPart => defaultPart;
        
        [SerializeField]
        private PartConfig[] parts;
        

        public T GetPart<T>(int idSkin) where T : PartConfig
        {
            if (parts[idSkin] == null)
                return null;
            return parts[idSkin] as T;
        }
        
        
        public int GetNextId(int idSkin)
        {

            idSkin++;
            if (idSkin == parts.Length)
            {
                idSkin = 0;
            }

            return idSkin;
        }
        
        public int GetBeforeId(int idSkin)
        {
            idSkin--;
            if (idSkin == -1)
            {
                idSkin = parts.Length -1;
            }

            return idSkin;
        }
    }
}