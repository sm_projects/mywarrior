﻿using System.Collections.Generic;
using Architecture.AssetsManager;
using UnityEngine;
using UnityEngine.Serialization;

namespace DefaultNamespace
{
    [CreateAssetMenu(menuName = "Character/Appearance/AllPartAssets")]
    public class AllPartAssets: AViewAsset
    {
        [SerializeField]
        private PartsAsset skinAsset;
        
        [SerializeField]
        private PartsAsset eyeAsset;
        
        [SerializeField]
        private PartsAsset beardsAsset;
        
        
        public PartsAsset SkinAsset => skinAsset;
        public PartsAsset EyeAsset => eyeAsset;
        public PartsAsset BeardsAsset => beardsAsset;
    }
}