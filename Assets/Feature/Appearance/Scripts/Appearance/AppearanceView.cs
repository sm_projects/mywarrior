﻿﻿using System;
using System.Collections.Generic;
 using Architecture.MCOV;
 using Architecture.ObjectManager;
 using DefaultNamespace.Equippable;
 using Feature.Items.Systems;
 using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
 using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
 using UnityEngine;

namespace DefaultNamespace.Appearance
{
    public class AppearanceView: AView
    {
        [SerializeField] private SkinSubView skinSubView;
        [SerializeField] private EquipmentSubView equipmentSubView;
        [SerializeField] private CrystalSubView crystalSubView;
        
        [SerializeField] private Animator animator;


        public void SetSkin(SkinConfig skinConfig)
        {
            skinSubView.RepaintSkin(skinConfig);
        }

        public void SetEye(EyeConfig eye)
        {
            skinSubView.RepaintEye(eye);
        }

        public void SetBeard(BeardConfig beard)
        {
            skinSubView.RepaintBeard(beard);
        }
        

        public void Equip(EquipmentType part, EquipmentViewConfig equipment, CrystalViewConfig crystalViewConfig)
        {
            equipmentSubView.Equip(part, equipment, crystalViewConfig);
            crystalSubView.Equip(part, crystalViewConfig);
        }


    }
}