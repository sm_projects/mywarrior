﻿using System.Collections.Generic;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Appearance
{
    public class CrystalSubView : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> listPart;
        [SerializeField]
        private List<Image> listImage;
        
        
        public void Equip(EquipmentType part, CrystalViewConfig crystalViewConfig)
        {
            if ((int)part >= listPart.Count || (int)part >= listImage.Count)
            {
                Debug.LogError($"Не найдена CrystalSubView для {part}");
                return;
            }

            if (crystalViewConfig == null)
            {
                listPart[(int)part].gameObject.SetActive(false);
            }
            else
            {
                listPart[(int)part].gameObject.SetActive(true);
                listImage[(int)part].sprite = crystalViewConfig.ViewSprite;
            }
            

        }
    }
}