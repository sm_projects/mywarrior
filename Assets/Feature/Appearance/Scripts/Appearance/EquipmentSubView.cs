﻿using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.Equippable;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;

namespace DefaultNamespace.Appearance
{
    public class EquipmentSubView : MonoBehaviour
    {
        [SerializeField] private Transform weaponLContent;
        [SerializeField] private Transform weaponRContent;
        [SerializeField] private Transform hatContent;
        [SerializeField] private Transform armorContent;
        [SerializeField] private Transform scarfContent;


        public void Equip(EquipmentType part, EquipmentViewConfig equipment, CrystalViewConfig crystalViewConfig)
        {
            switch (part)
            {
                case EquipmentType.Hat:
                {
                    SpawnPart(hatContent, equipment, crystalViewConfig);
                    break;
                }
                case EquipmentType.WeaponL:
                {
                    SpawnPart(weaponLContent, equipment, crystalViewConfig);
                    break;
                }
                case EquipmentType.WeaponR:
                {
                    SpawnPart(weaponRContent, equipment, crystalViewConfig);
                    break;
                }
                case EquipmentType.Armor:
                {
                    SpawnPart(armorContent, equipment, crystalViewConfig);
                    break;
                }
                case EquipmentType.Scarf:
                {
                    SpawnPart(scarfContent, equipment, crystalViewConfig);
                    break;
                }
            } 
        }
        

        private void AddPartsToDisabled(List<Part> partsToDisabled, List<Part> configs)
        {
            foreach (var config in configs)
            {
                partsToDisabled.Add(config);
            }
        }
        private void SpawnPart(Transform content, EquipmentViewConfig part = null, CrystalViewConfig crystalViewConfig = null)
        {
            for (int i = 0; i < content.childCount; i++)
            {
#if UNITY_EDITOR
                DestroyImmediate(content.GetChild(i).gameObject);
#else
                Destroy(content.GetChild(i).gameObject);
#endif
            }

            if (part != null)
            {
                var view = Instantiate(part.EquipmentViewObject, content);
                view.SetCrystal(crystalViewConfig);
            }

            
        }
    }
}