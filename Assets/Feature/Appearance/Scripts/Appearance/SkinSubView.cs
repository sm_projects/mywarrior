﻿﻿using System.Collections.Generic;
using DefaultNamespace.Equippable;
using UnityEngine;
using UnityEngine.XR;

namespace DefaultNamespace.Appearance
{
    public class SkinSubView : MonoBehaviour
    {
        [SerializeField] private Transform headContent;
        [SerializeField] private Transform breadContent;
        [SerializeField] private Transform eyesContent;
        
        [SerializeField] private Transform bodyContent;
        [SerializeField] private Transform footLContent;
        [SerializeField] private Transform footRContent;
        [SerializeField] private Transform handLContent;
        [SerializeField] private Transform handRContent;

        private List<Part> _allParts => new List<Part>()
        {
            Part.Head,
            Part.Bread,
            Part.Eyes,
            Part.Body,
            Part.FootL,
            Part.FootR,
            Part.HandL,
            Part.HandR,
        };
        
        private List<Part> _partsToActive = new List<Part>();
        
        
        public void RepaintSkin(SkinConfig skinConfig)
        {
            if (skinConfig == null)
            {
                SpawnPart(headContent);
                SpawnPart(bodyContent);
                SpawnPart(footLContent);
                SpawnPart(footRContent);
                SpawnPart(handLContent);
                SpawnPart(handRContent);  
                return;
            }

            SpawnPart(headContent, skinConfig.Head);
            SpawnPart(bodyContent, skinConfig.Body);
            SpawnPart(footLContent, skinConfig.Foot);
            SpawnPart(footRContent, skinConfig.Foot);
            SpawnPart(handLContent, skinConfig.HandL);
            SpawnPart(handRContent, skinConfig.HandR);
        }

        public void RepaintEye(EyeConfig eyeConfig)
        {
            SpawnPart(eyesContent, eyeConfig != null ? eyeConfig.Eye : null);
        }
        
        public void RepaintBeard(BeardConfig beardConfig)
        {
            SpawnPart(breadContent, beardConfig != null ? beardConfig.Beard : null);
        }

        public void SetActiveParts(List<Part> partsToDisable)
        {
            _partsToActive = _allParts;

            if (partsToDisable != null)
            {
                foreach (var part in partsToDisable)
                {
                    _partsToActive.Remove(part);
                    ChangePart(part, false);
                }
            }

            foreach (var part in _partsToActive)
            {
                ChangePart(part, true);
            }
            _partsToActive.Clear();
        }

        private void ChangePart(Part part, bool isActive)
        {
            switch (part)
            {
                case Part.Head:
                {
                    headContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.Bread:
                {
                    breadContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.Eyes:
                {
                    eyesContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.Body:
                {
                    bodyContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.FootL:
                {
                    footLContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.FootR:
                {
                    footRContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.HandL:
                {
                    handLContent.gameObject.SetActive(isActive);
                    break;
                }
                
                case Part.HandR:
                {
                    handRContent.gameObject.SetActive(isActive);
                    break;
                }
            }
        }
        
        private void SpawnPart(Transform content, GameObject part = null)
        {
            for (int i = 0; i < content.childCount; i++)
            {
#if UNITY_EDITOR
                DestroyImmediate(content.GetChild(i).gameObject);
#else
                Destroy(content.GetChild(i).gameObject);
#endif
            }

            
            
            if (part != null)
                Instantiate(part, content);
        }
    }
}