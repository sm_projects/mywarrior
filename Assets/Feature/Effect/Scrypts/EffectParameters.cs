﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EffectParameters
{
    public string EffectName;

    private List<UpdatableEffectActionParameters> _updatebleActions;
    private float _timeRemove;
    
    
    public bool IsNeedRemove(float currentTime)
    {
        return _timeRemove + 0.02f <= currentTime;
    }



    public List<EffectActionSendParameters> GetActionForUpdate(float currentTime)
    {
        if (_updatebleActions == null || _updatebleActions.Count == 0)
            return null;
        
        var result = new List<EffectActionSendParameters>();
        foreach (var parameters in _updatebleActions)
        {
            if (parameters.TimeToUpdate <= currentTime)
            {
                parameters.TimeToUpdate = currentTime + parameters.TimeСycle;
                result.Add(parameters.EffectAction);
            }
            Debug.Log($"time to effect {parameters.TimeToUpdate - currentTime}");
        }

        return result;
    }

    public EffectParameters(EffectSendParameters effectSend, float timeCreate)
    {
        _updatebleActions = new List<UpdatableEffectActionParameters>();
        foreach (var action in effectSend.EffectSendActions)
        {
            if (action.EffectActivationType == EffectActivationType.Time)
            {
                var parameters = new UpdatableEffectActionParameters();
                parameters.EffectAction = action;
                parameters.TimeToUpdate = action.TimeСycle + timeCreate;
                parameters.TimeСycle = action.TimeСycle;
                _updatebleActions.Add(parameters);
            }
        }
        _timeRemove = effectSend.TimeToRemove + timeCreate;

        EffectName = effectSend.EffectName;
    }
}

public class UpdatableEffectActionParameters
{
    public EffectActionSendParameters EffectAction;
    public float TimeToUpdate;
    public float TimeСycle;
}