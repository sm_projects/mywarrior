﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;
using Utilits;

[Serializable]
public class EffectAction
{
    public EffectActivationType EffectActivationType;
    public Component Component;
    public MathType TypeChange;
    public float Value;
    public List<ModificatorAction> ModificatorsCurrent;
    public List<ModificatorAction> ModificatorsTarget;
    
    
    public float TimeСycle;
}


[Serializable]
public class ModificatorAction
{
    public Component Component;
    public MathType MathType;
}


public enum EffectActivationType
{
    Time,
    Add,
    Remove,
    
}