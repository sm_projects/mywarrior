﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/Effect/EffectAssets")]
public class EffectAssets : ScriptableObject
{
    public List<EffectConfig> Effects;
    
    public EffectConfig GetEffect(string nameEffect)
    {
        foreach (var effect in Effects)
        {
            if (effect.EffectName == nameEffect)
            {
                return effect;
            }
        }

        return null;
    }
}