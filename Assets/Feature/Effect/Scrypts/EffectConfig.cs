﻿using System;
using System.Collections.Generic;

[Serializable]
public class EffectConfig
{
    public string EffectName;
    public float TimeToRemove;//-1 of no need remove

    public List<EffectAction> ListAction;
}