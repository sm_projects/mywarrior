﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Fight.Systems.FightCollisionSystem;
using UnityEngine;

namespace Feature.Fight.Systems.FightHeroSystem.Objects.FightHeroCollisionObject
{
    public class FightHeroCollisionObject : AObject<FightHeroSystem>
    {
        [SerializeField]
        private CharacterCollisionView heroCharacterCollisionView;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnHeroCollisionSpawn += HeroCollisionSpawn;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnHeroCollisionSpawn -= HeroCollisionSpawn;
        }

        private void HeroCollisionSpawn(CharacterCollisionParameters heroParameters)
        {
            heroParameters.Initialize(heroCharacterCollisionView);
        }
    }
}