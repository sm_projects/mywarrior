﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;

namespace Feature.Fight.Systems.FightHeroSystem.Objects.FightHeroSkillCollisionObject
{
    public class FightHeroSkillCollisionObject : AObject<FightHeroSystem>
    {
        [SerializeField]
        private SpawnSkillCollisionView spawnSkillCollisionView;
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSpawnCollisionSkill += SpawnSkill;
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSpawnCollisionSkill -= SpawnSkill;
        }

        private void SpawnSkill(SkillCollisionParameters obj)
        {
            var view = Instantiate(obj.SkillCollisionConfig.Prefab, spawnSkillCollisionView.DotSpawn);
            view.Initialize(obj);


        }
    }

}