﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Appearance;
using DefaultNamespace.Equippable;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class FightHeroAppearanceObject : AObject<FightHeroSystem>
    {
        [SerializeField]
        private AppearanceView appearance;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnLoadAppearance += LoadAppearance;
            Controller.OnLoadEquip += LoadEquipItems;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnLoadAppearance -= LoadAppearance;
            Controller.OnLoadEquip -= LoadEquipItems;
        }


        private void LoadAppearance(AppearanceParameters parameters)
        {
            appearance.SetBeard(parameters.Beard);
            appearance.SetEye(parameters.Eye);
            appearance.SetSkin(parameters.Skin);
        }
        
        private void LoadEquipItems(List<EquipViewParameters> parameters)
        {
            foreach (var parameter in parameters)
            {
                if(parameter == null)
                    continue;
                appearance.Equip(parameter.Part, parameter.ViewConfig, parameter.CrystalViewConfig);
            }
        }
    }
}

[Serializable]
public class EquipViewParameters
{
    public EquipmentType Part;

    public EquipmentViewConfig ViewConfig;

    public CrystalViewConfig CrystalViewConfig;
}