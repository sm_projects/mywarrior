﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class FightHeroVelocityObject : AObject<FightHeroSystem>
    {
        private ComponentParameters _parameters;
        
        [SerializeField]
        private HeroAnimationAssets heroAnimationAssets;

        [SerializeField]
        private Rigidbody2D rigidbody;

        [FormerlySerializedAs("componentName")] [SerializeField]
        private Component component;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSpawnComponents += SpawnComponents;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSpawnComponents -= SpawnComponents;
            _parameters.OnChangeValue -= ChangeSpeed;
        }

        private void SpawnComponents(List<ComponentParameters> parametersList)
        {
            foreach (var parameters in parametersList)
            {
                if (parameters.ComponentConfig.Component == component)
                {
                    _parameters = parameters;
                    _parameters.OnChangeValue += ChangeSpeed;
                }

                
            }
        }

        private void ChangeSpeed(ComponentParameters parameters)
        {
            rigidbody.velocity = new Vector2(parameters.Value, 0);
        }


    }
}