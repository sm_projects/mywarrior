﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class HeroSkillView :  MonoBehaviour
    {
        public event Action OnDown;
        public event Action OnUp;

        [SerializeField]
        private Image iconSkill;
        [SerializeField]
        private Image slider;
        [SerializeField]
        private Image crystalImage;
        [SerializeField]
        private Color32 defaultColor;
        [SerializeField]
        private Color32 clickColor;
        [SerializeField]
        private Pointer pointer;

        private SkillCrystalParameters _parameters;

        private void OnEnable()
        {
            pointer.OnDown += OnPointerDown;
            pointer.OnUp += OnPointerUp;
        }

        private void OnDisable()
        {
            pointer.OnDown -= OnPointerDown;
            pointer.OnUp -= OnPointerUp;
        }
        
        private void OnPointerDown()
        {
            OnDown?.Invoke();
            iconSkill.color = clickColor;
        }
        
        private void OnPointerUp()
        {
            OnUp?.Invoke();
            iconSkill.color = defaultColor;
        }

        private void Update()
        {
            if (_parameters.SkillConfig.TimeToReady != 0)
            {
                var value = (_parameters.TimeReady - Time.time) / _parameters.SkillConfig.TimeToReady;
                slider.fillAmount = Mathf.Clamp(value, 0, 1);
            }

           
        }

        public void Repaint(SkillCrystalParameters parameters, Sprite sprite)
        {
            _parameters = parameters;
            slider.fillAmount = 0;
            iconSkill.sprite = sprite;
            /*
            if (parameters.CrystalItemConfig != null)
                crystalImage.color = parameters.CrystalItemConfig.Color;
            else
                crystalImage.color = defaultColor;
                */

        }
    }
}