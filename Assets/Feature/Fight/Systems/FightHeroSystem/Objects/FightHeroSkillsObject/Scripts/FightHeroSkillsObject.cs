﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Skills.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class FightHeroSkillsObject : AObject<FightHeroSystem>
    {
        [SerializeField]
        private Transform contentPet;
        
        [SerializeField]
        private Transform contentMove;
        
        [SerializeField]
        private Transform contentSkills;
        
        [SerializeField]
        private HeroSkillView prefab;
        
        
        private Dictionary<int, SkillViewParametersPair> _listPrefab;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSpawnSkills += SpawnSkills;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSpawnSkills -= SpawnSkills;
            if (_listPrefab != null)
            {
                foreach (var prefab in _listPrefab)
                {
                    prefab.Value.View.OnDown += delegate { DownSkill(prefab.Key); };
                    prefab.Value.View.OnUp += delegate { DownSkill(prefab.Key);};
                    prefab.Value.Parameters.OnChange += delegate { ChangeView(prefab.Value.View, prefab.Value.Parameters); };
                }
            }
        }
        
        private void SpawnSkills(Dictionary<int, SkillCrystalParameters> listSkill)
        {
            _listPrefab = new Dictionary<int, SkillViewParametersPair>();
            foreach (var skill in listSkill)
            {
                HeroSkillView view = null;
                switch (skill.Value.SkillConfig.PositionSkillView)
                {
                    case PositionSkillView.Move:
                    {
                        view = Instantiate(prefab, contentMove);
                        break;
                    }
                    case PositionSkillView.PetSkills:
                    {
                        view = Instantiate(prefab, contentPet);
                        break;
                    }
                    case PositionSkillView.MainSkills:
                    {
                        view = Instantiate(prefab, contentSkills);
                        break;
                    }
                }
                view.Repaint(skill.Value, skill.Value.SkillConfig.Icon);
                view.OnDown += delegate { DownSkill(skill.Key); };
                view.OnUp += delegate { UpSkill(skill.Key);};
                skill.Value.OnChange += delegate { ChangeView(view, skill.Value); };

                var pair = new SkillViewParametersPair();
                pair.View = view;
                pair.Parameters = skill.Value;
                
                _listPrefab.Add(skill.Key,pair);
            }
        }

        private void ChangeView(HeroSkillView heroSkillView, SkillCrystalParameters parameters)
        {
            heroSkillView.Repaint(parameters, parameters.SkillConfig.Icon);
        }

        private void DownSkill(int index)
        {
            Controller.ClickSkill(index, true);
        }
        
        private void UpSkill(int index)
        {
            Controller.ClickSkill(index, false);
        }
    }

    public class SkillViewParametersPair
    {
        public HeroSkillView View;
        public SkillCrystalParameters Parameters;
    }

}