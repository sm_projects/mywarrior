﻿using System;
using UnityEngine.EventSystems;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class Pointer : EventTrigger
    {
        public event Action OnDown;
        public event Action OnUp;
        
        public override void OnPointerDown( PointerEventData data )
        {
            OnDown?.Invoke();
        }
        
        public override void OnPointerUp( PointerEventData data )
        {
            OnUp?.Invoke();
        }
    }
}