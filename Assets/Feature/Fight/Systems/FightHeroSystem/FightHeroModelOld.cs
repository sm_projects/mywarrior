﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class FightHeroModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public List<int> LoadEquipItems()
        {
            var result = new List<int>();
            var data = LoadObjectInfo<InventoryEquipData>(HeroId);
            foreach (var slot in data.slots)
            {
                if(slot != null && slot.itemId != -1)
                    result.Add(slot.itemId);
            }

            return result;
        }

        public int LoadEquipmentPart(EquipmentType equipmentType)
        {
            var data = LoadObjectInfo<InventoryEquipData>(HeroId);
            if (data == null || data.slots == null)
                return -1;

            foreach (var slot in data.slots)
            {
                if (slot.EquipmentType == equipmentType)
                {
                    return slot.itemId;
                }
            }
            return -1;
        }
        
        public HeroAppearanceData LoadAppearance()
        {
            return LoadObjectInfo<HeroAppearanceData>(HeroId);
        }

        public HeroInfoParameters LoadLoadHeroInfo()
        {
            return LoadObjectInfo<HeroInfoParameters>(HeroId);
        }
    }
}