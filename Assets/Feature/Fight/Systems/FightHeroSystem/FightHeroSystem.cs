﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.Equippable;
using Feature.Fight.Systems.FightCollisionSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Data;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Skills.Scripts;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Fight.Systems.FightHeroSystem
{
    public class FightHeroSystem : AbstractFightCharacterSystem
    {
        public event Action<AppearanceParameters> OnLoadAppearance;
        
        public event Action<List<EquipViewParameters>> OnLoadEquip;
        
        public event Action<SkillCollisionParameters> OnSpawnCollisionSkill;

        public event Action<List<ComponentParameters>> OnSpawnComponents;

        public event Action<Dictionary<int, SkillCrystalParameters>> OnSpawnSkills;

        public event Action<CharacterCollisionParameters> OnHeroCollisionSpawn;

        public event Action<HeroInfoParameters> OnHeroLoaded;

        [SerializeField]
        private FightHeroModelOld modelOld;

        [SerializeField]
        private AllPartAssets allPartAssets;

        [SerializeField]
        private ItemsAsset itemsAsset;
        
        [SerializeField]
        private SkillsAsset skillsAsset;
        

        [SerializeField]
        private CrystalCategory crystalCategory;

        private ItemsController ItemsController => ItemsController.Instance;
        

        public void StartHero(int groupId)
        {
            RepaintEquip();
            RepaintAppearance();
            StartCalculations(groupId);
            LoadHeroInfo();
            
            OnHeroCollisionSpawn?.Invoke(collisionCalculation.GetHeroCollision());
            OnSpawnComponents?.Invoke(effectsCalculation.GetComponents());
            OnSpawnSkills?.Invoke(skillsCalculation.GetListSkill());

            collisionCalculation.OnSpawnCollisionSkill += SpawnCollisionSkill;
        }



        public void StopHero()
        {
            collisionCalculation.OnSpawnCollisionSkill -= SpawnCollisionSkill;
        }

        private void LoadHeroInfo()
        {
            var heroInfo = modelOld.LoadLoadHeroInfo();
            OnHeroLoaded?.Invoke(heroInfo);
        }

        private void SpawnCollisionSkill(SkillCollisionParameters parameters)
        {
            OnSpawnCollisionSkill?.Invoke(parameters);
        }
        

        private void StartCalculations(int groupId)
        {
            var equipSkills = GetEquipSkills();
            
            var moveLeft = new SkillCrystalParameters();
            moveLeft.SkillConfig = skillsAsset.MoveLeft;
            equipSkills.Add(moveLeft);
            
            var moveRight = new SkillCrystalParameters();
            moveRight.SkillConfig = skillsAsset.MoveRght;
            equipSkills.Add(moveRight);

            var stats = GetListStats();
            
            StartFight(equipSkills, stats, groupId);
        }

        private void RepaintEquip()
        {
            var result = new List<EquipViewParameters>();
            result.Add(GetEquipItem(EquipmentType.WeaponL));
            result.Add(GetEquipItem(EquipmentType.WeaponR));
            result.Add(GetEquipItem(EquipmentType.Armor));
            result.Add(GetEquipItem(EquipmentType.Hat));
            result.Add(GetEquipItem(EquipmentType.Scarf));
            OnLoadEquip?.Invoke(result);
        }
        
        private void RepaintAppearance()
        {
            var data = modelOld.LoadAppearance();
            
            var  appearance = new AppearanceParameters();
            appearance.Beard = allPartAssets.BeardsAsset.GetPart<BeardConfig>(data.BeardId);
            appearance.Eye = allPartAssets.EyeAsset.GetPart<EyeConfig>(data.EyeId);
            appearance.Skin = allPartAssets.SkinAsset.GetPart<SkinConfig>(data.SkinId);
            
            OnLoadAppearance?.Invoke(appearance);
        }
        
        
        

        public void ClickSkill(int index, bool isDown)
        {
            UseSkill(index, isDown);
        }
        

        private List<SkillCrystalParameters> GetEquipSkills()
        {
            var listSkills = new List<SkillCrystalParameters>();
            var items = modelOld.LoadEquipItems();
            
            foreach (var itemId in items)
            {
                var itemData = ItemsController.LoadItem(itemId) as EquipItemData;
                
                var itemConfig = itemData.ItemConfig as EquipItemConfig;
                var skillConfig = skillsAsset.GetSkill(itemConfig.SkillName);
                if (skillConfig == null)
                {
                    continue;
                }

                CrystalItemConfig crystalConfig = null;
                if (itemData.CrystalId != -1)
                {
                    crystalConfig = (ItemsController.Instance.LoadItem(itemData.CrystalId) as CrystalItemData).ItemConfig as CrystalItemConfig;
                }
                var skillCrystal = new SkillCrystalParameters();
                skillCrystal.SkillConfig = skillConfig;
                skillCrystal.CrystalItemConfig = crystalConfig;
                listSkills.Add(skillCrystal);
            }

            return listSkills;

        }

        private List<ComponentModificator> GetListStats()
        {
            var stats = new List<ComponentModificator>();
            var items = modelOld.LoadEquipItems();
            foreach (var itemId in items)
            {
                var equipItem = ItemsController.LoadItem(itemId) as EquipItemData;

                var config = equipItem.ItemConfig as EquipItemConfig;
                if (config != null)
                {
                    foreach (var stat in config.Stats)
                    {
                        stats.Add(stat);
                    }
                }
            }

            return stats;
        }

        private EquipViewParameters GetEquipItem(EquipmentType part)
        {
            int itemId = modelOld.LoadEquipmentPart(part);
            EquipViewParameters result = null;
            if (itemId != -1)
            {
                result = new EquipViewParameters();
                var itemData = ItemsController.LoadItem(itemId) as EquipItemData;
                var itemConfig = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;
                if (itemData.CrystalId != -1)
                {
                    var crystalConfig = itemsAsset.GetItemConfig(itemData.CrystalId) as CrystalItemConfig;
                    result.CrystalViewConfig = crystalConfig.CrystalViewConfig;
                }

                result.Part = part;
                result.ViewConfig = itemConfig.PrefabView;
                
            }

            return result;
        }
    }
}