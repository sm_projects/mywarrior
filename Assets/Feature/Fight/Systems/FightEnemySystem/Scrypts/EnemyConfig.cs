﻿using System;
using System.Collections.Generic;
using Feature.Fight.Systems;
using Feature.Stats.Scripts;

[Serializable]
public class EnemyConfig
{
    public int EnemyId;
    public List<EquipViewParameters> EquipViewParameters;
    public AppearanceParameters AppearanceParameters;
    public List<SkillCrystalParameters> SkillCrystalParameters;
    public List<ComponentModificator> StatParameters;
    public HeroInfoParameters HeroInfoParameters;
}