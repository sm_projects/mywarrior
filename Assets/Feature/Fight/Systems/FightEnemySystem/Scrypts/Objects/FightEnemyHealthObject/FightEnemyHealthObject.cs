﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Feature.Fight.Systems.EnemyHeroSystem.Objects
{
    public class FightEnemyHealthObject: AObject<FightEnemySystem>
    {
        private ComponentParameters _parameters;

        [SerializeField]
        private Image healthSlider;
        
        [SerializeField]
        private Text healthText;
        
        [SerializeField]
        private Text playerName;

        [SerializeField]
        private ChangeHealthView prefabView;

        [SerializeField]
        private Transform contentView;
        
        
        [FormerlySerializedAs("componentName")] [SerializeField]
        private Component component;

        private float _maxValue;
        private float _currentValue;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSpawnComponents += SpawnComponents;
            Controller.OnHeroLoaded += HeroLoaded;
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSpawnComponents -= SpawnComponents;
            Controller.OnHeroLoaded -= HeroLoaded;
            _parameters.OnChangeValue -= ChangeHealth;
        }

        private void SpawnComponents(List<ComponentParameters> parametersList)
        {
            foreach (var parameters in parametersList)
            {
                if (parameters.ComponentConfig.Component == component)
                {
                    _parameters = parameters;
                    _parameters.OnChangeValue += ChangeHealth;
                    ChangeHealth(_parameters);
                }

                
            }
        }

        private void HeroLoaded(HeroInfoParameters heroInfo)
        {
            _maxValue = 0;
            _currentValue = 0;
            playerName.text = heroInfo.HeroName;
        }
        private void ChangeHealth(ComponentParameters parameters)
        {
            if (parameters.Value >= _maxValue)
                _maxValue = parameters.Value;
            
            if (parameters.Value != _maxValue)
            {
                var view = Instantiate(prefabView, contentView);
                view.Initialize(_currentValue - parameters.Value);
            }

            _currentValue = parameters.Value;
            
            healthSlider.fillAmount = parameters.Value / _maxValue;
            healthText.text = ((int)parameters.Value).ToString();
        }
    }
}