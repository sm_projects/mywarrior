﻿using UnityEngine;
using UnityEngine.UI;

namespace Feature.Fight.Systems.EnemyHeroSystem.Objects
{
    public class ChangeHealthView : MonoBehaviour
    {
        [SerializeField]
        private Color greenColor;
        
        [SerializeField]
        private Color redColor;

        [SerializeField]
        private Animator animator;

        [SerializeField] 
        private Text text;
        
        
        public void Initialize(float value)
        {
            text.text = value.ToString();
            if (value > 0)
            {
                text.color = greenColor;
            }
            else
            {
                text.color = redColor;
            }

            transform.localPosition = transform.localPosition + new Vector3(Random.Range(-50, 50), Random.Range(-50, 50),0);
            animator.Play($"ShowDamage");
        }

        private void DestroyView()
        {
            Destroy(gameObject);
        }
    }
}