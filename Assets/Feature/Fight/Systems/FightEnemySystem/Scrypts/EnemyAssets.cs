﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/Enemy/EnemyAssets")]
public class EnemyAssets : ScriptableObject
{
    public List<EnemyConfig> ListConfig;

    public EnemyConfig GetEnemyConfig(int enemyId)
    {
        foreach (var enemy in ListConfig)
        {
            if (enemy.EnemyId == enemyId)
                return enemy;
        }

        return null;
    }

}