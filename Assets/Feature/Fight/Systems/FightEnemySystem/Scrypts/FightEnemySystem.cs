﻿using System;
using System.Collections.Generic;
using Feature.Fight.Systems.FightCollisionSystem;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;

namespace Feature.Fight.Systems.EnemyHeroSystem
{
    public class FightEnemySystem : AbstractFightCharacterSystem
    {
        public event Action<AppearanceParameters> OnLoadAppearance;
        public event Action<List<EquipViewParameters>> OnLoadEquip;
        
        public event Action<HeroInfoParameters> OnHeroLoaded;
        
        public event Action<CharacterCollisionParameters> OnHeroCollisionSpawn;
        
        public event Action<List<ComponentParameters>> OnSpawnComponents;
        public event Action<List<SkillCrystalParameters>> OnSpawnSkills;
        public event Action<SkillCollisionParameters> OnSpawnCollisionSkill;


        [SerializeField]
        private EnemyAssets enemyAssets;

        private EnemyConfig _enemyConfig;
        
        public void StartEnemy(int groupId, int enemyId)
        {
            _enemyConfig = enemyAssets.GetEnemyConfig(enemyId);
            
            OnLoadEquip?.Invoke(_enemyConfig.EquipViewParameters);
            OnLoadAppearance?.Invoke(_enemyConfig.AppearanceParameters);
            StartCalculations(groupId);
            OnHeroLoaded?.Invoke(_enemyConfig.HeroInfoParameters);
            
            OnHeroCollisionSpawn?.Invoke(collisionCalculation.GetHeroCollision());
            OnSpawnComponents?.Invoke(effectsCalculation.GetComponents());
            //OnSpawnSkills?.Invoke(skillsCalculation.GetListSkill());

            collisionCalculation.OnSpawnCollisionSkill += SpawnCollisionSkill;
        }
        
        public void StopEnemy()
        {
            collisionCalculation.OnSpawnCollisionSkill -= SpawnCollisionSkill;
        }
        
        private void StartCalculations(int groupId)
        {
            var equipSkills = _enemyConfig.SkillCrystalParameters;
            var stats = _enemyConfig.StatParameters;
            
            StartFight(equipSkills,stats, groupId);
        }
        
        private void SpawnCollisionSkill(SkillCollisionParameters parameters)
        {
            OnSpawnCollisionSkill?.Invoke(parameters);
        }

        
    }
}