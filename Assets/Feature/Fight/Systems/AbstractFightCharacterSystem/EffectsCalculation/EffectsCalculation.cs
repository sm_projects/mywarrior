﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Fight.Systems;
using Feature.Stats.Scripts;
using Feature.Windows.Inventory.Systems.InventorySkillsSystems;
using UnityEngine;
using Utilits;

namespace Feature.Fight.Scripts
{
    public class EffectsCalculation : ACalculation
    {
        
        [SerializeField]
        private SwapEffectAssets swapEffectAssets;

        [SerializeField]
        private EffectAssets effectAssets;

        [SerializeField]
        private ComponentAssets componentAssets;
        
        private List<ComponentParameters> _currentComponents;
        
        private List<EffectParameters> _currentEffects;

        private float CurrentTime => Time.time;
        
        
        //передаем эквип и по нему определяем значения компонента и наверное позицию
        public void Initialize(List<ComponentModificator> stats)
        {
            _currentComponents = new List<ComponentParameters>();
            _currentEffects = new List<EffectParameters>();
            foreach (var componentConfig in componentAssets.DefaultComponents)
            {
                var componentParameters = new ComponentParameters();
                componentParameters.ComponentConfig = componentConfig;

                switch (componentConfig.StartValue)
                {
                    case StartValue.Zero:
                    {
                        componentParameters.Value = 0; 
                        break;
                    }
                    case StartValue.DefaultValue:
                    {
                        componentParameters.Value = componentConfig.DefaultValue;  
                        break;
                    }
                    case StartValue.MathValue:
                    {
                        var listPara = new List<MathTypePara>();
                        foreach (var stat in stats)
                        {
                            if (stat.Component == componentConfig.Component)
                            {
                                var para = new MathTypePara();
                                para.MathType = stat.MathType;
                                para.Value = stat.Value;
                                listPara.Add(para);
                            }
                        }
                    
                        componentParameters.Value = MathTypeOperation.GetResult(componentConfig.DefaultValue, listPara); 
                        break;
                    }
                }

                componentParameters.ChangeComponent();
                AddComponent(componentParameters);
            }
            

        }

        public void Update()
        {
            if(_currentEffects == null)
                return;
            var buffer = _currentEffects;
            foreach (var effect in buffer)
            {
                var effectActions = effect.GetActionForUpdate(CurrentTime);
                foreach (var effectAction in effectActions)
                {
                    ActionEffect(effectAction);
                    Debug.Log($"ActionEffect");
                }
                
                if (effect.IsNeedRemove(CurrentTime))
                {
                    RemoveEffect(effect);
                    Debug.Log($"RemoveEffect");
                    break;
                }
                
            }
        }
        
        public bool IsCanUseSkill(SkillCrystalParameters current)
        {
            foreach (var effectName in current.SkillConfig.BlockedEffectNames)
            {
                foreach (var effect in _currentEffects)
                {
                    if (effect.EffectName == effectName)
                        return false;
                }
            }
            return true;
        }
        
        public void UseSkill(List<EffectSendParameters> effectsSend)
        {
            foreach (var effect in effectsSend)
            {
                AddEffect(effect);
            }
            
        }

        public List<ComponentParameters> GetComponents()
        {
            return _currentComponents;
        }

        private void AddComponent(ComponentParameters componentConfig)
        {
            _currentComponents.Add(componentConfig);
        }

        private void AddEffect(EffectSendParameters newEffectConfig)
        {
            Debug.Log($"AddEffect {newEffectConfig.EffectName}");
            foreach (var effect in _currentEffects)
            {
                if (effect.EffectName == newEffectConfig.EffectName)
                {
                    return;
                }
            }
            
            var listSwap = new List<SwapEffectConfig>();
            foreach (var swapEffect in swapEffectAssets.Swaps)
            {
                if (swapEffect.NameFirstEffects == newEffectConfig.EffectName)
                    listSwap.Add(swapEffect);
            }
            
            foreach (var swapEffectConfig in listSwap)
            {
                foreach (var currentEffect in _currentEffects)
                {
                    if (swapEffectConfig.NameSecondEffects == currentEffect.EffectName)
                    {

                        SwapEffectAction(swapEffectConfig, newEffectConfig, currentEffect);
                        return;
                    }
                }
            }
            
            foreach (var effectAction in newEffectConfig.EffectSendActions)
            {
                if (effectAction.EffectActivationType == EffectActivationType.Add)
                {
                    ActionEffect(effectAction);
                }
            }

            if (newEffectConfig.TimeToRemove > 0)
            {
                var effectParameters = new EffectParameters(newEffectConfig, CurrentTime);
            
                _currentEffects.Add(effectParameters);
            }
            
        }


        private void ActionEffect(EffectActionSendParameters effectAction)
        {
            
            ComponentParameters componentForChange = null;
            foreach (var component in _currentComponents)
            {
                if (component.ComponentConfig.Component == effectAction.Component)
                {
                    componentForChange = component;
                    break;
                }
            }

            if (componentForChange != null)
            {
                ChangeValueComponent(componentForChange, effectAction);
            }
            
        }

        public List<EffectSendParameters> GetSendEffects(List<EffectConfig> effectConfigs)
        {
            var result = new List<EffectSendParameters>();
            foreach (var effectConfig in effectConfigs)
            {
                var effectSend = new EffectSendParameters();
                effectSend.EffectName = effectConfig.EffectName;
                effectSend.TimeToRemove = effectConfig.TimeToRemove;
                effectSend.EffectSendActions = new List<EffectActionSendParameters>();
                foreach (var effectAction in effectConfig.ListAction)
                {
                    var sendAction = new EffectActionSendParameters(effectAction);
                
                    float value = effectAction.Value;
                    foreach (var modificator in effectAction.ModificatorsCurrent)
                    {
                        foreach (var component in _currentComponents)
                        {
                            if (component.ComponentConfig.Component == modificator.Component)
                            {
                                value = MathTypeOperation.GetResult(value, modificator.MathType, component.Value);
                                break;
                            }
                        }
                    }

                    sendAction.Value = value;
                    effectSend.EffectSendActions.Add(sendAction);
                     
                }
                result.Add(effectSend); 
            }

            return result;
        }

        private float ModificatorEffect(EffectActionSendParameters effectAction)
        {
            float value = effectAction.Value;
            foreach (var modificator in effectAction.ModificatorTarget)
            {
                foreach (var component in _currentComponents)
                {
                    if (component.ComponentConfig.Component == modificator.Component)
                    {

                        value = MathTypeOperation.GetResult(value, modificator.MathType, component.Value);
                        
                        break;
                    }
                }
            }

            return value;
        }



        //add or Multiply
        private void ChangeValueComponent(ComponentParameters componentConfig, EffectActionSendParameters action)
        {
            var modificatorValue = ModificatorEffect(action);
            var newValue = MathTypeOperation.GetResult(componentConfig.Value, action.MathType, modificatorValue);
            componentConfig.Value = newValue;
            componentConfig.ChangeComponent();
            Debug.Log($"ActionEffect {action.Component} {action.EffectActivationType} {action.MathType} {action.Value} {componentConfig.Value - newValue}");

        }

        private void SwapEffectAction(SwapEffectConfig swapEffectConfig, EffectSendParameters first, EffectParameters second)
        {
            switch (swapEffectConfig.SwapEffectActions)
            {
                case SwapEffectActions.RemoveSecond:
                {
                    RemoveEffect(second);
                    break;
                }
                case SwapEffectActions.RemoveSecondAndAddFirst:
                {
                    RemoveEffect(second);
                    AddEffect(first);
                    break;
                }
                case SwapEffectActions.RemoveSecondAndAddNewEffect:
                {
                    RemoveEffect(second);
                    var effectConfig = effectAssets.GetEffect(swapEffectConfig.NameNewEffect);
                    var effectSendParameters = new EffectSendParameters();
                    effectSendParameters.EffectName = effectConfig.EffectName;
                    effectSendParameters.TimeToRemove = effectConfig.TimeToRemove;
                    effectSendParameters.EffectSendActions = new List<EffectActionSendParameters>();
                    foreach (var effectAction in effectConfig.ListAction)
                    {
                        var sendAction = new EffectActionSendParameters(effectAction);
                        sendAction.Value = effectAction.Value;
                        effectSendParameters.EffectSendActions.Add(sendAction);
                    }
                    AddEffect(effectSendParameters);
                    break;
                }
            }
        }
        
        private void RemoveEffect(EffectParameters effectConfigForRemove)
        {
            _currentEffects.Remove(effectConfigForRemove);
        }

    }

   
}

public class EffectSendParameters
{
    public string EffectName;
    public float TimeToRemove;

    public List<EffectActionSendParameters> EffectSendActions;

}

public class EffectActionSendParameters
{
    public EffectActivationType EffectActivationType;
    public float Value;
    public MathType MathType;
    public Component Component;
    public List<ModificatorAction> ModificatorTarget;
    public float TimeСycle;

    public EffectActionSendParameters(EffectAction action)
    {
        EffectActivationType = action.EffectActivationType;
        MathType = action.TypeChange;
        Component = action.Component;
        ModificatorTarget = action.ModificatorsTarget;
        TimeСycle = action.TimeСycle;
    }

}
