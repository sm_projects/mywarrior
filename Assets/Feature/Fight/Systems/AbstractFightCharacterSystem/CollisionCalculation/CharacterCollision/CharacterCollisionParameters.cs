﻿using System;

namespace Feature.Fight.Systems.FightCollisionSystem
{
    public class CharacterCollisionParameters
    {
        
        public event Action<SkillCollisionParameters> OnCharacterStartCollision;
        public event Action<CharacterCollisionParameters> OnDestroy;

        private CharacterCollisionView _view;
        private int _groupId;
        
        public CharacterCollisionParameters(int groupId)
        {
            _groupId = groupId; 
        }

        public void Initialize(CharacterCollisionView view)
        {
            view.Initialize(this);
            _view = view;
           
            
            _view.OnStartCollision += StartCollision;
        }

        private void StartCollision(SkillCollisionParameters target)
        {
            if (target.GroupId != _groupId)
            {
                OnCharacterStartCollision?.Invoke(target);
            }

            
        }

        public void Destroy()
        {
            OnDestroy?.Invoke(this);
            _view.OnStartCollision -= StartCollision;
        }
    }
}