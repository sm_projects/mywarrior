﻿using System;
using Feature.Fight.Systems.FightCollisionSystem.SkillCollision;
using UnityEngine;

namespace Feature.Fight.Systems.FightCollisionSystem
{
    public class CharacterCollisionView : MonoBehaviour
    {
        private CharacterCollisionParameters _parameters;

        public event Action<SkillCollisionParameters> OnStartCollision;

        public void Initialize(CharacterCollisionParameters parameters)
        {
            _parameters = parameters;
        }

        private void OnDestroy()
        {
            if(_parameters != null)
                _parameters.Destroy();
        }

        private CharacterCollisionParameters GetParameters()
        {
            return _parameters;
        }
        
        private void OnTriggerEnter2D(Collider2D col)
        {
            if(_parameters == null)
                return;
            
            var view = col.gameObject.GetComponent<SkillCollisionView>();
            if(view == null)
                return;

            var target = view.GetParameters();
            OnStartCollision?.Invoke(target);
        }
    }
}