﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using UnityEngine;

namespace Feature.Fight.Systems.FightCollisionSystem
{
    public class CollisionCalculation : ACalculation
    {

        [SerializeField]
        private SkillCollisionAssets skillCollisionAssets;

        public event Action<SkillCollisionParameters> OnSpawnCollisionSkill;

        public event Action<List<EffectSendParameters>> OnCollisionSkill;
        
        private CharacterCollisionParameters _heroCharacterCollisionParameters;
        private int _groupId;

        public void Initialize(int groupId)
        {
            _groupId = groupId;
            _heroCharacterCollisionParameters = new CharacterCollisionParameters(_groupId);
            _heroCharacterCollisionParameters.OnCharacterStartCollision += CharacterStartCharacterCollision;
            _heroCharacterCollisionParameters.OnDestroy += DestroyParameters;
        }

        public CharacterCollisionParameters GetHeroCollision()
        {
            return _heroCharacterCollisionParameters;
        }
        
        private void DestroyParameters(CharacterCollisionParameters parameters)
        {
            parameters.OnDestroy -= DestroyParameters;
            parameters.OnCharacterStartCollision -= CharacterStartCharacterCollision;
        }

        private void CharacterStartCharacterCollision(SkillCollisionParameters target)
        {
            OnCollisionSkill?.Invoke(target.ListSendEffects);
            target.DestroyView();
        }

        public void UseSkill(List<EffectSendParameters> listEffects, string skillName)
        {
           var skillConfig = skillCollisionAssets.GetConfig(skillName);
            if(skillConfig == null)
                return;

            var parameters = new SkillCollisionParameters();
            parameters.ListSendEffects = listEffects;
            parameters.GroupId = _groupId;
            parameters.SkillCollisionConfig = skillConfig;
            OnSpawnCollisionSkill?.Invoke(parameters);
        }
    }
}