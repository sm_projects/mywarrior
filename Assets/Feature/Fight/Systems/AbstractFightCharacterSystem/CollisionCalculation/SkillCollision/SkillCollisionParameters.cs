﻿using System.Collections.Generic;
using Feature.Fight.Systems;
using Feature.Fight.Systems.FightCollisionSystem.SkillCollision;

public class SkillCollisionParameters
{
    public List<EffectSendParameters> ListSendEffects;
    public SkillCollisionConfig SkillCollisionConfig;
    public int GroupId;

    private SkillCollisionView _skillCollisionView;

    public void DestroyView()
    {
        _skillCollisionView.DestroyView();
    }

    public void SetView(SkillCollisionView skillCollisionView)
    {
        _skillCollisionView = skillCollisionView;
    }
}