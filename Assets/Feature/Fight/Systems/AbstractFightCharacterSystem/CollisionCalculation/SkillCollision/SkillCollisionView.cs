﻿using UnityEngine;

namespace Feature.Fight.Systems.FightCollisionSystem.SkillCollision
{
    public class SkillCollisionView : MonoBehaviour
    {

        private SkillCollisionParameters _parameters;
        
        
        public void Initialize(SkillCollisionParameters parameters)
        {
            _parameters = parameters;
            parameters.SetView(this);
        }

        public SkillCollisionParameters GetParameters()
        {
            return _parameters;
        }

        public void DestroyView()
        {
            Destroy(this.gameObject);
        }
    }
}