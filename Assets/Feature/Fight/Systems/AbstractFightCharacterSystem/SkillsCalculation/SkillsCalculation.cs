﻿using System;
using System.Collections.Generic;
using System.Linq;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Skills.Scripts;
using Feature.Windows.Inventory.Systems.InventorySkillsSystems;
using UnityEngine;

namespace Feature.Fight.Systems
{
    public class SkillsCalculation : ACalculation
    {
        [SerializeField]
        private SkillsAsset skillsAsset;
        
        private Dictionary<int, SkillCrystalParameters> _listSkills;
        private float CurrentTime => Time.time;
        
        public  void Initialize(List<SkillCrystalParameters> listEquip)
        {
            _listSkills = new Dictionary<int, SkillCrystalParameters>();
            int i = 0;
            foreach (var skillCrystal in listEquip)
            {
                
                //skillCrystal.Index = i;
                if (skillCrystal.SkillConfig.IsReadyInStart)
                {
                    skillCrystal.TimeReady = 0;
                }
                else
                {
                    skillCrystal.TimeReady += CurrentTime + skillCrystal.SkillConfig.TimeToReady;
                }

                _listSkills.Add(i, skillCrystal);
                i++;
            }
            
        }

        public Dictionary<int, SkillCrystalParameters> GetListSkill()
        {
            return _listSkills;
        }

        public SkillCrystalParameters GetSkill(int index)
        {
            return _listSkills[index];
        }

        public bool IsCanClickSkill(SkillCrystalParameters current)
        {
            return current.TimeReady <= CurrentTime;
        }
        public void UseSkill(int index)
        {
            var skill = _listSkills[index];
            skill.TimeReady = CurrentTime + skill.SkillConfig.TimeToReady;
            skill.PushEvent();
        }
    }
    

    [Serializable]
    public class SkillCrystalParameters
    {
        public SkillConfig SkillConfig;
        public CrystalItemConfig CrystalItemConfig;
        public float TimeReady;
        public event Action<SkillCrystalParameters> OnChange;

        public void PushEvent()
        {
            OnChange?.Invoke(this);
        }
    }
    
}