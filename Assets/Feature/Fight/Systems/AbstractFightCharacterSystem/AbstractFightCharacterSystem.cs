﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Fight.Scripts;
using Feature.Fight.Systems;
using Feature.Fight.Systems.FightCollisionSystem;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Stats.Scripts;
using Feature.Windows.Inventory.Systems.InventorySkillsSystems;
using UnityEngine;

public abstract class AbstractFightCharacterSystem: AController
{
    [SerializeField]
    protected EffectsCalculation effectsCalculation;
    [SerializeField]
    protected SkillsCalculation skillsCalculation;

    [SerializeField]
    protected CollisionCalculation collisionCalculation;

    [SerializeField]
    private EffectAssets effectAssets;
    
    
    protected void StartFight(List<SkillCrystalParameters> listEquip, List<ComponentModificator> stats, int groupId)
    {
        effectsCalculation.Initialize(stats);
        skillsCalculation.Initialize(listEquip);
        collisionCalculation.Initialize(groupId);

        collisionCalculation.OnCollisionSkill += CollisionSkill;
    }

    protected void StopFight()
    {
        collisionCalculation.OnCollisionSkill -= CollisionSkill;
    }

    private void Update()
    {
        if(effectsCalculation != null)
            effectsCalculation.Update();
    }

    private void CollisionSkill(List<EffectSendParameters> listEffects)
    {
        effectsCalculation.UseSkill(listEffects);
    }

    protected void UseSkill(int index, bool isDown)
    {
        var current = skillsCalculation.GetSkill(index);
        if(current == null)
            return;
        if(!skillsCalculation.IsCanClickSkill(current))
            return;
        if (!effectsCalculation.IsCanUseSkill(current))
            return;
        
        skillsCalculation.UseSkill(index);

        var listEffects = GetSkillEffects(current, isDown);

        var effectSend = effectsCalculation.GetSendEffects(listEffects);
        
        if (current.SkillConfig.IsYourself)
        {
            effectsCalculation.UseSkill(effectSend);  
        }
        else
        {
            collisionCalculation.UseSkill(effectSend, current.SkillConfig.SkillName);
        }
    }

    private List<EffectConfig> GetSkillEffects(SkillCrystalParameters parameters, bool isDown)
    {

        var result = new List<EffectConfig>();
        if (isDown)
        {
            foreach (var effectName in parameters.SkillConfig.DownEffectNames)
            {
                var effect = effectAssets.GetEffect(effectName);
                result.Add(effect);
            }
        }
        else
        {
            foreach (var effectName in parameters.SkillConfig.UpEffectNames)
            {
                var effect = effectAssets.GetEffect(effectName);
                result.Add(effect);
            }
        }



        if (parameters.CrystalItemConfig != null)
        {
            var effect = effectAssets.GetEffect(parameters.CrystalItemConfig.EffectName);
            result.Add(effect);
        }

        return result;
    }


}
