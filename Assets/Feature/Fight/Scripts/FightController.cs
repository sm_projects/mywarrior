﻿using Architecture.ControllerManager;
using Architecture.ModelManager;
using Feature.Fight.Systems.EnemyHeroSystem;
using Feature.Fight.Systems.FightHeroSystem;
using Feature.SceneLoader.Configs;
using Feature.SceneLoader.Scripts;
using UnityEngine;

namespace Feature.Fight.Scripts
{
    public class FightController : AController, IModelProvider<SceneLoaderModel>
    {

        private SceneLoaderModel _model;
        
        public void SetModel(SceneLoaderModel model)
        {
            _model = model;
        }
        
        [SerializeField]
        private FightHeroSystem fightHeroSystem;

        [SerializeField]
        private FightEnemySystem fightEnemySystem;
        

        public override void Subscribe()
        {
            base.Subscribe();
            _model.OnLoadFightScene += StartFight;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            _model.OnLoadFightScene -= StartFight;
        }


        
        

        private void StartFight()
        {
            fightHeroSystem.StartHero(0);
            fightEnemySystem.StartEnemy(1, 0);
        }



    }
}