﻿using System;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts;
using UnityEngine;

namespace Feature.DebugMode
{
    public class DebugModeController : AController
    {
#if UNITY_EDITOR

        public event Action<bool> OnChangeDebugMode;
        
        private bool _isActive = false;
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                _isActive = !_isActive;
                OnChangeDebugMode?.Invoke(_isActive);
                
            }
            
        }
        
        public void AddItem(int itemId)
        {
            var item = ItemsController.Instance.CreateItem(itemId);
            if (item == null)
            {
                Debug.Log($"Не найден itemId {itemId}");
                return; 
            }

            
            //EventManager.PushEvent(new AddItemToInventoryEvent(item));
        }
#endif
        
    }
}