﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.DebugMode
{
    public class DebugModeObject: AObject<DebugModeController>
    {
#if UNITY_EDITOR

        [SerializeField]
        private GameObject panel;


        [SerializeField]
        private Button addItem;
        
        [SerializeField]
        private InputField inputItem;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnChangeDebugMode += SetActivePanel;
            
            addItem.onClick.AddListener(ClickAddItem);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnChangeDebugMode -= SetActivePanel;
            
            addItem.onClick.RemoveListener(ClickAddItem);
        }

        private void ClickAddItem()
        {
            int itemId = Convert.ToInt32(inputItem.text);
            Controller.AddItem(itemId);
        }

        private void SetActivePanel(bool isActive)
        {
            panel.SetActive(isActive);
        }

#endif
    }
}