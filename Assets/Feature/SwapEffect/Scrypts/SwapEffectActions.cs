﻿public enum SwapEffectActions
{
    RemoveSecond,
    RemoveSecondAndAddFirst,
    RemoveSecondAndAddNewEffect,
}