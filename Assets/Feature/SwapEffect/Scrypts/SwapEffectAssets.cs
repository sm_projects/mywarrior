﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/SwapEffect/SwapEffectAssets")]

public class SwapEffectAssets : ScriptableObject
{
    public List<SwapEffectConfig> Swaps;
}