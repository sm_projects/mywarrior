﻿using System;

[Serializable]
public class SwapEffectConfig
{
    public string NameFirstEffects;
    public string NameSecondEffects;
    public string NameNewEffect;
    public SwapEffectActions SwapEffectActions;
}