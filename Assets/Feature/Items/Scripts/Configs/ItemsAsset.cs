﻿using System;
using System.Collections.Generic;
using Architecture.AssetsManager;
using Feature.Items.Scripts.Abstract;
using UnityEditor;
using UnityEngine;

namespace Feature.Items
{
    [CreateAssetMenu(menuName = "Feature/Items/ItemsAsset")]
    public class ItemsAsset : AConfigAsset
    {
        
        [SerializeReference]
        private AbstractItemConfig[] Items;

        public List<AbstractCategory> Categories;

        public AbstractItemConfig GetItemConfig(int id)
        {
            return Items[id];
        }

        public AbstractCategory GetCategory(CategoryName categoryName)
        {
            foreach (var category in Categories)
            {
                if (category.CategoryName == categoryName)
                    return category;
            }

            return null;
        }

#if UNITY_EDITOR
        [ContextMenu("Sort")]
        private void Sort()
        {
            int maxId = 0;
            foreach (var category in Categories)
            {
                category.SetCategory();
                
                var list = category.GetListItems();
                if(list == null)
                    continue;

                foreach (var item in list)
                {
                    if(item == null)
                        continue;
                    if (item.ConfigId > maxId)
                        maxId = item.ConfigId;
                }
            }
            maxId++;
            var newItems = new AbstractItemConfig[maxId];
            foreach (var category in Categories)
            {
                var list = category.GetListItems();
                if(list == null)
                    continue;
                
                foreach (var item in list)
                {
                    if(item == null)
                        continue;
                
                    if (newItems[item.ConfigId] != null)
                    {
                        Debug.LogError($" {item.ConfigId} more one");
                        continue;
                    }

                    newItems[item.ConfigId] = item;
                }
            }
            Items = newItems;
            
            EditorUtility.SetDirty(this);
        }
#endif
        
    }
}