﻿using System;
using Newtonsoft.Json;

namespace Feature.Items.Scripts.Abstract
{
    [Serializable]
    public class AbstractItemData
    {
        [JsonIgnore]
        [NonSerialized]
        public AbstractItemConfig ItemConfig;
        
        public int IdItemConfig;
        

        public int IdItem;
    }
}