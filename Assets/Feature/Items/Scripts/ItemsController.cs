﻿using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using UnityEngine;

namespace Feature.Items.Scripts
{
    public class ItemsController : AController
    {
        public static ItemsController Instance;
        
        [SerializeField]
        private ItemsModelOld modelOld;

        private List<IItemFactory> _factories;
        public override void Initialize()
        {
            base.Initialize();
            var controllers = GetComponentsInChildren<AController>();
            _factories = new List<IItemFactory>();
            foreach (var controller in controllers)
            {
                if (controller is IItemFactory factory)
                {
                    _factories.Add(factory);
                }
            }

            Instance = this;
        }

        public AbstractItemData LoadItem(int itemId)
        {
            AbstractItemData itemData = null;
            foreach (var factory in _factories)
            {
               itemData = factory.LoadItem(itemId);
               if(itemData != null)
                   break;
            }

            return itemData;
        }

        public AbstractItemData CreateItem(int configId)
        {
            AbstractItemData itemData = null;
            var itemId = modelOld.GetLastId() +1;
            
            foreach (var factory in _factories)
            {
                itemData = factory.CreateItem(configId, itemId);
                if(itemData != null)
                    break;
            }

            if (itemData == null)
            {
                Debug.Log($"не получилось создать предмет {configId} так как нет фабрики");
            }

            modelOld.SetLastId(itemId);
            return itemData;
        }

        public void SaveData(EquipItemData selectedItem)
        {
            foreach (var factory in _factories)
            {
                var isBelongs = factory.IsBelongs(selectedItem.IdItemConfig);
                if (isBelongs)
                {
                    factory.SaveItem(selectedItem);
                    break;
                }

               
            }
        }
    }

    public interface IItemFactory
    {
        public bool IsBelongs(int configId);
        public AbstractItemData CreateItem(int configId, int itemId);
        public void SaveItem(AbstractItemData itemData);
        public AbstractItemData LoadItem(int itemId);
    }
}