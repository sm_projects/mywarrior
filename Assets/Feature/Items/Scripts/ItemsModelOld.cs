﻿using Architecture.MCOV;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Items.Scripts
{
    public class ItemsModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public int GetLastId()
        {
            var data = LoadObjectInfo<HeroLastIdItemData>(HeroId);
            if (data == null)
                return 0;
            return data.LastId;
        }

        public void SetLastId(int newLastId)
        {
            SaveObjectInfo(HeroId, new HeroLastIdItemData(){LastId = newLastId});
        }
        
        public class HeroLastIdItemData
        {
            public int LastId;
        }
    }
}