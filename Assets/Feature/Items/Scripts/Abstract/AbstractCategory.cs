﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Feature.Items.Scripts.Abstract
{
    public abstract class AbstractCategory: ScriptableObject
    {
        public CategoryName CategoryName;
        
        public string KeyName;

        public bool HideIsEmpty;

        public bool IsImprove = true;

        public virtual AbstractItemConfig[] GetListItems()
        {
            return null;
        }
        

        
#if UNITY_EDITOR
        

        public virtual void SetCategory()
        {
            
        }

#endif

    }
    
    public enum CategoryName
    {
            Equip,
            LootBox,
            Crystal,
            Ticket
    }
}