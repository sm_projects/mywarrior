﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Feature.Items.Scripts.Abstract
{
    [Serializable]
    public abstract class AbstractCategoryItem<T> : AbstractCategory where T: AbstractItemConfig
    {
        public T[] ListConfigItems;

        public override AbstractItemConfig[] GetListItems()
        {
            return ListConfigItems;
        }

        public T GetItemConfig(int id)
        {
            foreach (var config in ListConfigItems)
            {
                if (config.ConfigId == id)
                    return config;
            }

            return null;
        }

#if UNITY_EDITOR
        [ContextMenu("SetCategory")]
        public override void SetCategory()
        {
            base.SetCategory();
            foreach (var itemConfig in ListConfigItems)
            {
                itemConfig.Category = CategoryName;
            }

            EditorUtility.SetDirty(this);
        }
        
        
        
#endif
    }
}