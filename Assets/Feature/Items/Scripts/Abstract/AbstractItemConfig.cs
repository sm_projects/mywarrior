﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Items.Scripts.Abstract
{
    [Serializable]
    public abstract class AbstractItemConfig 
    {
        public string NameKey;
        public int ConfigId;
        public Sprite Icon;
        
        
        public CategoryName Category;
       
        
        public string DescriptionKey;

        public bool IsStackable ;
    }
}