﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Items.Systems.TicketFactorySystem.Scripts.Data;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Items.Systems.TicketFactorySystem.Scripts.MCOV
{
    public class TicketFactoryModelOld : AModelOld
    {
        public TicketItemData LoadItems(int itemId)
        {
            var  data = LoadPlayerInfo<TicketsData>();
            if (data == null || data.listTickets == null)
            {
                return null;
            }

            foreach (var ticket in data.listTickets)
            {
                if (ticket.IdItem == itemId)
                    return ticket;
            }

            return null;
        }
        


        public void SaveItem(TicketItemData itemData)
        {
            var  data = LoadPlayerInfo<TicketsData>();
            if (data == null || data.listTickets == null)
            {
                data = new TicketsData();
                data.listTickets = new List<TicketItemData>();
            }
            
            foreach (var dataOld in data.listTickets)
            {
                if (dataOld.IdItem == itemData.IdItem)
                {
                    data.listTickets.Remove(dataOld);
                    break;
                }
            }
            
            data.listTickets.Add(itemData);
            
            SavePlayerInfo(data);
        }
    }
}