﻿using System;
using Feature.Items.Scripts.Abstract;

namespace Feature.Items.Systems.TicketFactorySystem.Scripts.Data
{
    [Serializable]
    public class TicketItemData: AbstractItemData
    {
        public int CountUse;
    }
}