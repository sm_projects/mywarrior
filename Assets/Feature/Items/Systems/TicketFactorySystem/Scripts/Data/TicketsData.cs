﻿using System;
using System.Collections.Generic;

namespace Feature.Items.Systems.TicketFactorySystem.Scripts.Data
{
    [Serializable]
    public class TicketsData
    {
        public List<TicketItemData> listTickets;
    }
}