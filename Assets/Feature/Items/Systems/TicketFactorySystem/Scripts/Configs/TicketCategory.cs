﻿using System;
using Feature.Items.Scripts.Abstract;
using UnityEngine;

namespace Feature.Items.Systems.TicketFactorySystem.Scripts.Configs
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/TicketCategory")]
    public class TicketCategory : AbstractCategoryItem<TicketItemConfig>
    {
    }
    
    [Serializable]
    public class TicketItemConfig : AbstractItemConfig
    {
        public int CountUse;
        public int IdBoos;
    }
}