﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Data;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.MCOV
{
    public class CrystalFactoryModelOld : AModelOld
    {
        public CrystalItemData LoadItems(int itemId)
        {
            var  data = LoadPlayerInfo<CrystalsData>();
            if (data == null || data.listCrystalItems == null)
            {
                return null;
            }

            foreach (var ticket in data.listCrystalItems)
            {
                if (ticket.IdItem == itemId)
                    return ticket;
            }

            return null;
        }
        


        public void SaveItem(CrystalItemData itemData)
        {
            var  data = LoadPlayerInfo<CrystalsData>();
            if (data == null || data.listCrystalItems == null)
            {
                data = new CrystalsData();
                data.listCrystalItems = new List<CrystalItemData>();
            }
            
            foreach (var dataOld in data.listCrystalItems)
            {
                if (dataOld.IdItem == itemData.IdItem)
                {
                    data.listCrystalItems.Remove(dataOld);
                    break;
                }
            }
            
            data.listCrystalItems.Add(itemData);
            
            SavePlayerInfo(data);
        }
    }
}