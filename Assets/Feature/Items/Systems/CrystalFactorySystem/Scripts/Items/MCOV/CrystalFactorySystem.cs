﻿using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Data;
using UnityEngine;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.MCOV
{
    public class CrystalFactorySystem : AController, IItemFactory
    {
        [SerializeField]
        private CrystalFactoryModelOld modelOld;

        [SerializeField]
        private CrystalCategory Category;
        
        public AbstractItemData CreateItem(int configId, int itemId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return null;

            var equipItemData = new CrystalItemData();
            equipItemData.IdItem = itemId;
            

            equipItemData.IdItemConfig = configId;
            equipItemData.ItemConfig = config;

            modelOld.SaveItem(equipItemData);

            return equipItemData;
        }
        
        
        public AbstractItemData CreateConcrete(int configId, int itemId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return null;

            var equipItemData = new CrystalItemData();
            equipItemData.IdItem = itemId;
            

            equipItemData.IdItemConfig = configId;
            equipItemData.ItemConfig = config;

            modelOld.SaveItem(equipItemData);

            return equipItemData;
        }
        public bool IsBelongs(int configId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return false;
            return true;
        }

        public void SaveItem(AbstractItemData itemData)
        {
            var data = itemData as CrystalItemData;
            modelOld.SaveItem(data);
        }

        public AbstractItemData LoadItem(int itemId)
        {
            var result = modelOld.LoadItems(itemId);
            if (result == null)
                return null;
           
            var config = Category.GetItemConfig(result.IdItemConfig);
            if (config == null)
                return null;

            result.ItemConfig = config;
            return result;
        }
    }

  
}