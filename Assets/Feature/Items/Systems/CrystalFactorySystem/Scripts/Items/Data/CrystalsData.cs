﻿using System;
using System.Collections.Generic;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Data
{
    [Serializable]
    public class CrystalsData
    {
        public List<CrystalItemData> listCrystalItems;
    }
}