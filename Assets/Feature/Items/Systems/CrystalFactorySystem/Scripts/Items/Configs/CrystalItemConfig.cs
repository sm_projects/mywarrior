﻿using System;
using System.Collections.Generic;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.CrystalFactorySystem.Systems.CrystalItemTypeSystem.Scripts;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs
{
    [Serializable]
    public class CrystalItemConfig : AbstractItemConfig
    {
        public string EffectName;

        public CrystalViewConfig CrystalViewConfig;

        public List<ComponentModificator> Stats;
        public CrystalItemType CrystalItemType;
    }

    [Serializable]
    public class CrystalViewConfig
    {
        public Material ViewMaterial;

        public Sprite ViewSprite;
    }
    


}