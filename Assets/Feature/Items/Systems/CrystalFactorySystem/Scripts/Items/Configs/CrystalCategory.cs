﻿using Feature.Items.Scripts.Abstract;
using UnityEngine;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/Crystal/CrystalCategory")]
    public class CrystalCategory: AbstractCategoryItem<CrystalItemConfig>
    {
        
    }
}