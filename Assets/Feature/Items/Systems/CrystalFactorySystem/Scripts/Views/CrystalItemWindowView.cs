﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Equippable;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.CrystalFactorySystem.Systems.CrystalItemTypeSystem.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Items.Systems.CrystalFactorySystem.Scripts.Views
{
    public class CrystalItemWindowView : AView
    {
        [Header("Item")]
        
        [SerializeField]
        private Text nameItem;

        [SerializeField]
        private Text decItem;
        
        [SerializeField]
        private Image imageItem;

        [Header("ItemType")]
        
        [SerializeField]
        private Image imageTypeEquip;
        
        [SerializeField]
        private Button buttonType;
        
        [Header("Stats")]
        
        [SerializeField]
        private Transform contentStats;
        
        [SerializeField]
        private EquipItemStat statTextPrefab;
        

        public event Action OnClickType; 
        
        protected override void Subscribe()
        {
            base.Subscribe();
            buttonType.onClick.AddListener(ClickType);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            buttonType.onClick.RemoveListener(ClickType);
        }

        public void Initialize(CrystalItemConfig itemConfig, CrystalItemTypeConfig crystalItemTypeConfig, List<string> listStats)
        {
            nameItem.text = itemConfig.NameKey;
            decItem.text = itemConfig.DescriptionKey;
            imageItem.sprite = itemConfig.Icon;

            imageTypeEquip.sprite = crystalItemTypeConfig.IconType;

            for (int i = 0; i < contentStats.childCount; i++)
            {
                Destroy(contentStats.GetChild(i).gameObject);
            }

            foreach (var stat in listStats)
            {
                var viewStat = Instantiate(statTextPrefab, contentStats);
                viewStat.Initialize(stat);
            }
        }

        private void ClickType()
        {
            OnClickType?.Invoke();
        }
    }
}