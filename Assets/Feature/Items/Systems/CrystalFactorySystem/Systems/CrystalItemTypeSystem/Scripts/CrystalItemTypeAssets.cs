﻿using System;
using System.Collections.Generic;
using Architecture.AssetsManager;
using UnityEngine;

namespace Feature.Items.Systems.CrystalFactorySystem.Systems.CrystalItemTypeSystem.Scripts
{

    [CreateAssetMenu(menuName = "Feature/Items/Category/Crystal/CrystalItemTypeAssets")]
    public class CrystalItemTypeAssets : AViewAsset
    {
        
        public List<CrystalItemTypeConfig> Configs;

        public CrystalItemTypeConfig GetConfig(CrystalItemType equipItemType)
        {
            foreach (var config in Configs)
            {
                if (config.CtystalItemType == equipItemType)
                    return config;
            }

            return null;
        }
    }
    
    [Serializable]
    public class CrystalItemTypeConfig
    {
        public CrystalItemType CtystalItemType;
        public Sprite IconType;
        public string Decription;
    }

    public enum CrystalItemType
    {
        Ice,
        Water,
        Fire,
        Poison,
        Stone,
        Dark,
        Lightning,
        Light
        
    }
}