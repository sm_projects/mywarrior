﻿using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Equippable
{
    public class EquipmentViewObject : MonoBehaviour
    {
        [SerializeField]
        private Image image;

        public void SetCrystal(CrystalViewConfig crystalViewConfig)
        {
            if (crystalViewConfig == null)
            {
                image.material = null;
                return;
            }
            else
            {
                image.material = crystalViewConfig.ViewMaterial;
            }
        }
    }
}