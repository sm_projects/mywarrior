﻿using System.Collections.Generic;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;

namespace DefaultNamespace.Equippable
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/Equip/EquipmentViewConfig")]
    public class EquipmentViewConfig : ScriptableObject
    {
        public List<Part> PartsToDisabled;
        public EquipmentViewObject EquipmentViewObject;
    }

    public enum Part
    {
        Head,
        Bread,
        Eyes,
        Body,
        FootL,
        FootR,
        HandL,
        HandR,
    }
}