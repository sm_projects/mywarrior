﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Equippable;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.Views
{
    public class EquipItemWindowView : AView
    {
        [Header("Item")]
        
        [SerializeField]
        private Text nameItem;

        [SerializeField]
        private Text decItem;
        
        [SerializeField]
        private Image imageItem;

        [Header("ItemType")]
        
        [SerializeField]
        private Image imageTypeEquip;
        
        [SerializeField]
        private Button buttonType;
        
        [Header("Stats")]
        
        [SerializeField]
        private Transform contentStats;
        
        [SerializeField]
        private EquipItemStat statTextPrefab;
        
        [Header("Crystal")]
        
        [SerializeField]
        private Image imageCrystal;
        
        [SerializeField]
        private Sprite defaultSpriteCrystal;
        
        [SerializeField]
        private Button buttonCrystal;


        public event Action OnClickType; 
        
        public event Action OnClickCrystal; 

        protected override void Subscribe()
        {
            base.Subscribe();
            buttonCrystal.onClick.AddListener(ClickCrystal);
            buttonType.onClick.AddListener(ClickType);
        }



        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            buttonCrystal.onClick.RemoveListener(ClickCrystal);
            buttonType.onClick.RemoveListener(ClickType);

        }

        public void Initialize(EquipItemConfig itemConfig, EquipItemTypeConfig equipItemTypeConfig, List<string> listStats, CrystalItemConfig crystalConfig = null)
        {
            nameItem.text = itemConfig.NameKey;
            decItem.text = itemConfig.DescriptionKey;
            imageItem.sprite = itemConfig.Icon;

            imageTypeEquip.sprite = equipItemTypeConfig.IconType;

            for (int i = 0; i < contentStats.childCount; i++)
            {
                Destroy(contentStats.GetChild(i).gameObject);
            }

            foreach (var stat in listStats)
            {
                var viewStat = Instantiate(statTextPrefab, contentStats);
                viewStat.Initialize(stat);

            }

            if (crystalConfig == null)
            {
                imageItem.material = null;
                imageCrystal.sprite = defaultSpriteCrystal;
            }
            else
            {
                imageItem.material = crystalConfig.CrystalViewConfig.ViewMaterial;
                imageCrystal.sprite = crystalConfig.Icon;
            }

        }

        private void ClickType()
        {
            OnClickType?.Invoke();
        }

        private void ClickCrystal()
        {
            OnClickCrystal?.Invoke();
        }
    }
}