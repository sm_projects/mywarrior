﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Equippable
{
    public class EquipItemStat : AView
    {
        [SerializeField]
        private Text text;


        public void Initialize(string textStat)
        {
            text.text = textStat;
        }
    }
}