﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.MCOV
{
    public class EquipFactoryModelOld : AModelOld
    {

        public EquipItemData LoadItems(int itemId)
        {
            var  data = LoadPlayerInfo<EquipsData>();
            if (data == null || data.listEquipItems == null)
            {
                return null;
            }

            foreach (var ticket in data.listEquipItems)
            {
                if (ticket.IdItem == itemId)
                    return ticket;
            }

            return null;
        }


        public void AddItem(EquipItemData itemData)
        {
            var  data = LoadPlayerInfo<EquipsData>();
            if (data == null || data.listEquipItems == null)
            {
                data = new EquipsData();
                data.listEquipItems = new List<EquipItemData>();
            }


            foreach (var dataOld in data.listEquipItems)
            {
                if (dataOld.IdItem == itemData.IdItem)
                {
                    data.listEquipItems.Remove(dataOld);
                    break;
                }
            }

            data.listEquipItems.Add(itemData);
            
            SavePlayerInfo(data);
        }
    }
}