﻿using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Skills.Scripts;
using UnityEngine;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.MCOV
{
    public class EquipFactorySystem : AController, IItemFactory
    {
        [SerializeField]
        private EquipFactoryModelOld modelOld;

        [SerializeField]
        private EquipCategory Category;
        

        

        public AbstractItemData CreateItem(int configId, int itemId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return null;

            var equipItemData = new EquipItemData();
            equipItemData.CrystalId = -1;
            equipItemData.IdItem = itemId;
            equipItemData.IdItemConfig = configId;
            equipItemData.ItemConfig = config;

            modelOld.AddItem(equipItemData);

            return equipItemData;
        }
        
        public bool IsBelongs(int configId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return false;
            return true;
        }

        public void SaveItem(AbstractItemData itemData)
        {
            var data = itemData as EquipItemData;
            modelOld.AddItem(data);
        }


        

        public AbstractItemData LoadItem(int itemId)
        {
            var result = modelOld.LoadItems(itemId);
            if (result == null)
                return null;
           
            var config = Category.GetItemConfig(result.IdItemConfig);
            if (config == null)
                return null;
            

            result.ItemConfig = config;
            return result;
        }
    }
}