﻿using System;
using System.Collections.Generic;
using DefaultNamespace.Equippable;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.Configs
{
    [Serializable]
    public class EquipItemConfig : AbstractItemConfig
    {
        public EquipmentViewConfig PrefabView;
        public EquipmentType EquipmentType;
        public EquipItemType EquipItemType;
        
        public string SkillName;
        public List<ComponentModificator> Stats;
    }
    
    public enum EquipmentType
    {
        Hat,
        Armor,
        Scarf,
        WeaponL,
        WeaponR,
        Pet
    }
}