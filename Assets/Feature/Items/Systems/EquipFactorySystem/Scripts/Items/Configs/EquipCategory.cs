﻿using System.Collections.Generic;
using DefaultNamespace.Equippable;
using Feature.Items.Scripts.Abstract;
using UnityEditor;
using UnityEngine;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.Configs
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/Equip/EquipCategory")]
    public class EquipCategory : AbstractCategoryItem<EquipItemConfig>
    {
    }


    
}