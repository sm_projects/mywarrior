﻿using System;
using System.Collections.Generic;

namespace Feature.Items.Systems.EquipFactorySystem.Scripts.Data
{
    [Serializable]
    public class EquipsData
    {
        public List<EquipItemData> listEquipItems;
    }
}