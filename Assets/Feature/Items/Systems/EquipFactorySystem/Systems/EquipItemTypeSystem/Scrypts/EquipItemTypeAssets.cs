﻿using System;
using System.Collections.Generic;
using Architecture.AssetsManager;
using UnityEngine;

namespace Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/Equip/EquipItemTypeAssets")]
    public class EquipItemTypeAssets : AViewAsset
    {
        
        public List<EquipItemTypeConfig> Configs;

        public EquipItemTypeConfig GetConfig(EquipItemType equipItemType)
        {
            foreach (var config in Configs)
            {
                if (config.EquipItemType == equipItemType)
                    return config;
            }

            return null;
        }
    }
    
    [Serializable]
    public class EquipItemTypeConfig
    {
        public EquipItemType EquipItemType;
        public Sprite IconType;
        public string Decription;
    }

    public enum EquipItemType
    {
        WeaponLong,
        WeaponBig,
        WeaponSmile,
        ArmorBlackWarrior,
        ArmorGreenWarrior,
        ArmorBrownMagician,
        ArmorGreenMagician,
        HatBlackMagician,
        HatBlackWarrior,
        HatBrownMagician,
        HatGreenMagician,
        HatGrayWarrior,
    }
}