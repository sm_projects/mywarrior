﻿using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.TicketFactorySystem.Scripts.Data;
using UnityEngine;

namespace Feature.Items.Systems.LootBoxFactorySystem.Scripts.MCOV
{
    public class LootBoxFactorySystem : AController, IItemFactory
    {
        [SerializeField]
        private LootBoxFactoryModelOld modelOld;

        [SerializeField]
        private LootBoxCategory Category;
        
        public AbstractItemData CreateItem(int configId, int itemId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return null;

            var ticketItemData = new LootBoxItemData();
            ticketItemData.IdItem = itemId;

            ticketItemData.IdItemConfig = configId;
            ticketItemData.ItemConfig = config;
            

            modelOld.SaveItem(ticketItemData);

            return ticketItemData;
        }
        
        public bool IsBelongs(int configId)
        {
            var config = Category.GetItemConfig(configId);
            if (config == null)
                return false;
            return true;
        }

        public void SaveItem(AbstractItemData itemData)
        {
            var data = itemData as LootBoxItemData;
            modelOld.SaveItem(data);
        }

        public AbstractItemData LoadItem(int itemId)
        {
            var result = modelOld.LoadItems(itemId);
            if (result == null)
                return null;
           
            var config = Category.GetItemConfig(result.IdItemConfig);
            if (config == null)
                return null;

            result.ItemConfig = config;
            return result;
        }
    }
}