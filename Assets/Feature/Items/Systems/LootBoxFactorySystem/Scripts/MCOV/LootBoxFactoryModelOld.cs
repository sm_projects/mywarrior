﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Items.Systems.TicketFactorySystem.Scripts.Data;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Items.Systems.LootBoxFactorySystem.Scripts.MCOV
{
    public class LootBoxFactoryModelOld : AModelOld
    {

        public LootBoxItemData LoadItems(int itemId)
        {
            var  data = LoadPlayerInfo<LootBoxesData>();
            if (data == null || data.listLootBoxes == null)
            {
                return null;
            }

            foreach (var lootBoxItemData in data.listLootBoxes)
            {
                if (lootBoxItemData.IdItem == itemId)
                    return lootBoxItemData;
            }

            return null;
        }
        


        public void SaveItem(LootBoxItemData itemData)
        {
            var  data = LoadPlayerInfo<LootBoxesData>();
            if (data == null || data.listLootBoxes == null)
            {
                data = new LootBoxesData();
                data.listLootBoxes = new List<LootBoxItemData>();
            }
            
            foreach (var dataOld in data.listLootBoxes)
            {
                if (dataOld.IdItem == itemData.IdItem)
                {
                    data.listLootBoxes.Remove(dataOld);
                    break;
                }
            }
            
            data.listLootBoxes.Add(itemData);
            
            SavePlayerInfo(data);
        }
    }
}