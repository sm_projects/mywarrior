﻿using System;
using Feature.Items.Scripts.Abstract;
using UnityEngine;
using Utilits;

namespace Feature.Items.Systems.LootBoxFactorySystem.Scripts
{
    [CreateAssetMenu(menuName = "Feature/Items/Category/LootBoxCategory")]
    public class LootBoxCategory: AbstractCategoryItem<LootBoxItemConfig>
    {
        
    }
    
    
    [Serializable]
    public class LootBoxItemConfig : AbstractItemConfig
    {
        [Header("Items")] public GenerateObject<int> GenerateItems;


        [Header("Currency")] public GenerateObject<Currency> GenerateCurrencys;
    }
    
    public enum Currency
    {
        Gold,
        Silver,
        Diamond
    }
    
    
    
}