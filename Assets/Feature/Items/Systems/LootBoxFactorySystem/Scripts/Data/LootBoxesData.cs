﻿using System;
using System.Collections.Generic;

namespace Feature.Items.Systems.LootBoxFactorySystem.Scripts.MCOV
{
    [Serializable]
    public class LootBoxesData
    {
        public List<LootBoxItemData> listLootBoxes;
    }
}