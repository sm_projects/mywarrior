﻿using System;
using System.Collections.Generic;
using Architecture.AssetsManager;
using UnityEngine;

namespace Feature.SceneLoader.Configs
{
    [CreateAssetMenu(menuName = "Feature/SceneLoader/SceneAssets")]
    public class SceneAssets : AConfigAsset
    {
        public string StartSceneName;
        
        public string MenuSceneName;

        public string FightSceneName;

    }
}