﻿using System;
using Architecture.DataManager;
using Architecture.ModelManager;

namespace Feature.SceneLoader.Scripts
{
    public class SceneLoaderModel : AModel, IDataProvider<CurrentSceneData>
    {
        public event Action OnLoadStartScene;
        public event Action OnLoadHomeScene;
        public event Action OnLoadFightScene;
        
        private DataProvider<CurrentSceneData> _sceneLoaderData;
        
         
        
        public void SetDataProvider(DataProvider<CurrentSceneData> provider)
        {
            _sceneLoaderData = provider;
        }

        public void LoadStartScene(string sceneName)
        {
            _sceneLoaderData.GetData.NameScene = sceneName;
            OnLoadStartScene?.Invoke();
        }
        
        public void LoadHomeScene(string sceneName)
        {
            _sceneLoaderData.GetData.NameScene = sceneName;
            OnLoadHomeScene?.Invoke();
        }
        
        public void LoadFightScene(string sceneName)
        {
            _sceneLoaderData.GetData.NameScene = sceneName;
            OnLoadFightScene?.Invoke();
        }


    }

    [Serializable]
    public class CurrentSceneData : AData
    {
        public string NameScene;

    }
    
}