using System;
using System.Collections;
using Architecture.ControllerManager;
using Architecture.ModelManager;
using Feature.SceneLoader.Configs;
using Feature.SceneLoader.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoaderController : AController,IModelProvider<SceneLoaderModel>
{
    public event Action<string> OnStartLoad;
    public event Action<float> OnChangeProgress;
    public event Action OnSceneLoaded;

    private SceneAssets _sceneAssets;

    private SceneLoaderModel _model;
    
    public void SetModel(SceneLoaderModel model)
    {
        _model = model;
    }
    
    public override void Initialize()
    {
        base.Initialize();
        _sceneAssets = GetConfig<SceneAssets>();
    }

    public override void ArchitectureInitialized()
    {
        base.ArchitectureInitialized();
        StartCoroutine(LoadAsyncScene(_sceneAssets.StartSceneName));
    }

    public void ClickLoadHome()
    {
        StartCoroutine(LoadAsyncScene(_sceneAssets.MenuSceneName));
    }
    public void ClickLoadFight()
    {
        StartCoroutine(LoadAsyncScene(_sceneAssets.FightSceneName));
    }


    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        

        if (sceneName != _sceneAssets.StartSceneName)
        {
            OnStartLoad?.Invoke(sceneName);
        }
        
        while (!asyncLoad.isDone)
        {
            yield return null;
            OnChangeProgress?.Invoke(asyncLoad.progress);
        }

        if (sceneName == _sceneAssets.StartSceneName)
        {
            _model.LoadStartScene(sceneName);
        }
        
        if (sceneName == _sceneAssets.FightSceneName)
        {
            _model.LoadFightScene(sceneName);
        }
        
        if (sceneName == _sceneAssets.MenuSceneName)
        {
            _model.LoadHomeScene(sceneName);
        }


        
        
        
        OnSceneLoaded?.Invoke();
    }



}