﻿using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.SceneLoader.Scripts
{
    public class SceneLoaderObject : AObject<SceneLoaderController>
    {
        
        [SerializeField]
        private Image loadImage;
        
        [SerializeField]
        private Text progressText;
        

        [SerializeField]
        private Animator animator;
        
        public override void Subscribe()
        {
            Controller.OnStartLoad += SceneLoad;
            Controller.OnChangeProgress += ChangeProgress;
            Controller.OnSceneLoaded += SceneLoaded;
        }

        public override void Unsubscribe()
        {
            Controller.OnStartLoad -= SceneLoad;
            Controller.OnChangeProgress -= ChangeProgress;
            Controller.OnSceneLoaded -= SceneLoaded;
        }

        private void SceneLoad(string text)
        {
            animator.Play("StartLoad");
            ChangeProgress(0);
        }
        
        private void SceneLoaded()
        {
            animator.Play("EndLoad");
        }
        
        private void ChangeProgress(float progress)
        {
            progressText.text = (int)progress + "%";
            loadImage.fillAmount = progress;
        }
        

    }
}