﻿using System;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Data;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Windows.CrystalWindow.Systems.CrystalInventorySystem;
using Feature.Windows.CrystalWindow.Systems.CrystalItemSystem;
using UnityEngine;

namespace Feature.Windows.CrystalWindow.Scripts.MCOV
{
    public class CrystalWindowController : AController
    {
        public event Action OnShowWindow;

        [SerializeField]
        private CrystalInventorySystem crystalInventorySystem;

        [SerializeField]
        private CrystalItemSystem crystalItemSystem;
        

        [SerializeField]
        private ItemsAsset itemsAsset;
        
        
        private EquipItemData _itemData;
        private CrystalItemData _crystalData;

        public override void Subscribe()
        {
            base.Subscribe();

            //EventManager.Subscribe<OpenCrystalWindowEvent>(OpenCrystalWindow);
            
            crystalInventorySystem.OnClickCell += ClickCell;
            
            crystalItemSystem.OnClickUnselectedItem += ClickUnselectedItem;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            
            //EventManager.Unsubscribe<OpenCrystalWindowEvent>(OpenCrystalWindow);
            
            crystalInventorySystem.OnClickCell -= ClickCell;
            crystalItemSystem.OnClickUnselectedItem -= ClickUnselectedItem;
            
            
           
        }

        /*
        
        private void OpenCrystalWindow(OpenCrystalWindowEvent obj)
        {
            OnShowWindow?.Invoke();
            RepaintDefault();

        }
        
        */
        
        private void ClickCell(CellViewParameters viewParameters)
        {
            if (_itemData != null)
            {
                SelectedCrystal(viewParameters);
            }
            else
            {
                SelectedItem(viewParameters);
            }
        }



        private void SelectedItem(CellViewParameters viewParameters)
        {
            if(viewParameters.ItemData == null)
                return;

            _itemData = viewParameters.ItemData as EquipItemData;
            _crystalData = null;

            CrystalItemConfig crystalConfig = null;
            
            if (_itemData.CrystalId == -1)
            {
                _crystalData = ItemsController.Instance.LoadItem(_itemData.CrystalId) as CrystalItemData;
                crystalConfig = _crystalData.ItemConfig as CrystalItemConfig;
            }
            
            crystalItemSystem.SelectedItem(_itemData.ItemConfig as EquipItemConfig, crystalConfig);
            crystalInventorySystem.OpenCrystalItems();
        }
        
        
        private void SelectedCrystal(CellViewParameters viewParameters)
        {
            crystalInventorySystem.OpenCrystalItems();
        }
        
        private void ClickUnselectedItem()
        {
            RepaintDefault();
        }

        private void RepaintDefault()
        {
            crystalItemSystem.OpenDefaultWindow();
            crystalInventorySystem.OpenEquipItems();

            _itemData = null;
            _crystalData = null;
        }
    }


}