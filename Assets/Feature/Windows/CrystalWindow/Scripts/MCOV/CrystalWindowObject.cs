﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.CrystalWindow.Scripts.MCOV
{
    public class CrystalWindowObject : AObject<CrystalWindowController>
    {
        [SerializeField]
        private GameObject panel;
        
        [SerializeField]
        private Button closeButton;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnShowWindow += ShowWindow;
            
            closeButton.onClick.AddListener(HideWindow);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnShowWindow -= ShowWindow;
            
            closeButton.onClick.RemoveListener(HideWindow);
        }
        
        private void ShowWindow()
        {
            panel.SetActive(true);
        }
        
        private void HideWindow()
        {
            panel.SetActive(false);
        }
    }
}