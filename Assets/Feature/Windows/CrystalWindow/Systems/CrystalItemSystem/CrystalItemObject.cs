﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.CrystalWindow.Systems.CrystalItemSystem
{
    public class CrystalItemObject : AObject<CrystalItemSystem>
    {
        [SerializeField]
        private Button itemButton;
        
        [SerializeField]
        private Image itemImage;
        
        [SerializeField]
        private Sprite defaultItemImage;
        
        
        
        [SerializeField]
        private Button crystalButton;
        
        [SerializeField]
        private Image crystalImage;
        
        [SerializeField]
        private Sprite defaultCrystalImage;

        public override void Subscribe()
        {
            base.Subscribe();

            Controller.OnSelectedItem += SelectedItem;
            Controller.OnShowDefaultWindow += OpenDefaultWindow;

            itemButton.onClick.AddListener(ClickUnselectedItem);
            crystalButton.onClick.AddListener(ClickUnselectedCrystal);
        }
        

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            
            Controller.OnSelectedItem -= SelectedItem;
            Controller.OnShowDefaultWindow -= OpenDefaultWindow;
            
            itemButton.onClick.RemoveListener(ClickUnselectedItem);
            crystalButton.onClick.RemoveListener(ClickUnselectedCrystal);
        }

        private void OpenDefaultWindow()
        {
            itemImage.sprite = defaultItemImage;
            crystalImage.sprite = defaultCrystalImage;
        }

        private void SelectedItem(EquipItemConfig itemConfig, CrystalItemConfig crystalConfig)
        {
            itemImage.sprite = itemConfig.Icon;
            if (crystalConfig == null)
            {
                crystalImage.sprite = defaultCrystalImage;
            }
            else
            {
                crystalImage.sprite = crystalConfig.Icon;
            }


        }
        
        private void ClickUnselectedItem()
        {
            Controller.ClickUnselectedItem();
        }
        
        private void ClickUnselectedCrystal()
        {
            Controller.ClickUnselectedCrystal();
        }
    }
}