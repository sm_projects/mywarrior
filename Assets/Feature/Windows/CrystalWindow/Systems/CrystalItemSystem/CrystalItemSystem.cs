﻿using System;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;

namespace Feature.Windows.CrystalWindow.Systems.CrystalItemSystem
{
    public class CrystalItemSystem : AController
    {
        public event Action<EquipItemConfig, CrystalItemConfig> OnSelectedItem;
        public event Action OnShowDefaultWindow;
        
        public event Action OnClickUnselectedItem;

        public event Action OnClickUnselectedCrystal;
        
        public void OpenDefaultWindow()
        {
            OnShowDefaultWindow?.Invoke();
        }

        public void SelectedItem(EquipItemConfig itemConfig, CrystalItemConfig crystalConfig)
        {
            OnSelectedItem?.Invoke(itemConfig, crystalConfig);
        }

        public void ClickUnselectedItem()
        {
            OnClickUnselectedItem?.Invoke();
        }

        public void ClickUnselectedCrystal()
        {
            OnClickUnselectedCrystal?.Invoke();
        }
    }
}