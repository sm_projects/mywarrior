﻿using System.Collections;
using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Windows.CrystalWindow.Systems.CrystalInventorySystem
{
    public class CrystalInventoryModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;

        public List<CellData> LoadCategoryData(CategoryName category)
        {
            var dates = LoadInventoryData();
            if (dates == null)
            {
                return null;
            }

            foreach (var data in dates)
            {
                if (data.Category == category)
                {
                    return data.CellsOnCategory;
                }
            }

            return null;
        }
        
        public List<CategoryCellData> LoadInventoryData()
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);
            if (data == null)
            {
                return null;
            }

            return data.Categories;
        }
    }
}