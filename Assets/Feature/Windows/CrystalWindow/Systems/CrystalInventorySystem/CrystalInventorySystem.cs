﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Windows.CrystalWindow.Systems.CrystalInventorySystem
{
    public class CrystalInventorySystem: AController
    {
        public event Action OnOpenInventory;
        
        public event Action<List<CellViewParameters>> OnLoadItems;

        public event Action<CellViewParameters> OnClickCell;
        
        [SerializeField]
        private ItemsAsset itemsAsset;
        

        [SerializeField]
        private EquipCategory weaponCategory;
        
        [SerializeField]
        private CrystalCategory crystalCategory;

        [SerializeField]
        private CrystalInventoryModelOld modelOld;

        public void OpenEquipItems()
        {
            OnOpenInventory?.Invoke();
            var result = GetViewParametersCategory(weaponCategory.CategoryName);
            OnLoadItems?.Invoke(result);
        }
        
        public void OpenCrystalItems()
        {
            OnOpenInventory?.Invoke();
            var result = GetViewParametersCategory(crystalCategory.CategoryName);
            OnLoadItems?.Invoke(result);
        }
        
        public void ClickCell(CellViewParameters item)
        {
            OnClickCell?.Invoke(item);
        }
        
        private List<CellViewParameters> GetViewParametersCategory(CategoryName category)
        {
            var cellsData = modelOld.LoadCategoryData(category);
            
            if (cellsData == null || cellsData.Count == 0)
            {
                //OnSelectionCategory?.Invoke(null);
                return null;
            }

            List<CellViewParameters> result = new List<CellViewParameters>();
            
            foreach (var cellData in cellsData)
            {
                if (cellData == null || cellData.ItemsInCell == null)
                {
                    continue;
                }
                
                var firstItemId = cellData.ItemsInCell[0];
                var cellViewParameters = new CellViewParameters();
                 
                
                var item = ItemsController.Instance.LoadItem(firstItemId);
                var config = itemsAsset.GetItemConfig(item.IdItemConfig);
                cellViewParameters.CellSprite = config.Icon;
                cellViewParameters.DescriptionCell = cellData.ItemsInCell.Count.ToString();
                cellViewParameters.IsCheckedCell = true;
                cellViewParameters.ItemConfig = config;
                cellViewParameters.ItemData = item;
                cellViewParameters.CellId = cellData.IdCell;
                
                result.Add(cellViewParameters);
            }

            return result;
            
        }

  
    }
}