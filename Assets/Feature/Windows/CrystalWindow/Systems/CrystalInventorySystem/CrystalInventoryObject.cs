﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.CrystalWindow.Systems.CrystalInventorySystem
{
    public class CrystalInventoryObject : AObject<CrystalInventorySystem>
    {
        [SerializeField]
        private InventoryPanelView inventoryPanelView;
        
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenInventory += Show;
            Controller.OnLoadItems += RepaintItemsPanel;

            inventoryPanelView.OnClickItem += ClickItem;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenInventory -= Show;
            Controller.OnLoadItems -= RepaintItemsPanel;
            
            
            inventoryPanelView.OnClickItem -= ClickItem;
        }
        
        private void Show()
        {
            inventoryPanelView.Clear();
        }
        
        private void RepaintItemsPanel(List<CellViewParameters> listItems)
        {
            if (listItems == null || listItems.Count == 0)
            {
                inventoryPanelView.SpawnItems(null);
                return;
            }
            inventoryPanelView.SpawnItems(listItems);
            
        }
        
        private void ClickItem(CellViewParameters obj)
        {
            Controller.ClickCell(obj);
        }
    }
}