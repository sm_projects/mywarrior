﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.DebugMode;
using Feature.Inventory.Systems;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Windows.Inventory.Systems.InventorySkillsSystems;
using UnityEngine;

namespace Feature.Windows.Inventory.Scripts
{
    public class InventoryController : AController
    {

        [SerializeField]
        private InventoryModelOld modelOld;

        [SerializeField]
        private ItemsAsset ItemsAsset;
        
       
        public event Action OnOpenInventory;



        [SerializeField]
        private InventoryItemsSystem inventoryItemsSystem;

        [SerializeField]
        private InventoryEquipSystem inventoryEquipSystem;

        [SerializeField]
        private InventoryStatsSystems inventoryStatsSystems;

        [SerializeField]
        private InventorySkillsSystems inventorySkillsSystems;
 
        public override void Subscribe()
        {
            base.Subscribe();

            inventoryEquipSystem.OnEquipChange += EquipChange;
            
            //EventManager.Subscribe<OpenInventoryWindowEvent>(OpenInventory);
            
            //EventManager.Subscribe<NeedEquipItemEvent>(EquipItem);
            
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            
            inventoryEquipSystem.OnEquipChange -= EquipChange;
            
            //EventManager.Unsubscribe<OpenInventoryWindowEvent>(OpenInventory);
            
            
            //EventManager.Unsubscribe<NeedEquipItemEvent>(EquipItem);
        }
        
        private void EquipChange()
        {
            inventoryStatsSystems.EquipChange();
            inventorySkillsSystems.EquipChange();
        }
        
        
        /*
        private void EquipItem(NeedEquipItemEvent eventData)
        {
            inventoryEquipSystem.EquipItem(eventData);
        }
        

        private void OpenInventory(OpenInventoryWindowEvent windowEventData)
        {
            OnOpenInventory?.Invoke();
            inventoryItemsSystem.OpenInventoryItems();
            inventoryEquipSystem.OpenEquipItems();
            inventoryStatsSystems.OpenStats();
            inventorySkillsSystems.OpenSkillsWindow();
        }
        */




    }

   



}