﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Inventory.Scripts
{
    public class InventoryObject : AObject<InventoryController>
    {
        [SerializeField]
        private GameObject panel;

        [SerializeField]
        private Button close;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenInventory += OpenPanel;
            close.onClick.AddListener(ClickClose);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenInventory -= OpenPanel;
            close.onClick.RemoveListener(ClickClose);
        }
        
        private void ClickClose()
        {
            panel.SetActive(false);
        }

        private void OpenPanel()
        {
            panel.SetActive(true);
        }
    }

   
}