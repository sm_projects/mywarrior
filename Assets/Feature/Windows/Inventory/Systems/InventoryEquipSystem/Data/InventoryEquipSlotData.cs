﻿using System;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;

namespace Feature.Inventory.Systems.InventoryEquipSystem
{
    [Serializable]
    public class InventoryEquipSlotData
    {
        public EquipmentType EquipmentType;
        public int itemId;
    }
}