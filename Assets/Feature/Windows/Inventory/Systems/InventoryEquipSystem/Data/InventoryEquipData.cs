﻿using System;
using System.Collections.Generic;

namespace Feature.Inventory.Systems.InventoryEquipSystem
{
    [Serializable]
    public class InventoryEquipData
    {
        public List<InventoryEquipSlotData> slots;
    }
}