﻿using System;
using Architecture.ControllerManager;
using Architecture.MCOV;
using DefaultNamespace;
using DefaultNamespace.Equippable;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using UnityEngine;

namespace Feature.Inventory.Systems.InventoryEquipSystem
{
    public class InventoryEquipSystem : AController
    {

        public event Action<EquipmentType, CellViewParameters, EquipmentViewConfig, CrystalViewConfig> OnLoadEquipItem;
        public event Action<AppearanceParameters> OnLoadAppearance;

        public event Action OnEquipChange;

        [SerializeField]
        private ItemsAsset itemsAsset;

        
        [SerializeField] 
        private AllPartAssets allPartAssets;

        [SerializeField]
        private InventoryEquipModelOld modelOld;
        private ItemsController ItemsController => ItemsController.Instance;


        [SerializeField]
        private InventoryUnEquipItemWindowSystem inventoryUnEquipItemWindowSystem;

        public void ClickEquipSlot(EquipmentType itemType)
        {
           var itemId = modelOld.LoadEquipmentPart(itemType);
           if (itemId != -1)
           {
               //UnEquipItem(itemId);
               inventoryUnEquipItemWindowSystem.OpenWindow(itemId);
           }
        }

        public void OpenEquipItems()
        {
            RepaintEquipItem(EquipmentType.WeaponL);
            RepaintEquipItem(EquipmentType.WeaponR);
            RepaintEquipItem(EquipmentType.Hat);
            RepaintEquipItem(EquipmentType.Scarf);
            RepaintEquipItem(EquipmentType.Armor);

            RepaintAppearance();
        }
        
        /*
        public void EquipItem(NeedEquipItemEvent eventData)
        {
            int newItemId = eventData.ItemId;
            var newItemData = ItemsController.LoadItem(newItemId) as EquipItemData;
            var newConfig = itemsAsset.GetItemConfig(newItemData.IdItemConfig) as EquipItemConfig;

            int oldItemId = modelOld.LoadEquipmentPart(newConfig.EquipmentType);

            modelOld.SetEquipmentPart(newConfig.EquipmentType, newItemId);
            
            if (oldItemId != -1)
            {
                var oldItemData = ItemsController.LoadItem(oldItemId);
                //EventManager.PushEvent(new AddItemToInventoryEvent(oldItemData, false));
            }

            OnEquipChange?.Invoke();
            RepaintEquipItem(newConfig.EquipmentType);
        }
        */

        private void RepaintAppearance()
        {
            var data = modelOld.LoadAppearance();
            
            var  appearance = new AppearanceParameters();
            appearance.Beard = allPartAssets.BeardsAsset.GetPart<BeardConfig>(data.BeardId);
            appearance.Eye = allPartAssets.EyeAsset.GetPart<EyeConfig>(data.EyeId);
            appearance.Skin = allPartAssets.SkinAsset.GetPart<SkinConfig>(data.SkinId);
            
            OnLoadAppearance?.Invoke(appearance);
        }


        private void UnEquipItem(int itemId)
        {
            var itemData = ItemsController.LoadItem(itemId) as EquipItemData;
            var itemConfig = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;
            int oldItemId = modelOld.LoadEquipmentPart(itemConfig.EquipmentType);

            modelOld.SetEquipmentPart(itemConfig.EquipmentType, -1);
            
            //EventManager.PushEvent(new AddItemToInventoryEvent(itemData, false));

            
            OnEquipChange?.Invoke();
            RepaintEquipItem(itemConfig.EquipmentType);
        }

       


        private void RepaintEquipItem(EquipmentType part)
        {
            int itemId = modelOld.LoadEquipmentPart(part);
            CellViewParameters cellViewParameters = null;
            EquipmentViewConfig viewPrefab = null;
            CrystalViewConfig crystalViewConfig = null;
            if (itemId != -1)
            {
                cellViewParameters = new CellViewParameters();
                var itemData = ItemsController.LoadItem(itemId) as EquipItemData;
                var config = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;
                viewPrefab = config.PrefabView;
                cellViewParameters.CellSprite = config.Icon;
                cellViewParameters.DescriptionCell = "";
                cellViewParameters.IsCheckedCell = true;
                cellViewParameters.ItemConfig = config;
                cellViewParameters.ItemData = itemData;
                cellViewParameters.CellId = (int)part;
                if (itemData.CrystalId != -1)
                {
                    var configCrystal = itemsAsset.GetItemConfig(itemData.CrystalId) as CrystalItemConfig;
                    crystalViewConfig = configCrystal.CrystalViewConfig;
                }

            }

            OnLoadEquipItem?.Invoke(part, cellViewParameters, viewPrefab, crystalViewConfig);
        }
    }


}