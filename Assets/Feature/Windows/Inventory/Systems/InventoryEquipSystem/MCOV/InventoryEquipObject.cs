﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Appearance;
using DefaultNamespace.Equippable;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;

namespace Feature.Inventory.Systems.InventoryEquipSystem
{
    public class InventoryEquipObject : AObject<InventoryEquipSystem>
    {
        [SerializeField]
        private SlotView slotWeaponL;
        
        [SerializeField]
        private SlotView slotWeaponR;
        
        [SerializeField]
        private SlotView slotHat;
        
        [SerializeField]
        private SlotView slotArmor;
        
        [SerializeField]
        private SlotView slotScarf;
        
        [SerializeField]
        private SlotView slotPet;

        [SerializeField]
        private AppearanceView appearance;


        private Dictionary<EquipmentType, SlotView> _slotViews;
        

        private void Awake()
        {
            _slotViews = new Dictionary<EquipmentType, SlotView>()
            {
                {EquipmentType.WeaponL, slotWeaponL},
                {EquipmentType.WeaponR, slotWeaponR},
                {EquipmentType.Hat, slotHat},
                {EquipmentType.Armor, slotArmor},
                {EquipmentType.Scarf, slotScarf},
                {EquipmentType.Pet, slotPet}
            };
        }
        

        public override void Subscribe()
        {
            base.Subscribe();

            Controller.OnLoadEquipItem += LoadEquipItem;
            Controller.OnLoadAppearance += LoadAppearance;
            
            slotWeaponL.OnClickItem += ClickWeaponL;
            slotWeaponR.OnClickItem += ClickWeaponR;
            slotHat.OnClickItem += ClickHat;
            slotArmor.OnClickItem += ClickArmor;
            slotScarf.OnClickItem += ClickScarf;
            slotPet.OnClickItem += ClickPet;

        }




        public override void Unsubscribe()
        {
            base.Unsubscribe();
            
            Controller.OnLoadEquipItem -= LoadEquipItem;
            Controller.OnLoadAppearance -= LoadAppearance;
            
            slotWeaponL.OnClickItem -= ClickWeaponL;
            slotWeaponR.OnClickItem -= ClickWeaponR;
            slotHat.OnClickItem -= ClickHat;
            slotArmor.OnClickItem -= ClickArmor;
            slotScarf.OnClickItem -= ClickScarf;
            slotPet.OnClickItem -= ClickPet;
        }
        
        
        private void LoadAppearance(AppearanceParameters parameters)
        {
            appearance.SetBeard(parameters.Beard);
            appearance.SetEye(parameters.Eye);
            appearance.SetSkin(parameters.Skin);
        }
        
        private void LoadEquipItem(EquipmentType part, CellViewParameters equipItem, EquipmentViewConfig viewPrefab, CrystalViewConfig crystalViewConfig)
        {
            appearance.Equip(part, viewPrefab, crystalViewConfig);
            _slotViews[part].Initialize(equipItem);
        }

        private void ClickPet()
        {
            Controller.ClickEquipSlot(EquipmentType.Pet);
        }

        private void ClickArmor()
        {
            Controller.ClickEquipSlot(EquipmentType.Armor);
        }
        
        private void ClickScarf()
        {
            Controller.ClickEquipSlot(EquipmentType.Scarf);
        }
        
        private void ClickHat()
        {
            Controller.ClickEquipSlot(EquipmentType.Hat);
        }

        private void ClickWeaponR()
        {
            Controller.ClickEquipSlot(EquipmentType.WeaponR);
        }

        private void ClickWeaponL()
        {
            Controller.ClickEquipSlot(EquipmentType.WeaponL);
        }
    }
}