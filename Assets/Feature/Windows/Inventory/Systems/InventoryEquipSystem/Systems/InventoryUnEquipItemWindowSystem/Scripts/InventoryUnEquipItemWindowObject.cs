﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Equippable;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Views;
using Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utilits;

namespace Feature.Windows.Inventory.Systems.InventoryEquipSystem.Systems.InventoryUnEquipItemWindowSystem.Scripts
{
    public class InventoryUnEquipItemWindowObject : AObject<global::InventoryUnEquipItemWindowSystem>
    {
        [Header("Default")]
        
        [SerializeField]
        private GameObject panel;
        
        [SerializeField]
        private Button close;

        [SerializeField]
        private EquipItemWindowView equipItemWindowView;
        
        [Header("Buttons")]
        
        [SerializeField]
        private Button unEquipButton;
        
        [SerializeField]
        private Button improveButton;

        private ComponentAssets _componentAssets;
        private EquipItemTypeAssets _equipItemTypeAssets;

        public override void Initialize()
        {
            base.Initialize();
            _componentAssets = GetViewAsset<ComponentAssets>();
            _equipItemTypeAssets = GetViewAsset<EquipItemTypeAssets>();
        }


        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenWindow += OpenWindow;
            unEquipButton.onClick.AddListener(ClickEquip);
            improveButton.onClick.AddListener(ClickImprove);
            close.onClick.AddListener(Close);

            equipItemWindowView.OnClickCrystal += ClickCrystal;
            equipItemWindowView.OnClickType += ClickType;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenWindow -= OpenWindow;
            unEquipButton.onClick.RemoveListener(ClickEquip);
            improveButton.onClick.RemoveListener(ClickImprove);
            close.onClick.RemoveListener(Close);

            equipItemWindowView.OnClickCrystal -= ClickCrystal;
            equipItemWindowView.OnClickType -= ClickType;
        }


        private void OpenWindow(EquipItemConfig itemConfig, CrystalItemConfig crystalConfig)
        {
            panel.SetActive(true);
            var listStats = new List<string>();

            var equipItemTypeConfig = _equipItemTypeAssets.GetConfig(itemConfig.EquipItemType);
            
            foreach (var stat in itemConfig.Stats)
            {
                var componentConfig = _componentAssets.GetComponentConfig(stat.Component);
                var stringStat = componentConfig.KeyName + " " + MathTypeOperation.GetChar(stat.MathType) + " " +
                                 stat.Value;
                listStats.Add(stringStat);
            }
            
            equipItemWindowView.Initialize(itemConfig, equipItemTypeConfig, listStats, crystalConfig);

        }
        
        private void ClickType()
        {
            Controller.ClickType();
        }

        private void ClickCrystal()
        {
            Controller.ClickCrystal();
            Close();
        }

        private void ClickImprove()
        {
            Controller.ClickImprove();
            Close();
        }
        
        private void ClickEquip()
        {
            Controller.ClickUnEquip();
            Close();
        }

        private void Close()
        {
            panel.SetActive(false);
        }
    }
}