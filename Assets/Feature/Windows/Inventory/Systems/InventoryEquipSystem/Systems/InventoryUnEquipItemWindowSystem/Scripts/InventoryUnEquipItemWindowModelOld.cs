﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Windows.Inventory.Systems.InventoryEquipSystem.Systems.InventoryUnEquipItemWindowSystem.Scripts
{
    public class InventoryUnEquipItemWindowModelOld : AModelOld
    {
        private int HeroId => LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public int LoadEquipmentPart(EquipmentType equipmentType)
        {
            var data = LoadObjectInfo<InventoryEquipData>(HeroId);
            if (data == null || data.slots == null)
                return -1;

            foreach (var slot in data.slots)
            {
                if (slot.EquipmentType == equipmentType)
                {
                    return slot.itemId;
                }
            }
            return -1;
        }
        
        public void SetEquipmentPart(EquipmentType newConfigEquipmentType, int newItemId)
        {
            var data = LoadObjectInfo<InventoryEquipData>(HeroId);
            if (data == null)
            {
                data = new InventoryEquipData();
                data.slots = new List<InventoryEquipSlotData>();
            }

            bool isSet = false;
            foreach (var slot in data.slots)
            {
                if (slot.EquipmentType == newConfigEquipmentType)
                {
                    slot.itemId = newItemId;
                    isSet = true;
                    break;
                }
            }

            if (!isSet)
            {
                var slot = new InventoryEquipSlotData();
                slot.EquipmentType = newConfigEquipmentType;
                slot.itemId = newItemId;
                data.slots.Add(slot);
            }

            SaveObjectInfo(HeroId ,data);
        }
    }
}