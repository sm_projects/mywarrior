using System;
using Architecture.ControllerManager;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Windows.Inventory.Systems.InventoryEquipSystem.Systems.InventoryUnEquipItemWindowSystem.Scripts;
using UnityEngine;

public class InventoryUnEquipItemWindowSystem : AController
{
    public event Action<EquipItemConfig, CrystalItemConfig> OnOpenWindow;

    private CrystalItemConfig _currentCrystalItemConfig;

    private EquipItemConfig _currentEquipItemConfig;

    private EquipItemData _currentEquipItemData;

    private ItemsAsset _itemsAsset;

    [SerializeField]
    private InventoryUnEquipItemWindowModelOld modelOld;
    
    private ItemsController ItemsController => ItemsController.Instance;

    public override void Initialize()
    {
        base.Initialize();
        _itemsAsset = GetConfig<ItemsAsset>();

    }

    public void EquipItem(int itemId)
    {
        //EventManager.PushEvent(new NeedEquipItemEvent(itemId));
        //EventManager.PushEvent(new RemoveItemFromInventoryEvent(itemId));
    }


    public void OpenWindow(int itemId)
    {
        _currentEquipItemData  = ItemsController.Instance.LoadItem(itemId) as EquipItemData;
        _currentEquipItemConfig = _currentEquipItemData.ItemConfig as EquipItemConfig;
        if (_currentEquipItemData.CrystalId == -1)
        {
            _currentCrystalItemConfig = null;
        }
        else
        {
            var crystalData = ItemsController.Instance.LoadItem(_currentEquipItemData.CrystalId);
            _currentCrystalItemConfig = _itemsAsset.GetItemConfig(crystalData.IdItemConfig) as CrystalItemConfig;
        }


        OnOpenWindow?.Invoke(_currentEquipItemConfig, _currentCrystalItemConfig);
    }

    public void ClickUnEquip()
    {
        var itemData = ItemsController.LoadItem(_currentEquipItemData.IdItem) as EquipItemData;
        var itemConfig = _itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;
        int oldItemId = modelOld.LoadEquipmentPart(itemConfig.EquipmentType);

        modelOld.SetEquipmentPart(itemConfig.EquipmentType, -1);
            
        //EventManager.PushEvent(new AddItemToInventoryEvent(itemData, false));

            
        //OnEquipChange?.Invoke();
       // RepaintEquipItem(itemConfig.EquipmentType);
    }

    public void ClickImprove()
    {
        throw new NotImplementedException();
    }

    public void ClickCrystal()
    {
        throw new NotImplementedException();
    }

    public void ClickType()
    {
        throw new NotImplementedException();
    }
}
