﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    public class InventoryItemsObject : AObject<InventoryItemsSystem>
    {
        [SerializeField]
        private ToggleGroup toggleGroup;

        [SerializeField]
        private TabView prefabTabView;

        [SerializeField]
        private InventoryPanelView inventoryPanelView;

        private Dictionary<InventoryCategoryInfo, TabView> _activeView;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenInventory += Show;
            Controller.OnAddTab += AddTab;
            Controller.OnChangeTab += ChangeTab;
            Controller.OnSelectionCategory += RepaintItemsPanel;
            Controller.OnChangeCell += ChangeCell;
            
            
            inventoryPanelView.OnClickItem += ClickItem;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenInventory -= Show;
            Controller.OnAddTab -= AddTab;
            Controller.OnChangeTab -= ChangeTab;
            Controller.OnSelectionCategory -= RepaintItemsPanel;
            Controller.OnChangeCell -= ChangeCell;
            
            inventoryPanelView.OnClickItem -= ClickItem;
        }

        private void ChangeCell(CellViewParameters obj)
        {
            inventoryPanelView.ChangeCell(obj);
        }
        
        private void ChangeTab(InventoryCategoryInfo tabInfo)
        {
            foreach (var tab in _activeView)
            {
                if (tab.Key.Category == tabInfo.Category)
                {
                    tab.Value.Initialize(tabInfo); 
                    tab.Value.gameObject.SetActive(!(tabInfo.HideIsEmpty && tabInfo.CountItems == 0));

                    break;
                }
            }
        }


        private void AddTab(InventoryCategoryInfo tabInfo)
        {
            var tab = Instantiate(prefabTabView, toggleGroup.transform);
            tab.Initialize(tabInfo);
            tab.SetToggleGroup(toggleGroup);
            tab.OnSelected += ()=> ClickTab(tabInfo);
            _activeView.Add(tabInfo, tab);
            if (tabInfo.HideIsEmpty && tabInfo.CountItems == 0)
            {
                tab.gameObject.SetActive(false);
            }

        }

        private void RepaintItemsPanel(List<CellViewParameters> listItems)
        {
            if (listItems == null || listItems.Count == 0)
            {
                inventoryPanelView.SpawnItems(null);
                return;
            }
            inventoryPanelView.SpawnItems(listItems);
            
        }

        private void ClickItem(CellViewParameters obj)
        {
            Controller.ClickCell(obj);
        }




        private void ClickTab(InventoryCategoryInfo tabInfo)
        {
            Controller.ClickTab(tabInfo);
        }

        private void Show()
        {
            if (_activeView != null)
            {
                foreach (var view in _activeView)
                {
                    view.Value.OnSelected -= () => ClickTab(view.Key);
                    Destroy(view.Value.gameObject);
                }
                _activeView.Clear();
            }
            else
            {
                _activeView = new Dictionary<InventoryCategoryInfo, TabView>();
            }
            
            inventoryPanelView.Clear();
        }
    }
}