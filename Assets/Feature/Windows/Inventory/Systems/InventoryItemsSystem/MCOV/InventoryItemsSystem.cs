﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.DebugMode;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem;
using Feature.Windows.Inventory.Scripts;
using Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems;
using Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryBoxItemWindowSystem;
using Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryCrystalItemWindowSystem;
using Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryEquipItemWindowSystem.Scripts;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    public class InventoryItemsSystem : AController
    {
        [SerializeField]
        private ItemsAsset itemsAsset;

        [SerializeField]
        private InventoryEquipItemWindowSystem equipWindow;


        [SerializeField]
        private InventoryCrystalItemWindowSystem crystalWindow;

        [SerializeField]
        private InventoryMoreItemWindowObject moreWindow;

        [SerializeField]
        private InventoryBoxItemWindowSystem boxWindow;
        
        [SerializeField]
        private InventoryItemsModelOld modelOld;
        
        
        public event Action<InventoryCategoryInfo> OnAddTab;
        public event Action<InventoryCategoryInfo> OnChangeTab;
        
        public event Action OnOpenInventory;

        public event Action<List<CellViewParameters>> OnSelectionCategory;
        public event Action<CellViewParameters> OnChangeCell;

        private CategoryName _currentCategory;

        public override void Subscribe()
        {
            base.Subscribe();
            //EventManager.Subscribe<AddItemToInventoryEvent>(AddItemToInventory);
            //EventManager.Subscribe<RemoveItemFromInventoryEvent>(RemoveItemFromInventory);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            //EventManager.Unsubscribe<AddItemToInventoryEvent>(AddItemToInventory);
            //EventManager.Unsubscribe<RemoveItemFromInventoryEvent>(RemoveItemFromInventory);
        }

        public void OpenInventoryItems()
        {
            OnOpenInventory?.Invoke();
            foreach (var category in itemsAsset.Categories)
            {
                var infoSystem = modelOld.LoadCategoryInfo(category.CategoryName);
                infoSystem.KeyName = category.KeyName;
                infoSystem.HideIsEmpty = category.HideIsEmpty;
                OnAddTab?.Invoke(infoSystem);
                _currentCategory = infoSystem.Category;
            }
        }

        public void ClickTab(InventoryCategoryInfo tabInfo)
        {
            LoadCategoryItems(tabInfo.Category);
            var infoSystem = modelOld.LoadCategoryInfo(tabInfo.Category);
            var categoryConfig = itemsAsset.GetCategory(tabInfo.Category);
            infoSystem.KeyName = categoryConfig.KeyName;
            infoSystem.HideIsEmpty = categoryConfig.HideIsEmpty;
            _currentCategory = tabInfo.Category;
            OnChangeTab?.Invoke(infoSystem);
        }
        
        
        public void ClickCell(CellViewParameters item)
        {
            switch (_currentCategory)
            {
                case CategoryName.Equip:
                {
                    equipWindow.OpenWindow(item);
                    break;
                }
                case CategoryName.Crystal:
                {
                    crystalWindow.OpenWindow(item);
                    break;
                }
                case CategoryName.LootBox:
                {
                    boxWindow.OpenWindow(item);
                    break;
                }

                default:
                {
                    moreWindow.OpenWindow(item);
                    break;
                }
            }
            
                
            item.IsCheckedCell = true;
            modelOld.SetCheckedCell(item.CellId);
            OnChangeCell?.Invoke(item);
            
            
            var infoSystem = modelOld.LoadCategoryInfo(item.ItemConfig.Category);
            var categoryConfig = itemsAsset.GetCategory(item.ItemConfig.Category);
            infoSystem.KeyName = categoryConfig.KeyName;
            infoSystem.HideIsEmpty = categoryConfig.HideIsEmpty;
            OnChangeTab?.Invoke(infoSystem);
            
        }
        
        

        private void LoadCategoryItems(CategoryName category)
        {
            var cellsData = modelOld.LoadCategoryData(category);
            
            if (cellsData == null || cellsData.Count == 0)
            {
                OnSelectionCategory?.Invoke(null);
                return;
            }

            List<CellViewParameters> result = new List<CellViewParameters>();
            
            foreach (var cellData in cellsData)
            {
                if (cellData == null || cellData.ItemsInCell == null)
                {
                    continue;
                }
                
                var firstItemId = cellData.ItemsInCell[0];
                var cellViewParameters = new CellViewParameters();
                 
                
                var item = ItemsController.Instance.LoadItem(firstItemId);
                var config = itemsAsset.GetItemConfig(item.IdItemConfig);
                var sprite = config.Icon;
                cellViewParameters.CellSprite = sprite;
                cellViewParameters.DescriptionCell = cellData.ItemsInCell.Count.ToString();
                cellViewParameters.IsCheckedCell = cellData.IsChecked;
                cellViewParameters.ItemConfig = config;
                cellViewParameters.ItemData = item;
                cellViewParameters.CellId = cellData.IdCell;
                
                result.Add(cellViewParameters);
            }

            //Model.SetCheckedCategory(category);
            OnSelectionCategory?.Invoke(result);
        }
        
        /*
        private void AddItemToInventory(AddItemToInventoryEvent eventData)
        {
            modelOld.AddItemToInventory(eventData.Item, eventData.IsNew);
            LoadCategoryItems(eventData.Item.ItemConfig.Category);
        }

        private void RemoveItemFromInventory(RemoveItemFromInventoryEvent eventData)
        {
            var itemData = ItemsController.Instance.LoadItem(eventData.ItemID);
            var config = itemsAsset.GetItemConfig(itemData.IdItemConfig);
            modelOld.RemoveItemFromInventory(config, eventData.ItemID);
            LoadCategoryItems(config.Category);
        }
        */

        
    }

    public class CellViewParameters
    {
        public int CellId;
        public Sprite CellSprite;
        public string DescriptionCell;
        public bool IsCheckedCell;
        public AbstractItemData ItemData;
        public AbstractItemConfig ItemConfig;
        public float Transparency = 1;
    }
    
    
    public struct InventoryCategoryInfo
    {
        public CategoryName Category;
        public string KeyName;
        public int CountNewItems;
        public int CountItems;
        public bool HideIsEmpty;
    }
    
}