﻿using System.Collections;
using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Items;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.Inventory.Scripts;
using Feature.Windows.StartGameWindow.Scripts;
using UnityEngine;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    public class InventoryItemsModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;

        private int LastCellId
        {
            get
            {
                var cellData = LoadObjectInfo<LastCellId>(HeroId);
                if (cellData == null)
                    return 0;
                return cellData.LastId; 
            }
            set
            {
                var data = new LastCellId();
                data.LastId = value;
                SaveObjectInfo(HeroId,data);
            }
            
        }


        public List<CellData> LoadCategoryData(CategoryName category)
        {
            var dates = LoadInventoryData();
            if (dates == null)
            {
                return null;
            }

            foreach (var data in dates)
            {
                if (data.Category == category)
                {
                    return data.CellsOnCategory;
                }
            }

            return null;
        }
        

        public List<CategoryCellData> LoadInventoryData()
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);
            if (data == null)
            {
                return null;
            }

            return data.Categories;
        }

        public void AddItemToInventory(AbstractItemData eventDataItem, bool isNew)
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);
            if (data == null)
            {
                data = new InventoryData();
                data.Categories = new List<CategoryCellData>();
            }

            CategoryCellData currentCategory = null;
                
            foreach (var category in data.Categories)
            {
                if (category.Category == eventDataItem.ItemConfig.Category)
                {
                    currentCategory = category;
                    break;
                }
            }

            if (currentCategory == null)
            {
                currentCategory = new CategoryCellData();
                currentCategory.Category = eventDataItem.ItemConfig.Category;
                currentCategory.CellsOnCategory = new List<CellData>();
                data.Categories.Add(currentCategory);
            }
            
            
            
            List<CellData> cells = currentCategory.CellsOnCategory;
            if (cells == null)
            {
                cells = new List<CellData>();
                currentCategory.CellsOnCategory = cells;
            }
            


            if (eventDataItem.ItemConfig.IsStackable)
            {
                CellData currentCell = null;
                foreach (var cell in cells)
                {
                    if (cell.configId == eventDataItem.IdItemConfig)
                    {
                        if (cell.ItemsInCell == null)
                        {
                            cell.ItemsInCell = new List<int>();
                        }

                        currentCell = cell;
                        break;
                    }
                }

                if (currentCell == null)
                {
                    currentCell= new CellData();
                    currentCell.IdCell = LastCellId +1;
                    LastCellId = currentCell.IdCell;
                    currentCell.configId = eventDataItem.IdItemConfig;
                    currentCell.ItemsInCell = new List<int>();
                    cells.Add(currentCell);
                }
                
                currentCell.IsChecked = !isNew;
                currentCell.ItemsInCell.Add(eventDataItem.IdItem);
               

            }
            else 
            {
                
                var cellData = new CellData();
                cellData.IdCell = LastCellId +1;
                LastCellId = cellData.IdCell;
                cellData.IsChecked = !isNew;
                cellData.configId = eventDataItem.IdItemConfig;
                cellData.ItemsInCell = new List<int>();
                cellData.ItemsInCell.Add(eventDataItem.IdItem);
                cells.Add(cellData);
            }
            
            SaveObjectInfo(HeroId ,data);
        }

        public void RemoveItemFromInventory(AbstractItemConfig itemConfig, int itemToRemove)
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);
            if(data == null || data.Categories == null)
                return;


            CategoryCellData currentCategory = null;
            foreach (var category in data.Categories)
            {
                if (category.Category == itemConfig.Category)
                {
                    currentCategory = category;
                    break;
                }
            }
            if(currentCategory == null)
                return;
            
            
            
            bool isRemove = false;
            foreach (var cellData in currentCategory.CellsOnCategory)
            {
                foreach (var itemId in cellData.ItemsInCell)
                {
                    if (itemId == itemToRemove)
                    {
                        cellData.ItemsInCell.Remove(itemToRemove);
                        if (cellData.ItemsInCell.Count == 0)
                        {
                            currentCategory.CellsOnCategory.Remove(cellData);

                            if (currentCategory.CellsOnCategory.Count == 0)
                            {
                                data.Categories.Remove(currentCategory);
                            }

                        }

                        isRemove = true;
                        break;
                    }
                }
                if(isRemove)
                   break; 
            }
            SaveObjectInfo(HeroId ,data);
        }

        public InventoryCategoryInfo LoadCategoryInfo(CategoryName categoryName)
        {
            var data = LoadCategoryData(categoryName);
            var info = new InventoryCategoryInfo();
            if (data == null)
            {
                info.Category = categoryName;
                info.CountItems = 0;
                info.CountNewItems = 0;
                return info;
            }

            info.Category = categoryName;
            info.CountItems = data.Count;
            info.CountNewItems = 0;
            foreach (var cell in data)
            {
                if (cell != null && !cell.IsChecked)
                {
                    info.CountNewItems++;
                }
            }

            return info;

        }

        public void SetCheckedCell(int cellId)
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);

            if (data == null || data.Categories == null)
            {
                return;
            }
            

            foreach (var category in data.Categories)
            {
                if(category == null || category.CellsOnCategory == null)
                    continue;

                foreach (var cell in category.CellsOnCategory)
                {
                    if (cell.IdCell == cellId)
                    {
                        cell.IsChecked = true;
                        SaveObjectInfo(HeroId ,data);
                        return;
                    }
                }

            }
            
        }
    }
}