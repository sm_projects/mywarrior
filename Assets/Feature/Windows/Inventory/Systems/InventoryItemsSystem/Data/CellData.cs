﻿using System;
using System.Collections.Generic;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    [Serializable]
    public class CellData
    {
        public int IdCell;
        public bool IsChecked;
        public int configId;
        public List<int> ItemsInCell;
    }
}