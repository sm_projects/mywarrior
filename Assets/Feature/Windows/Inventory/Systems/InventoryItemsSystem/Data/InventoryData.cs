﻿using System;
using System.Collections.Generic;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    [Serializable]
    public class InventoryData
    {
        public List<CategoryCellData> Categories;
    }
}