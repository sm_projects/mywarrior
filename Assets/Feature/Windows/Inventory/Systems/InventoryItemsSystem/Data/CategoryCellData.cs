﻿using System;
using System.Collections.Generic;
using Feature.Items.Scripts.Abstract;

namespace Feature.Inventory.Systems.InventoryItemsSystem
{
    [Serializable]
    public class CategoryCellData
    {
        public CategoryName Category;
        public List<CellData> CellsOnCategory;
    }
}