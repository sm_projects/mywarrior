﻿using System;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;

namespace Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryCrystalItemWindowSystem
{
    public class InventoryCrystalItemWindowSystem : AController
    {
        
        public event Action<CrystalItemConfig> OnOpenWindow;
        
        private CrystalItemConfig _currentConfig;
        public void OpenWindow(CellViewParameters parameters)
        {
            if (parameters.ItemData.IdItem == -1)
                return;
            
            _currentConfig = parameters.ItemConfig as CrystalItemConfig;
            OnOpenWindow?.Invoke(_currentConfig);
        }

        

        public void ClickType()
        {
            throw new NotImplementedException();
        }

        public void ClickImprove()
        {
            throw new NotImplementedException();
        }

        public void ClickEquip()
        {
            throw new NotImplementedException();
        }
    }
}