﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Views;
using Feature.Items.Systems.CrystalFactorySystem.Systems.CrystalItemTypeSystem.Scripts;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Views;
using Feature.Items.Systems.EquipFactorySystem.Systems.EquipItemTypeSystem;
using UnityEngine;
using UnityEngine.UI;
using Utilits;

namespace Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryCrystalItemWindowSystem
{
    public class InventoryCrystalItemWindowObject : AObject<InventoryCrystalItemWindowSystem>
    {
        [Header("Default")]
        
        [SerializeField]
        private GameObject panel;
        
        [SerializeField]
        private Button close;

        [SerializeField]
        private CrystalItemWindowView crystalItemWindowView;
        
        [Header("Buttons")]
        
        [SerializeField]
        private Button equipButton;
        
        [SerializeField]
        private Button improveButton;

        private ComponentAssets _componentAssets;
        private CrystalItemTypeAssets _crystalItemTypeAssets;

        public override void Initialize()
        {
            base.Initialize();
            _componentAssets = GetViewAsset<ComponentAssets>();
            _crystalItemTypeAssets = GetViewAsset<CrystalItemTypeAssets>();
        }


        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenWindow += OpenWindow;
            equipButton.onClick.AddListener(ClickEquip);
            improveButton.onClick.AddListener(ClickImprove);
            close.onClick.AddListener(Close);
            
            crystalItemWindowView.OnClickType += ClickType;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenWindow -= OpenWindow;
            equipButton.onClick.RemoveListener(ClickEquip);
            improveButton.onClick.RemoveListener(ClickImprove);
            close.onClick.RemoveListener(Close);
            
            crystalItemWindowView.OnClickType -= ClickType;
        }


        private void OpenWindow(CrystalItemConfig itemConfig)
        {
            panel.SetActive(true);
            var listStats = new List<string>();

            var equipItemTypeConfig = _crystalItemTypeAssets.GetConfig(itemConfig.CrystalItemType);
            
            foreach (var stat in itemConfig.Stats)
            {
                var componentConfig = _componentAssets.GetComponentConfig(stat.Component);
                var stringStat = componentConfig.KeyName + " " + MathTypeOperation.GetChar(stat.MathType) + " " +
                                 stat.Value;
                listStats.Add(stringStat);
            }
            
            crystalItemWindowView.Initialize(itemConfig, equipItemTypeConfig, listStats);

        }
        
        private void ClickType()
        {
            Controller.ClickType();
        }

        private void ClickImprove()
        {
            Controller.ClickImprove();
            Close();
        }
        
        private void ClickEquip()
        {
            Controller.ClickEquip();
            Close();
        }

        private void Close()
        {
            panel.SetActive(false);
        }
    }
}