﻿using System;
using Architecture.ControllerManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;

namespace Feature.Windows.Inventory.Systems.InventoryItemsSystem.Systems.InventoryEquipItemWindowSystem.Scripts
{
    public class InventoryEquipItemWindowSystem : AController
    {
        public event Action<EquipItemConfig, CrystalItemConfig> OnOpenWindow;

        private CrystalItemConfig _currentCrystalItemConfig;
        
        private EquipItemConfig _currentEquipItemConfig;
        
        private EquipItemData _currentEquipItemData;

        private ItemsAsset _itemsAsset;

        public override void Initialize()
        {
            base.Initialize();
            _itemsAsset = GetConfig<ItemsAsset>();

        }
        
        
        public void OpenWindow(CellViewParameters parameters)
        {

            if (parameters.ItemData.IdItem == -1)
                return;

            _currentEquipItemData = parameters.ItemData as EquipItemData;
            _currentEquipItemConfig = parameters.ItemConfig as EquipItemConfig;
            if (_currentEquipItemData.CrystalId == -1)
            {
                _currentCrystalItemConfig = null;
            }
            else
            {
                var crystalData= ItemsController.Instance.LoadItem(_currentEquipItemData.CrystalId);
                _currentCrystalItemConfig = _itemsAsset.GetItemConfig(crystalData.IdItemConfig) as CrystalItemConfig;
            }


            OnOpenWindow?.Invoke(_currentEquipItemConfig, _currentCrystalItemConfig);
        }
        
        public void ClickEquip()
        {
            //EventManager.PushEvent(new NeedEquipItemEvent(_currentEquipItemData.IdItem));
            //EventManager.PushEvent(new RemoveItemFromInventoryEvent(_currentEquipItemData.IdItem));
        }

        public void ClickImprove()
        {
            throw new NotImplementedException();
        }

        public void ClickCrystal()
        {
            throw new NotImplementedException();
        }

        public void ClickType()
        {
            throw new NotImplementedException();
        }
    }
}