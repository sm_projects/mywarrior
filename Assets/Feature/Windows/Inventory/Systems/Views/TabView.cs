﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Inventory.Scripts
{
    public class TabView : AView
    {
        [SerializeField]
        private Text nameTab;
        
        [SerializeField]
        private Toggle toggle;

        [SerializeField]
        private GameObject notification;

        [SerializeField]
        private Text countNew;


        public event Action OnSelected;
        

        protected override void Subscribe()
        {
            base.Subscribe();
            toggle.onValueChanged.AddListener(Click);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            toggle.onValueChanged.RemoveListener(Click);
        }


        public void Initialize(InventoryCategoryInfo infoTab)
        {
            nameTab.text = infoTab.KeyName;
            notification.SetActive(infoTab.CountNewItems != 0);
            countNew.text = infoTab.CountNewItems.ToString();
        }
        
        
        public void SetToggleGroup(ToggleGroup group)
        {
            toggle.group = group;
        }

        private void Click(bool isActive)
        {
            if (isActive)
            {
                OnSelected?.Invoke();
                
            }
        }


    }
}