﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Scripts.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Inventory.Scripts
{
    public class SlotView : AView
    {
        [SerializeField]
        private GameObject defaultIcon;
        
        [SerializeField]
        private Image itemIcon;

        [SerializeField]
        private Text countText;
        
        [SerializeField]
        private Text idText;

        [SerializeField]
        private Button button;

        [SerializeField]
        private GameObject checkedItem;
        
        [SerializeField]
        private bool isHaveDefaultIcon;

        public event Action OnClickItem;
        
        
        protected override void Subscribe()
        {
            base.Subscribe();
            
            button.onClick.AddListener(Click);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            
            button.onClick.RemoveListener(Click);
        }

        private void Click()
        {
            OnClickItem?.Invoke();   
        }

        public void Initialize(CellViewParameters parameters)
        {
            if (parameters == null)
            {
                itemIcon.gameObject.SetActive(false);
                countText.text = "";
                checkedItem.SetActive(false);
                defaultIcon.SetActive(isHaveDefaultIcon);
                SetTransparency(1);
                return;
            }
            
            SetTransparency(parameters.Transparency);
            defaultIcon.SetActive(false);
            itemIcon.gameObject.SetActive(true);
            itemIcon.sprite = parameters.CellSprite;
            if (parameters.DescriptionCell == "1")
            {
                countText.text = "";
            }
            else
            {
                countText.text = parameters.DescriptionCell;  
            }

            
            checkedItem.SetActive(!parameters.IsCheckedCell);

        }

        public void SetTransparency(float transparencyItem)
        {
            itemIcon.color = new Color(1, 1, 1, transparencyItem);
        }
    }
}