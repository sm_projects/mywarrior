﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Scripts.Abstract;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Inventory.Scripts
{
    public class InventoryPanelView : AView
    {
        [SerializeField]
        private GridLayoutGroup itemsGrid;

        [SerializeField]
        private SlotView slotViewPrefab;

        [SerializeField]
        private int minItems = 4;

        public event Action<CellViewParameters> OnClickItem;

        private Dictionary<SlotView, int> _dictionary = new Dictionary<SlotView, int>();

        public void SpawnItems(List<CellViewParameters> listItems)
        {
            
            int countItem = 0;
            if (listItems != null)
                countItem = listItems.Count;

            Clear();

            if (countItem != 0)
            {
                foreach (var item in listItems)
                {
                    var slot = Instantiate(slotViewPrefab, itemsGrid.transform);
                    
                    slot.OnClickItem += delegate
                    {
                        OnClickItem?.Invoke(item);
                    };
                    slot.Initialize(item);
                    
                    _dictionary.Add( slot,item.CellId);
                }  
            }

            int a = countItem % itemsGrid.constraintCount;

            var countVoid = (minItems * itemsGrid.constraintCount) - a;
            
            for (int i = 0; i < countVoid; i++)
            {
                var slot = Instantiate(slotViewPrefab, itemsGrid.transform);
                _dictionary.Add(slot, -1);
            }


        }

        public void Clear()
        {
            foreach (var slot in _dictionary)
            {
                Destroy(slot.Key.gameObject);
            }
            
            _dictionary.Clear();
        }

        public void ChangeCell(CellViewParameters cellViewParameters)
        {
            foreach (var item in _dictionary)
            {
                if (item.Value == cellViewParameters.CellId)
                {
                    item.Key.Initialize(cellViewParameters);
                }
            }
        }
    }
}