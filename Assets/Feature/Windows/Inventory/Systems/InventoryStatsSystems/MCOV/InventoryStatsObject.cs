﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Inventory.Systems
{
    public class InventoryStatsObject : AObject<InventoryStatsSystems>
    {

        [SerializeField]
        private Transform Content;

        [SerializeField]
        private InventoryStatView PrefabStatView;
        
        public override void Subscribe()
        {
            base.Subscribe();

            Controller.OnStatsShow += RepaintShow;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            
            Controller.OnStatsShow -= RepaintShow;
        }
        
        
        private void RepaintShow(Dictionary<Component, StatViewParameters> obj)
        {

            for (int i = 0; i < Content.childCount; i++)
            {
                Destroy(Content.GetChild(i).gameObject);
            }
            
            foreach (var viewParameters in obj.Values)
            {
                if (viewParameters.AdditionValue != 0 || viewParameters.MultiplicationValue != 1)
                {
                    var stat = Instantiate(PrefabStatView, Content);
                    stat.Initialize(viewParameters);
                }
                else
                {
                    var stat = Instantiate(PrefabStatView, Content);
                    stat.Initialize(viewParameters);
                }
            }
        }
    }
}