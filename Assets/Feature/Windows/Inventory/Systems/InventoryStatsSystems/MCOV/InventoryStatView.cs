﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Inventory.Systems
{
    public class InventoryStatView : AView
    {
        [SerializeField]
        private Text NameText;
        
        [SerializeField]
        private Text DefaultText;
        
        [SerializeField]
        private Text AdditionalText;
        
        [SerializeField]
        private Text MultiplicationText;
        
        [SerializeField]
        private Text ResultText;

        [SerializeField] 
        private Color32 AddColor;
        
        [SerializeField] 
        private Color32 SubtractColor;

        public void Initialize(StatViewParameters parameters)
        {
            NameText.text = parameters.NameStat;

            DefaultText.text = parameters.DefaultValue.ToString("0");
            
            
            ResultText.text = parameters.ResultValue.ToString("0");

            if (parameters.AdditionValue == 0)
            {
                AdditionalText.text = "";
            }
            else
            {
                AdditionalText.text = parameters.AdditionValue.ToString("0");
                if (parameters.AdditionValue > 0)
                {
                    AdditionalText.color = AddColor;
                }
                else
                {
                    AdditionalText.color = SubtractColor;
                }  
            }
            
            if (parameters.MultiplicationValue == 1)
            {
                MultiplicationText.text = "";
            }
            else
            {
                
                if (parameters.MultiplicationValue > 1)
                {
                    MultiplicationText.text = "+" + ((parameters.MultiplicationValue - 1) * 100).ToString("0");
                    MultiplicationText.color = AddColor;
                }
                else
                {
                    MultiplicationText.text = "-" + (100 - (parameters.MultiplicationValue - 1) * 100).ToString("0");
                    MultiplicationText.color = SubtractColor;
                }  
            }

            if (parameters.ResultValue >= parameters.DefaultValue)
            {
                ResultText.color = AddColor;
            }
            else
            {
                ResultText.color = SubtractColor;
            }


        }
    }
}