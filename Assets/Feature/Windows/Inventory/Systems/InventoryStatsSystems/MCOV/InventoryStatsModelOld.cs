﻿using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Inventory.Systems
{
    public class InventoryStatsModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public InventoryEquipData LoadEquipments()
        {
            return LoadObjectInfo<InventoryEquipData>(HeroId);
        }
    }
}