﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Inventory.Systems
{
    public class InventoryStatsSystems : AController
    {

        public event Action<Dictionary<Component,StatViewParameters>> OnStatsShow;

        [SerializeField]
        private InventoryStatsModelOld modelOld;

        [SerializeField]
        private ComponentAssets componentAssets;
        
        [SerializeField]
        private ItemsAsset itemsAsset;

        
        public void EquipChange()
        {
            OpenStats();
        }
        
        public void OpenStats()
        {
            Dictionary<Component, StatViewParameters>
                listView = new Dictionary<Component, StatViewParameters>();

            var data = modelOld.LoadEquipments();
            if (data == null || data.slots == null || data.slots.Count == 0)
            {
                OnStatsShow?.Invoke(listView);
                return;
            }
            
            foreach (var slot in data.slots)
            {
                if(slot.itemId == -1)
                    continue;
                var itemData = ItemsController.Instance.LoadItem(slot.itemId) as EquipItemData;
                var config = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;

                foreach (var parameter in config.Stats)
                {
                    if (!listView.ContainsKey(parameter.Component))
                    {
                        var componentConfig = componentAssets.GetComponentConfig(parameter.Component);

                        if (componentConfig == null)
                        {
                            Debug.LogError($"не найден компонент конфиг для {parameter.Component}");
                        }

                        var viewParameters = new StatViewParameters();
                        viewParameters.NameStat = componentConfig.KeyName;
                        if (componentConfig.StartValue == StartValue.Zero)
                            viewParameters.DefaultValue = 0;
                        else
                            viewParameters.DefaultValue = componentConfig.DefaultValue;
                        viewParameters.AdditionValue = 0;
                        viewParameters.MultiplicationValue = 1;
                        listView.Add(parameter.Component,viewParameters);
                    }

                    /*
                    switch (parameter.StatOperation)
                    {
                        case StatOperation.Addition:
                        {
                            listView[parameter.ComponentName].AdditionValue += parameter.Value;
                            break;
                        }
                        case StatOperation.Multiplication:
                        {
                            listView[parameter.ComponentName].MultiplicationValue *= parameter.Value;
                            break;
                        }

                    }
                    */
                }
                    
            }

            foreach (var view in listView.Values)
            {
                view.ResultValue = (view.DefaultValue + view.AdditionValue) * (view.MultiplicationValue);
            }
            
            
            OnStatsShow?.Invoke(listView);
        }
    }

    public class StatViewParameters
    {
        public string NameStat;
        public float DefaultValue;
        public float AdditionValue;
        public float MultiplicationValue;
        public float ResultValue;

    }
}