﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Items.Scripts.Abstract;
using Feature.Skills.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Inventory.Systems.InventorySkillsSystems
{
    public class SkillView : AView
    {
        public event Action OnClickSkill;
        
        [SerializeField]
        private Button skillButton;

        [SerializeField] 
        private Image skillImage;
        
        [SerializeField] 
        private Image lockImage;
        
        [SerializeField] 
        private Image glowImage;

        [SerializeField] 
        private GameObject lvlContent;
        
        [SerializeField] 
        private Text crystalLvl;
        
        
        protected override void Subscribe()
        {
            base.Subscribe();
            skillButton.onClick.AddListener(ClickSkill);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            skillButton.onClick.RemoveListener(ClickSkill);
        }
        

        public void Initialize(SkillViewParameters viewParameters)
        {
            if (viewParameters == null)
            {
                skillImage.gameObject.SetActive(false);
                lockImage.gameObject.SetActive(false);
                lvlContent.gameObject.SetActive(false);
                glowImage.gameObject.SetActive(false);
                crystalLvl.text = "";
                return;
            }
            
            //skillImage.gameObject.SetActive(!viewParameters.IsEmpty);
            
            skillImage.sprite = viewParameters.ImageSkill;
            
            lockImage.gameObject.SetActive(!viewParameters.IsActive);

            lvlContent.gameObject.SetActive(!viewParameters.IsEmpty);
            
            glowImage.gameObject.SetActive(!viewParameters.IsEmpty);
            
            if (viewParameters.IsEmpty)
            {
                crystalLvl.text = "";
            }
            else
            {
                glowImage.color = viewParameters.ColorSkill;
                crystalLvl.text = viewParameters.CurrentLvl;
            }


        }
        
        private void ClickSkill()
        {
            OnClickSkill?.Invoke();
        }
        
        
    }
    
    public class SkillViewParameters
    {
        public Sprite ImageSkill;
        public Color32 ColorSkill;
        public bool IsActive;
        public bool IsEmpty;
        public string CurrentLvl;

        public SkillConfig SkillConfig;

        public SkillViewParameters()
        {
        }
    }
}