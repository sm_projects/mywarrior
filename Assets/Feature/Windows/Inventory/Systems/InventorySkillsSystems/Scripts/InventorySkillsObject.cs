﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;

namespace Feature.Windows.Inventory.Systems.InventorySkillsSystems
{
    public class InventorySkillsObject : AObject<InventorySkillsSystems>
    {
        [SerializeField]
        private Transform content;

        [SerializeField]
        private SkillView prefabSkillView;
        
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnRepaintSkills += RepaintSkills;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnRepaintSkills -= RepaintSkills;
        }

        private void RepaintSkills(List<SkillViewParameters> listSkills)
        {
            Clear();
            if (listSkills != null)
            {
                foreach (var viewParameters in listSkills)
                {
                    var view = Instantiate(prefabSkillView, content);
                    view.Initialize(viewParameters);
                }
            }
        }

        private void Clear()
        {
            for (int i = 0; i < content.childCount; i++)
            {
                Destroy(content.GetChild(i).gameObject);
                
            }
        }
    }
}