﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using Feature.Skills.Scripts;
using UnityEngine;

namespace Feature.Windows.Inventory.Systems.InventorySkillsSystems
{
    public class InventorySkillsSystems: AController
    {

        public event Action<List<SkillViewParameters>> OnRepaintSkills; 

        [SerializeField]
        private InventorySkillsModelOld modelOld;
        
        [SerializeField]
        private ItemsAsset itemsAsset;
        
        [SerializeField]
        private SkillsAsset skillsAsset;


        
        public void OpenSkillsWindow()
        {
            var result = new List<SkillViewParameters>();
            var data = modelOld.LoadEquipments();

            if (data == null || data.slots == null)
            {
                OnRepaintSkills?.Invoke(null);
                return;
            }

            foreach (var slotData in data.slots)
            {
                if(slotData.itemId == -1)
                    continue;
                
                var itemData = ItemsController.Instance.LoadItem(slotData.itemId) as EquipItemData;
                var itemConfig = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;
                
                if(string.IsNullOrEmpty(itemConfig.SkillName))
                    continue;

                var skillConfig = skillsAsset.GetSkill(itemConfig.SkillName);
                if(skillConfig == null)
                    continue;

                var viewParameters = new SkillViewParameters();
                viewParameters.SkillConfig = skillConfig;
                viewParameters.ImageSkill = skillConfig.Icon;
                viewParameters.IsEmpty = true;
                result.Add(viewParameters);
                
            }
            
            OnRepaintSkills?.Invoke(result);
        }
        

        public void EquipChange()
        {
            OpenSkillsWindow();
        }
    }


}