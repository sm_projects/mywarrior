﻿using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Windows.Inventory.Systems.InventorySkillsSystems
{
    public class InventorySkillsModelOld : AModelOld    
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public InventoryEquipData LoadEquipments()
        {
            return LoadObjectInfo<InventoryEquipData>(HeroId);
        }
    }
}