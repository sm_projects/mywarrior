﻿using Architecture.ControllerManager;
using Feature.SceneLoader.Configs;
using UnityEngine;

namespace Feature.Windows.MainMenu.Scripts
{
    public class MainMenuController : AController
    {
        [SerializeField]
        private SceneAssets SceneAssets;
        
        public void OpenInventoryClick()
        {
            //EventManager.PushEvent(new OpenInventoryWindowEvent());
        }

        public void OpenImproveClick()
        {
            //EventManager.PushEvent(new OpenImproveWindowEvent());
        }

        public void OpenCrystalClick()
        {
           // EventManager.PushEvent(new OpenCrystalWindowEvent());
        }

        public void Fight()
        {
            //EventManager.PushEvent(new SceneLoadEvent(SceneAssets.FightSceneName));
        }
    }
}