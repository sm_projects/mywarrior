﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.MainMenu.Scripts
{
    public class MainMenuObject : AObject<MainMenuController>
    {
        [SerializeField]
        private Button inventoryButton;
        
        [SerializeField]
        private Button improveButton;
        
        [SerializeField]
        private Button crystalButton;
        
        [SerializeField]
        private Button fightButton;

        public override void Subscribe()
        {
            base.Subscribe();
            inventoryButton.onClick.AddListener(InventoryClick);
            improveButton.onClick.AddListener(ImproveClick);
            crystalButton.onClick.AddListener(CrystalClick);
            fightButton.onClick.AddListener(Fight);
        }

        private void Fight()
        {
            Controller.Fight();
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            inventoryButton.onClick.RemoveListener(InventoryClick);
            improveButton.onClick.RemoveListener(ImproveClick);
            crystalButton.onClick.RemoveListener(CrystalClick);
            fightButton.onClick.RemoveListener(Fight);
        }

        private void InventoryClick()
        {
            Controller.OpenInventoryClick();
        }
        
        private void ImproveClick()
        {
            Controller.OpenImproveClick();
        }
        
        private void CrystalClick()
        {
            Controller.OpenCrystalClick();
        }
    }
}