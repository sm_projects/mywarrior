﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.Improve.Scripts.Configs;
using UnityEngine;

namespace Feature.Windows.Improve.Systems
{
    public class ImproveInfoSystems : AController
    {
        public event Action OnShowWindow;
        
        public event Action OnClickImprove;
        
        public event Action<bool> OnSetCanImprove;
        public event Action OnUnselectedItem;
        public event Action<int> OnAddItemForImprove;

        public event Action<ImproveViewParameters> OnSelectedItem; 



        public void ShowWindow()
        {
            OnShowWindow?.Invoke();
        }

        public void AddItemForImprove(CellViewParameters parameters)
        {
            OnAddItemForImprove?.Invoke(parameters.ItemConfig.ConfigId);
        }

        public void SelectedItem(ImproveViewParameters parameters)
        {
            OnSelectedItem?.Invoke(parameters);
        }

        public void UnselectedItem()
        {
            OnShowWindow?.Invoke();
            OnUnselectedItem?.Invoke();
        }

        public void SetCanImprove(bool isCan)
        {
            OnSetCanImprove?.Invoke(isCan);
        }

        public void ClickImproveButton()
        {
            OnClickImprove?.Invoke();
        }
    }

    public class ImproveViewParameters
    {
        public ImproveInfo ImproveInfo;
        public CellViewParameters OldItemParameters;
        public CellViewParameters NewItemParameters;
        public List<CellViewParameters> ListItemsCellView;
    }
}