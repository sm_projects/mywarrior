﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Improve.Systems.Objects.ImproveDoneObject
{
    public class ImproveDoneObject: AObject<ImproveInfoSystems>
    {
        [SerializeField]
        private Button improveButton;
        
        
        public override void Subscribe()
        {
            base.Subscribe();
            improveButton.onClick.AddListener(ClickImproveButton);
            
            Controller.OnSetCanImprove += SetCanImprove;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            improveButton.onClick.RemoveListener(ClickImproveButton);
            
            Controller.OnSetCanImprove -= SetCanImprove;
        }
        
        private void SetCanImprove(bool isCan)
        {
            improveButton.gameObject.SetActive(isCan);
        }
        
        private void ClickImproveButton()
        {
            Controller.ClickImproveButton();
        }
        
    }
}