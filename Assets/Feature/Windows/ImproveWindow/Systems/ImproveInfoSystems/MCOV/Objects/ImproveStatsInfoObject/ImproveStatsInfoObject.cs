﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Items.Systems;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Stats.Scripts;
using UnityEngine;

namespace Feature.Windows.Improve.Systems
{
    public class ImproveStatsInfoObject : AObject<ImproveInfoSystems>
    {
        [SerializeField]
        private Transform contentStats;

        [SerializeField] 
        private ComponentAssets componentAssets;
        
        [SerializeField]
        private ImproveStatsView prefabImproveStatsView;

        private List<ImproveStatViewParameters> _listStats;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSelectedItem += SelectedItem;
            Controller.OnShowWindow += RepaintDefaultWindow;
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSelectedItem -= SelectedItem;
            Controller.OnShowWindow -= RepaintDefaultWindow;
        }

        private void SelectedItem(ImproveViewParameters parameters)
        {
            _listStats = new List<ImproveStatViewParameters>();
            var listOldStats = new List<ComponentModificator>();
            var listNewStats = new List<ComponentModificator>();
            if (parameters.OldItemParameters.ItemConfig is EquipItemConfig oldEquipConfig)
            {
                EquipItemConfig newConfig = parameters.NewItemParameters.ItemConfig as EquipItemConfig;
                
                listOldStats = oldEquipConfig.Stats;
                listNewStats = newConfig.Stats;
            }
            
            if (parameters.OldItemParameters.ItemConfig is CrystalItemConfig oldCrystalConfig)
            {
                CrystalItemConfig newConfig = parameters.NewItemParameters.ItemConfig as CrystalItemConfig;
                
                listOldStats = oldCrystalConfig.Stats;
                listNewStats = newConfig.Stats;
            }


            foreach (var stat in listOldStats)
            {
                    
                var statConfig = componentAssets.GetComponentConfig(stat.Component);
                var viewParameters = new ImproveStatViewParameters();
                viewParameters.NameStat = statConfig.KeyName;
                viewParameters.OldValue = stat.Value;
                viewParameters.Component = stat.Component;
                _listStats.Add(viewParameters);  
            }

                
                
            var _listNewStats = new List<ImproveStatViewParameters>();
            foreach (var stat in listNewStats)
            {
                var statConfig = componentAssets.GetComponentConfig(stat.Component);
                foreach (var statInList in _listStats)
                {
                    
                    if (statInList.Component == statConfig.Component)
                    {

                        statInList.NewValue = stat.Value;
                        break;
                    }
                    

                    var viewParameters = new ImproveStatViewParameters();
                    viewParameters.NameStat = statConfig.KeyName;
                    viewParameters.OldValue = 0;
                    viewParameters.NewValue = stat.Value;
                    viewParameters.Component = statConfig.Component;
                    _listNewStats.Add(viewParameters);

                }
            }

            foreach (var newStat in _listNewStats)
            {
                _listStats.Add(newStat); 
            }

            
            for (int i = 0; i < contentStats.childCount; i++)
            {
                Destroy(contentStats.GetChild(i).gameObject);
            }
            
            foreach (var stat in _listStats)
            {
                var view = Instantiate(prefabImproveStatsView, contentStats);
                view.Initialize(stat);
            }

            
        }

        private void RepaintDefaultWindow()
        {
            for (int i = 0; i < contentStats.childCount; i++)
            {
                Destroy(contentStats.GetChild(i).gameObject);
            }
        }
    }

    public class ImproveStatViewParameters
    {
        public Component Component;
        public string NameStat;
        public float OldValue;
        public float NewValue;
    }
}