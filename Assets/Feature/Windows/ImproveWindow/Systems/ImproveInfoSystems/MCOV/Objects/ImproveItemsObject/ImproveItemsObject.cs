﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;

namespace Feature.Windows.Improve.Systems.Objects.ImproveItemsObject
{
    public class ImproveItemsObject: AObject<ImproveInfoSystems>
    {
        
        [SerializeField]
        private SlotView oldItem;
        
        [SerializeField]
        private SlotView newItem;
        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSelectedItem += SelectedItem;
            Controller.OnShowWindow += RepaintDefaultWindow;

            oldItem.OnClickItem += UnselectedItem;
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSelectedItem -= SelectedItem;
            Controller.OnShowWindow -= RepaintDefaultWindow;
            
            oldItem.OnClickItem -= UnselectedItem;
        }
        
        
        private void SelectedItem(ImproveViewParameters parameters)
        {
            newItem.Initialize(parameters.NewItemParameters);
            oldItem.Initialize(parameters.OldItemParameters);
        }
        
        private void UnselectedItem()
        {
            Controller.UnselectedItem();
        }

        private void RepaintDefaultWindow()
        {
            newItem.Initialize(null);
            oldItem.Initialize(null);
        }
    }
}