﻿using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Improve.Systems
{
    public class ImproveInfoObject : AObject<ImproveInfoSystems>
    {

        
        [SerializeField]
        private List<SlotView> contentItems;

        [SerializeField]
        private Text nameOldItem;

        [SerializeField]
        private Text nameNewItem;
        
        [SerializeField]
        private float transparencyItem = 0.5f;
        
        [SerializeField]
        private string oldItemText = "Item1";
        
        [SerializeField]
        private string newItemText = "Item2";

        private Dictionary<int, int> _listConfig;
        private List<int> _listSelectedItem;

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSelectedItem += SelectedItem;
            Controller.OnShowWindow += RepaintDefaultWindow;
            Controller.OnAddItemForImprove += AddItemForImprove;
        }
        
        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSelectedItem -= SelectedItem;
            Controller.OnShowWindow -= RepaintDefaultWindow;
            Controller.OnAddItemForImprove -= AddItemForImprove;
        }
        
        
        private void AddItemForImprove(int configItemId)
        {
            foreach (var slotView in _listConfig)
            {
                if (slotView.Value == configItemId)
                {
                    bool isSelected = false;
                    foreach (var selectedItem in _listSelectedItem)
                    {
                        if (selectedItem == slotView.Key)
                        {
                            isSelected = true;
                            break;
                        }
                    }

                    if (!isSelected)
                    {
                        contentItems[slotView.Key].SetTransparency(1);
                        _listSelectedItem.Add(slotView.Key);
                        return;
                    }
                }
            }
        }

        private void SelectedItem(ImproveViewParameters parameters)
        {
            _listConfig = new Dictionary<int, int>();
            _listSelectedItem = new List<int>();
            nameNewItem.text = parameters.NewItemParameters.ItemConfig.NameKey;
            nameOldItem.text = parameters.OldItemParameters.ItemConfig.NameKey;


            var countItem = parameters.ListItemsCellView.Count;
            
            for (int i = 0; i < countItem; i++)
            {
                _listConfig.Add(i, parameters.ListItemsCellView[i].ItemConfig.ConfigId);
                contentItems[i].gameObject.SetActive(true);
                contentItems[i].Initialize(parameters.ListItemsCellView[i]);
                contentItems[i].SetTransparency(transparencyItem);
            }

            for (int i = countItem; i < contentItems.Count; i++)
            {
                contentItems[i].gameObject.SetActive(false);
            }
        }
        

        private void RepaintDefaultWindow()
        {
            nameNewItem.text = newItemText;
            nameOldItem.text = oldItemText;
            
            for (int i = 0; i < contentItems.Count; i++)
            {
                contentItems[i].gameObject.SetActive(true);
                contentItems[i].Initialize(null);
                contentItems[i].SetTransparency(1);
            }
        }
    }
}