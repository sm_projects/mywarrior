﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Improve.Systems
{
    public class ImproveStatsView : AView
    {
        [SerializeField]
        private Text nameStat;

        [SerializeField]
        private Text oldValue;

        [SerializeField]
        private Text newValue;


        public void Initialize(ImproveStatViewParameters viewParameters)
        {
            nameStat.text = viewParameters.NameStat;

            if (viewParameters.OldValue != 0)
            {
                oldValue.text = viewParameters.OldValue.ToString("0.00");
            }
            else
            {
                oldValue.text = viewParameters.OldValue.ToString();
            }
            
            newValue.text = viewParameters.NewValue.ToString("0.00");
        }
    }
}