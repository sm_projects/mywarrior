﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.Inventory.Scripts;
using UnityEngine;

namespace Feature.Windows.Improve.Systems.ImproveItemsSystem
{
    public class ImproveInventorySystem : AController
    {
        
        [SerializeField]
        private ItemsAsset itemsAsset;

        public event Action<InventoryCategoryInfo> OnAddTab;
        
        public event Action OnOpenInventory;
        
        public event Action<List<CellViewParameters>> OnSelectionCategory;
        
        public event Action OnClickTab;

        public event Action<CellViewParameters> OnClickCell;

        [SerializeField]
        private ImproveInventoryModelOld modelOld;

        private List<CellViewParameters> _improveItems;
        private List<int> _selectedItems;

        public void OpenInventoryItems()
        {
            OnOpenInventory?.Invoke();
            _selectedItems = new List<int>();
            foreach (var category in itemsAsset.Categories)
            {
                if (category.IsImprove)
                {
                    var infoSystem = modelOld.LoadCategoryInfo(category.CategoryName);
                    infoSystem.KeyName = category.KeyName;
                    infoSystem.HideIsEmpty = category.HideIsEmpty;
                    infoSystem.CountNewItems = 0;
                    OnAddTab?.Invoke(infoSystem);
                }
            }
        }

        public void SelectedItem(ImproveViewParameters parameters)
        {
            _improveItems = new List<CellViewParameters>();
            _selectedItems = new List<int>();
            _selectedItems.Add(parameters.OldItemParameters.ItemData.IdItem);
            GetItemsForImprove(parameters);
            OnSelectionCategory?.Invoke(_improveItems);
        }
        
        public void AddItemToImprove(CellViewParameters parameters)
        {
            foreach (var item in _improveItems)
            {
                if (item.ItemData.IdItem == parameters.ItemData.IdItem)
                {
                    item.Transparency = 0.5f;
                }
            }
            OnSelectionCategory?.Invoke(_improveItems);
        }
        
        public void ClickCell(CellViewParameters item)
        {
            if (_selectedItems != null)
            {
                foreach (var selectedItem in _selectedItems)
                {
                    if (selectedItem == item.ItemData.IdItem)
                    {
                        return;
                    }
                } 
            }
 
            OnClickCell?.Invoke(item);
        }

        public void ClickTab(InventoryCategoryInfo tabInfo)
        {
            _selectedItems = new List<int>();
            var result = GetViewParametersCategory(tabInfo.Category);
            OnSelectionCategory?.Invoke(result);
            OnClickTab?.Invoke();
        }

        private void GetItemsForImprove(ImproveViewParameters parameters)
        {
            var category = parameters.OldItemParameters.ItemConfig.Category;
            var allItems = GetViewParametersCategory(category);
            
            foreach (var cellView in allItems)
            {
                if (cellView.ItemData.IdItem == parameters.OldItemParameters.ItemData.IdItem)
                {
                    cellView.Transparency = 0.5f;
                    _improveItems.Add(cellView);
                    continue;
                }
                
                foreach (var itemCellView in parameters.ListItemsCellView)
                {
                    if (itemCellView.ItemConfig.ConfigId == cellView.ItemConfig.ConfigId)
                    {
                        _improveItems.Add(cellView);
                        break;
                    }
                }
            }
        }

        private List<CellViewParameters> GetViewParametersCategory(CategoryName category)
        {
            var cellsData = modelOld.LoadCategoryData(category);
            
            if (cellsData == null || cellsData.Count == 0)
            {
                //OnSelectionCategory?.Invoke(null);
                return null;
            }

            List<CellViewParameters> result = new List<CellViewParameters>();
            
            foreach (var cellData in cellsData)
            {
                if (cellData == null || cellData.ItemsInCell == null)
                {
                    continue;
                }
                
                var firstItemId = cellData.ItemsInCell[0];
                var cellViewParameters = new CellViewParameters();
                 
                
                var item = ItemsController.Instance.LoadItem(firstItemId);
                var config = itemsAsset.GetItemConfig(item.IdItemConfig);
                cellViewParameters.CellSprite = config.Icon;
                cellViewParameters.DescriptionCell = cellData.ItemsInCell.Count.ToString();
                cellViewParameters.IsCheckedCell = true;
                cellViewParameters.ItemConfig = config;
                cellViewParameters.ItemData = item;
                cellViewParameters.CellId = cellData.IdCell;
                
                result.Add(cellViewParameters);
            }

            return result;
            
        }
    }
}