﻿using System.Collections.Generic;
using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.StartGameWindow.Scripts;

namespace Feature.Windows.Improve.Systems.ImproveItemsSystem
{
    public class ImproveInventoryModelOld : AModelOld
    {
        private int HeroId=> LoadPlayerInfo<SelectedHeroData>().HeroId;
        
        public InventoryCategoryInfo LoadCategoryInfo(CategoryName categoryName)
        {
            var data = LoadCategoryData(categoryName);
            var info = new InventoryCategoryInfo();
            if (data == null)
            {
                info.Category = categoryName;
                info.CountItems = 0;
                info.CountNewItems = 0;
                return info;
            }

            info.Category = categoryName;
            info.CountItems = data.Count;
            info.CountNewItems = 0;
            foreach (var cell in data)
            {
                if (cell != null && !cell.IsChecked)
                {
                    info.CountNewItems++;
                }
            }

            return info;
        }
        
        public List<CellData> LoadCategoryData(CategoryName category)
        {
            var dates = LoadInventoryData();
            if (dates == null)
            {
                return null;
            }

            foreach (var data in dates)
            {
                if (data.Category == category)
                {
                    return data.CellsOnCategory;
                }
            }

            return null;
        }
        
        public List<CategoryCellData> LoadInventoryData()
        {
            var data = LoadObjectInfo<InventoryData>(HeroId);
            if (data == null)
            {
                return null;
            }

            return data.Categories;
        }
    }
}