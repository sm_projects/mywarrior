﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.MCOV;
using Feature.DebugMode;
using Feature.Inventory.Systems.InventoryItemsSystem;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Scripts.Abstract;
using Feature.Windows.Improve.Scripts.Configs;
using Feature.Windows.Improve.Systems;
using Feature.Windows.Improve.Systems.ImproveItemsSystem;
using UnityEngine;

namespace Feature.Windows.Improve.Scripts
{
    public class ImproveWindowController : AController
    {
        
        [SerializeField]
        private ItemsAsset itemsAsset;
        
        [SerializeField]
        private ImproveAsset improveAsset;
        
        public event Action OnOpenImprove; 

        [SerializeField]
        private ImproveInventorySystem inventoryInventorySystem;

        [SerializeField]
        private ImproveInfoSystems improveInfoSystems;
        
        private Blueprint _currentBlueprint;

        private bool _isSelected = false;
        
        public override void Subscribe()
        {
            base.Subscribe();
            //EventManager.Subscribe<OpenImproveWindowEvent>(OpenImprove);

            inventoryInventorySystem.OnClickCell += ClickCellInventory;
            inventoryInventorySystem.OnClickTab += ClickTabInventory;
            
            improveInfoSystems.OnUnselectedItem += ClickUnselectedItem;
            improveInfoSystems.OnClickImprove += ClickImprove;
        }
        
        public override void Unsubscribe()
        {
            base.Unsubscribe();
            //EventManager.Unsubscribe<OpenImproveWindowEvent>(OpenImprove);
            
            inventoryInventorySystem.OnClickCell -= ClickCellInventory;
            inventoryInventorySystem.OnClickTab -= ClickTabInventory;
            
            improveInfoSystems.OnUnselectedItem -= ClickUnselectedItem;
            improveInfoSystems.OnClickImprove -= ClickImprove;
        }



        /*
        private void OpenImprove(OpenImproveWindowEvent windowEventData)
        {
            OnOpenImprove?.Invoke();
            _isSelected = false;
            inventoryInventorySystem.OpenInventoryItems();
            improveInfoSystems.ShowWindow();
        }
        */
        
        private void ClickCellInventory(CellViewParameters parameters)
        {
            if (_isSelected)
            {
                AddImproveItem(parameters);
            }
            else
            {
                SelectedItem(parameters);
            }
        }
        
        private void ClickTabInventory()
        {
            _isSelected = false;
            improveInfoSystems.SetCanImprove(false);
            improveInfoSystems.ShowWindow();
        }
        

        private void ClickUnselectedItem()
        {
            _isSelected = false;
            improveInfoSystems.SetCanImprove(false);
            inventoryInventorySystem.OpenInventoryItems();
            improveInfoSystems.ShowWindow();
        }
        
        private void ClickImprove()
        {
            var count = _currentBlueprint.ListItems.Count;
            foreach (var item in _currentBlueprint.ListItems)
            {
                if (item.ItemData != null)
                {
                    count--;
                }
            }

            if (count != 0)
            {
              return;  
            }

            var newItem = ItemsController.Instance.CreateItem(_currentBlueprint.ResultID);
            
            //EventManager.PushEvent(new AddItemToInventoryEvent(newItem));
            
            foreach (var item in _currentBlueprint.ListItems)
            {
                //EventManager.PushEvent(new RemoveItemFromInventoryEvent(item.ItemData.IdItem));
            }

            ClickUnselectedItem();

        }
        

        private void AddImproveItem(CellViewParameters parameters)
        {

            foreach (var item in _currentBlueprint.ListItems)
            {
                if (item.ItemData != null && item.ItemData.IdItem == parameters.ItemData.IdItem)
                {
                    return;
                }
            }
            
            foreach (var item in _currentBlueprint.ListItems)
            {
                if (item.ItemData == null && item.ConfigId == parameters.ItemConfig.ConfigId)
                {
                    item.ItemData = parameters.ItemData;
                    inventoryInventorySystem.AddItemToImprove(parameters);
                    improveInfoSystems.AddItemForImprove(parameters);
                    break;
                }
            }

            int count = _currentBlueprint.ListItems.Count;
            foreach (var item in _currentBlueprint.ListItems)
            {
                if (item.ItemData != null)
                {
                    count--;
                }
            }

            if (count == 0)
            {
                improveInfoSystems.SetCanImprove(true);
            }
            else
            {
                improveInfoSystems.SetCanImprove(false);
            }


        }
        
        private void SelectedItem(CellViewParameters parameters)
        {
            improveInfoSystems.SetCanImprove(false);
            
            var improveViewParameters = GenerateImproveViewParameters(parameters);

            if(improveViewParameters == null)
                return;
            
            improveInfoSystems.SelectedItem(improveViewParameters);
            inventoryInventorySystem.SelectedItem(improveViewParameters);
            
            _isSelected = true;


            _currentBlueprint = new Blueprint();
            _currentBlueprint.ListItems = new List<ImproveSetItem>();
            _currentBlueprint.ResultID = improveViewParameters.NewItemParameters.ItemConfig.ConfigId;

            var currentItem = new ImproveSetItem();
            currentItem.ConfigId = parameters.ItemConfig.ConfigId;
            currentItem.ItemData = parameters.ItemData;
            
            _currentBlueprint.ListItems.Add(currentItem);
            
            foreach (var cellView in improveViewParameters.ListItemsCellView)
            {
                var itemToImprove = new ImproveSetItem();
                itemToImprove.ConfigId = cellView.ItemConfig.ConfigId;
                itemToImprove.ItemData = null;
                _currentBlueprint.ListItems.Add(itemToImprove);
            }
            
        }


        private ImproveViewParameters GenerateImproveViewParameters(CellViewParameters parameters)
        {
            var itemConfig = parameters.ItemConfig;

            ImproveInfo improveInfo = null;
            foreach (var asset in improveAsset.ListImproveInfo)
            {
                if (asset.ItemConfigId == itemConfig.ConfigId)
                {
                    improveInfo = asset;
                    break;
                }
            }

            if (improveInfo == null)
            {
                return null;
            }

            var viewParameters = new ImproveViewParameters();
            viewParameters.ImproveInfo = improveInfo;
            var cellOldItem = parameters;
            cellOldItem.IsCheckedCell = true;
            var cellNewItem = new CellViewParameters();
            
            var newItemConfig = itemsAsset.GetItemConfig(improveInfo.ItemResultConfigId);
            cellNewItem.CellSprite = newItemConfig.Icon;
            cellNewItem.ItemConfig = newItemConfig;
            cellNewItem.IsCheckedCell = true;

            viewParameters.OldItemParameters = cellOldItem;
            viewParameters.NewItemParameters = cellNewItem;
            

            viewParameters.ListItemsCellView = new List<CellViewParameters>();
            foreach (var itemId in improveInfo.ListItemsForImprove)
            {
                var itemConfigS = itemsAsset.GetItemConfig(itemId);
                var parametersS = new CellViewParameters();
                parametersS.CellSprite = itemConfigS.Icon;
                parametersS.ItemConfig = itemConfigS;
                parametersS.IsCheckedCell = true;
                viewParameters.ListItemsCellView.Add(parametersS);
            }

            return viewParameters;
        }
    }



    public class Blueprint
    {
        public int ResultID;
        public List<ImproveSetItem> ListItems;
    }

    public class ImproveSetItem
    {
        public int ConfigId;
        public AbstractItemData ItemData;
    }
}