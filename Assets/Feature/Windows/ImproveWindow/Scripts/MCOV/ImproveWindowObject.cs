﻿using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.Improve.Scripts
{
    public class ImproveWindowObject : AObject<ImproveWindowController>
    {
        
        [SerializeField]
        private GameObject panel;

        [SerializeField]
        private Button close;


        
        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnOpenImprove += OpenPanel;
            close.onClick.AddListener(ClickClose);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnOpenImprove -= OpenPanel;
            close.onClick.RemoveListener(ClickClose);
        }


        private void ClickClose()
        {
            panel.SetActive(false);
        }

        private void OpenPanel()
        {
            panel.SetActive(true);
        }
    }
}