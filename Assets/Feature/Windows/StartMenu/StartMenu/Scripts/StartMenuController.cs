﻿using Architecture.ControllerManager;
using Architecture.ModelManager;
using DefaultNamespace.AppearanceCreator;
using Feature.SceneLoader.Configs;
using Feature.SceneLoader.Scripts;
using Feature.Windows.StartGameWindow.Scripts;
using UnityEngine;

namespace Feature.Windows.StartMenu.StartMenu.Scripts
{
    public class StartMenuController : AController, IModelProvider<SceneLoaderModel>
    {

        private SceneLoaderModel Model;
        
        public void SetModel(SceneLoaderModel model)
        {
            Model = model;
        }
        
        [SerializeField]
        private SceneAssets sceneAssets;

        
        [SerializeField]private HeroLoaderSystem HeroLoaderSystem;
        [SerializeField]private StartMenuSystem StartMenuSystem;
        //[SerializeField]private HeroCreatorSystem HeroCreatorSystem;
        
        
        public override void Subscribe()
        {
            base.Subscribe();
            //Model.OnChangeData += StartLoaded;
            HeroLoaderSystem.OnHeroCreated += CreteHero;
        }
        
        

        

        public override void Unsubscribe()
        {
            base.Unsubscribe();
           // Model.OnChangeData -= StartLoaded;
            HeroLoaderSystem.OnHeroCreated -= CreteHero;
        }

        private void StartLoaded(CurrentSceneData data)
        {
            /*
            if (data.State == SceneLoaderState.End && data.NameScene == sceneAssets.StartSceneName)
            {
                var listHero = HeroLoaderSystem.LoadPlayerHeroes();
                
                if (listHero != null)
                {
                    StartMenuSystem.SpawnLoadedHeroes(listHero);
                }
            }
            */
        }

        private void CreteHero(HeroParameters hero)
        {
            StartMenuSystem.SpawnCreatedHero(hero);
        }



    }
}