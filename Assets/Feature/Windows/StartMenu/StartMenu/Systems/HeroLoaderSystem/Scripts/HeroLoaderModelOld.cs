﻿using Architecture.MCOV;
using Feature.Inventory.Systems.InventoryEquipSystem;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;

public class HeroLoaderModelOld : AModelOld
{
    public PlayerHeroesData GetPlayerHeroes()
    {
        return LoadPlayerInfo<PlayerHeroesData>();
    }
    
    public HeroAppearanceData GetAppearance(int idHero)
    {
        return LoadObjectInfo<HeroAppearanceData>(idHero);
    }
    
    public InventoryEquipData GetEquipment(int idHero)
    {
        return LoadObjectInfo<InventoryEquipData>(idHero);
    }
    
        
    public HeroInfoParameters GetHeroInfo(int idHero)
    {
        return LoadObjectInfo<HeroInfoParameters>(idHero);
    }
}