using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using DefaultNamespace;
using Feature.Items;
using Feature.Items.Scripts;
using Feature.Items.Systems.CrystalFactorySystem.Scripts.Items.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Data;
using UnityEngine;

public class HeroLoaderSystem : AController
{

    [SerializeField] private AllPartAssets allPartAssets;
    
    [SerializeField]
    private ItemsAsset itemsAsset;
    
    [SerializeField]
    private HeroLoaderModelOld modelOld;

    public event Action<HeroParameters> OnHeroCreated;
    
    public override void Subscribe()
    {
        base.Subscribe();
       // EventManager.Subscribe<HeroCreatedEvent>(HeroCreated);
    }

    public override void Unsubscribe()
    {
        base.Unsubscribe();
        //EventManager.Unsubscribe<HeroCreatedEvent>(HeroCreated);
    }

    /*
    private void HeroCreated(HeroCreatedEvent eventData)
    {
        var hero = LoadHero(eventData.HeroId);
        OnHeroCreated?.Invoke(hero);
    }
    */

    public List<HeroParameters> LoadPlayerHeroes()
    {
        var data = modelOld.GetPlayerHeroes();
       
       if (data == null ||
           data.ConectedHeroes == null ||
           data.ConectedHeroes.Count == 0)
       {
           return null;
       }

       List<HeroParameters> result = new List<HeroParameters>();
       
       foreach (var idHero in data.ConectedHeroes)
       {
           
           var loaderHeroInfo = LoadHero(idHero);
           
           result.Add(loaderHeroInfo);
       }

       return result;

    }

    private HeroParameters LoadHero(int idHero)
    {
        var appearanceData = modelOld.GetAppearance(idHero);
        var appearance = new AppearanceParameters();
        appearance.Beard = allPartAssets.BeardsAsset.GetPart<BeardConfig>(appearanceData.BeardId);
        appearance.Eye = allPartAssets.EyeAsset.GetPart<EyeConfig>(appearanceData.EyeId);
        appearance.Skin = allPartAssets.SkinAsset.GetPart<SkinConfig>(appearanceData.SkinId);
            
        var equip = modelOld.GetEquipment(idHero);
        EquipmentParameters equipmentParameters = null;
        if (equip != null && equip.slots != null && equip.slots.Count != 0)
        {
            equipmentParameters = new EquipmentParameters();
            equipmentParameters.EquipmentList = new List<EquipViewParameters>();
            foreach (var item in equip.slots)
            {
                if (item.itemId == -1)
                {
                    continue;
                }

                var viewParameters = new EquipViewParameters();
                var itemData = ItemsController.Instance.LoadItem(item.itemId) as EquipItemData;
                var config = itemsAsset.GetItemConfig(itemData.IdItemConfig) as EquipItemConfig;

                if (itemData.CrystalId != -1)
                {
                    var configCrystal = itemsAsset.GetItemConfig(itemData.IdItemConfig) as CrystalItemConfig;
                    viewParameters.CrystalViewConfig = configCrystal.CrystalViewConfig;
                }

                viewParameters.Part = item.EquipmentType;
                viewParameters.ViewConfig = config.PrefabView;
                
                
                equipmentParameters.EquipmentList.Add(viewParameters);
            }
        }
        //var currency = Model.GetCurrency(idHero);
        var heroInfo = modelOld.GetHeroInfo(idHero); 


        var heroParameters = new HeroParameters();
        heroParameters.Appearance = appearance;
        heroParameters.Equipment = equipmentParameters;
        heroParameters.HeroInfo = heroInfo;
        
        return heroParameters;
    }
}