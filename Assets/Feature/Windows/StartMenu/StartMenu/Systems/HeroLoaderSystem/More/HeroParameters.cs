﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using DefaultNamespace.Equippable;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;

public struct HeroParameters
{
    public AppearanceParameters Appearance;
    public EquipmentParameters Equipment;
    public HeroInfoParameters HeroInfo;
}

[Serializable]
public class AppearanceParameters
{
    public SkinConfig Skin;
    public BeardConfig Beard;
    public EyeConfig Eye;
}

public class EquipmentParameters
{
    public List<EquipViewParameters> EquipmentList;
}

[Serializable]
public class HeroInfoParameters
{
    public string HeroName;
    public int HeroId;
}