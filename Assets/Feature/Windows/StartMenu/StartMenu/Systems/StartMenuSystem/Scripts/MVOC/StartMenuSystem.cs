﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Feature.SceneLoader.Configs;
using UnityEngine;

namespace Feature.Windows.StartGameWindow.Scripts
{
    public class StartMenuSystem : AController
    {
        public event Action<HeroParameters> OnPlayerHeroLoaded;

        [SerializeField] private StartMenuModelOld modelOld;
        
        
        
        [SerializeField] private SceneAssets SceneAssets;

        public event Action<bool> OnChangeSelected; 

        public void ClickedHero(int heroId, bool isOn)
        {
            if (isOn)
            {
                modelOld.SaveSelectedHero(heroId);
                OnChangeSelected?.Invoke(true);   
            }
            else
            {
                modelOld.RemoveSelectedHero();
                OnChangeSelected?.Invoke(false);
            }


        }
        
        public void RemovedHero(int heroId)
        {
            //EventManager.PushEvent(new OpenRemoveHeroEvent(heroId));
            OnChangeSelected?.Invoke(false);
        }

        
        public void LoadHero()
        {
            //EventManager.PushEvent(new OpenLoadHeroEvent());
        }

        public void ClickStart()
        {
            //EventManager.PushEvent(new SceneLoadEvent(SceneAssets.MenuSceneName));
        }

        public void SpawnLoadedHeroes(List<HeroParameters> infoData)
        {
            foreach (var hero in infoData)
            {
                    OnPlayerHeroLoaded?.Invoke(hero);
            }
        }

        public void SpawnCreatedHero(HeroParameters hero)
        {
            OnPlayerHeroLoaded?.Invoke(hero);
        }
    }
}




