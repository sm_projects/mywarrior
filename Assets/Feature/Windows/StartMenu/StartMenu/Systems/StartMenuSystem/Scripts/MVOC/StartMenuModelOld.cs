﻿using Architecture.MCOV;

namespace Feature.Windows.StartGameWindow.Scripts
{
    public class StartMenuModelOld : AModelOld
    {
        public void SaveSelectedHero(int id)
        {
            var data = new SelectedHeroData();
            data.HeroId = id;
            SavePlayerInfo(data);
        }
        
        public void RemoveSelectedHero()
        {
            RemovePlayerData(typeof(SelectedHeroData));
        }
    }
}