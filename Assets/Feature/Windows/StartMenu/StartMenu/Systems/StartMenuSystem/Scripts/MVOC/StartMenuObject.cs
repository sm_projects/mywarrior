﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Equippable;
using Feature.Items.Systems.EquipFactorySystem.Scripts.Configs;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.StartGameWindow.Scripts
{
    public class StartMenuObject : AObject<StartMenuSystem>
    {
        [SerializeField]
        private StartMenuView startMenuView;
        
        
        [SerializeField]
        private List<HeroLoadedView> listHeroView;
        
        [SerializeField]
        private HeroLoadedView prefab;

        

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnPlayerHeroLoaded += SpawnHero;
            Controller.OnChangeSelected += ChangeStartButton;
            
            startMenuView.OnClickLoadHero += ClickLoadHero;
            startMenuView.OnClickStartGame += ClickNewGame;
        }

       

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnPlayerHeroLoaded -= SpawnHero;
            Controller.OnChangeSelected -= ChangeStartButton;
            
            startMenuView.OnClickLoadHero -= ClickLoadHero;
            startMenuView.OnClickStartGame -= ClickNewGame;
        }
        
        private void ChangeStartButton(bool isActive)
        {
            startMenuView.SetActiveStartButtons(isActive);
        }
        

        private void SpawnHero(HeroParameters loadedInfo)
        {
            /*
            var view = Instantiate(prefab, toggleGroup.transform);
            view.SetToggleGroup(toggleGroup);

            var heroId = loadedInfo.HeroInfo.HeroId;
            
            view.Initialize(loadedInfo);
            
            view.OnClick += delegate(bool isOn)
            {
                ClickedHero(heroId, isOn);
            };
            
            
            view.OnRemoveClick += delegate
            {
                RemoveHero(heroId);
            };

            toggleGroup.AnyTogglesOn();
            */
        }

        private void ClickedHero(int idHero, bool isOn)
        {
            Controller.ClickedHero(idHero, isOn);

        }
        
        private void RemoveHero(int idHero)
        {
            Controller.RemovedHero(idHero);
        }

        private void ClickNewGame()
        {
            Controller.ClickStart();
        }
        

        
        private void ClickLoadHero()
        {
            Controller.LoadHero();
        }
    }
}