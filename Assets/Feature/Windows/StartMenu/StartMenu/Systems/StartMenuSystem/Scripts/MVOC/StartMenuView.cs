﻿using System;
using System.Collections.Generic;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.StartGameWindow.Scripts
{
    public class StartMenuView : AView
    {

        
        [SerializeField]
        private Button loadHero;
        [SerializeField]
        private Button createHero;
        [SerializeField]
        private Button starGame;
        
  
        

        public event Action OnClickStartGame;
        public event Action OnClickLoadHero;

        protected override void Subscribe()
        {
            base.Subscribe();
            starGame.onClick.AddListener(ClickStartGame);
            loadHero.onClick.AddListener(ClickLoadHero);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            
            starGame.onClick.RemoveListener(ClickStartGame);
            loadHero.onClick.RemoveListener(ClickLoadHero);
        }

        public void SetActiveStartButtons(bool isActive)
        {
            starGame.interactable = isActive;
        }


        private void ClickStartGame()
        {
            OnClickStartGame?.Invoke();
        }
        private void ClickLoadHero()
        {
            OnClickLoadHero?.Invoke();
        }

    }
}