﻿using System.Collections.Generic;
using Architecture.MCOV;

namespace DefaultNamespace.AppearanceCreator
{
    public class HeroCreatorModelOld : AModelOld
    {
        public int GetLastHeroId()
        {
            var data = LoadPlayerInfo<PlayerHeroesData>();

            if (data == null || data.ConectedHeroes == null || data.ConectedHeroes.Count == 0)
                return 0;
            
            int maxId = 0;
            
            foreach (var hero in data.ConectedHeroes)
            {
                if (hero >= maxId)
                {
                    maxId = hero;
                }
            }

            return maxId;
        }

        public bool IsGoodName(string newName)
        {
            if (string.IsNullOrEmpty(newName))
                return false;
            
            var data = LoadPlayerInfo<PlayerHeroesData>();

            if (data == null || data.ConectedHeroes == null || data.ConectedHeroes.Count == 0)
                return true;


            foreach (var hero in data.ConectedHeroes)
            {
                var heroInfo = LoadObjectInfo<HeroInfoParameters>(hero);
                if (heroInfo != null)
                {
                    if (heroInfo.HeroName == newName)
                        return false;
                }

            }

            return true;

        }


        public void AddHeroToPlayer(int idHero)
        {
            var data = LoadPlayerInfo<PlayerHeroesData>();

            if (data == null || data.ConectedHeroes == null || data.ConectedHeroes.Count == 0)
            {
                data = new PlayerHeroesData();
                data.ConectedHeroes = new List<int>(); 
            }
            data.ConectedHeroes.Add(idHero);
            
            SavePlayerInfo(data);
            
        }

        public void SaveHeroAppearance(HeroAppearanceData data, int idhero)
        {
            SaveObjectInfo(idhero, data);
        }
        
        public void SaveInfoHero(HeroInfoParameters data, int idhero)
        {
            SaveObjectInfo(idhero, data);
        }
    }
}