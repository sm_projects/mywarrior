﻿using System;

[Serializable]
public class HeroAppearanceData
{
    public int SkinId;
    public int BeardId;
    public int EyeId;
}