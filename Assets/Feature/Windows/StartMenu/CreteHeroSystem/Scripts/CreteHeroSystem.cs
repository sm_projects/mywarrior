﻿using System;
using Architecture.ControllerManager;
using Architecture.DataManager;
using DefaultNamespace;
using DefaultNamespace.AppearanceCreator;
using UnityEngine;

namespace Feature.Windows.StartMenu.CreteHeroSystem
{
    public class CreteHeroSystem : AController
    {
        [SerializeField] private AllPartAssets allPartAssets;


        private int _currentSkinId;
        private int _currentBeardId;
        private int _currentEyeId;
        
        private string _currentName;

        public event Action<bool> OnSetActiveWindow;


        public override void Initialize()
        {
            base.Initialize();
        }

        public void ClickOpenWindow()
        {
            OnSetActiveWindow?.Invoke(true);
        }

        public void ClickCreateHero()
        {
            var world = WorldManager.Manager.CreateNewWorld(_currentName);
            WorldManager.Manager.SelectedWorld(world.WorldId);
            
           // int heroId = heroCreatorModelOld.GetLastHeroId() + 1;

            HeroAppearanceData data = new HeroAppearanceData();
            data.BeardId = _currentBeardId;
            data.SkinId = _currentSkinId;
            data.EyeId = _currentEyeId;
            
            HeroInfoParameters parameters = new HeroInfoParameters();
            parameters.HeroName = _currentName;
            //parameters.HeroId = heroId;
            
            //heroCreatorModelOld.SaveHeroAppearance(data, heroId);
           // heroCreatorModelOld.SaveInfoHero(parameters, heroId);
            //heroCreatorModelOld.AddHeroToPlayer(heroId);
            
            //EventManager.PushEvent(new HeroCreatedEvent(heroId));
            
            OnSetActiveWindow?.Invoke(false);
        }
        
        public void SetHeroName(string newName)
        {
            _currentName = newName;
        }


        public SkinConfig GetDefaultSkin()
        {
            _currentSkinId = allPartAssets.SkinAsset.DefaultPart;
            return allPartAssets.SkinAsset.GetPart<SkinConfig>(_currentSkinId);
        }

        public SkinConfig GetSkin(bool isNext)
        {
            if (isNext)
            {
                _currentSkinId = allPartAssets.SkinAsset.GetNextId(_currentSkinId);
            }
            else
            {
                _currentSkinId = allPartAssets.SkinAsset.GetBeforeId(_currentSkinId);
            }

            return allPartAssets.SkinAsset.GetPart<SkinConfig>(_currentSkinId);
        }

        public EyeConfig GetDefaultEye()
        {
            _currentEyeId = allPartAssets.EyeAsset.DefaultPart;
            return allPartAssets.EyeAsset.GetPart<EyeConfig>(_currentEyeId);
        }

        public EyeConfig GetEye(bool isNext)
        {
            if (isNext)
            {
                _currentEyeId = allPartAssets.EyeAsset.GetNextId(_currentEyeId);
            }
            else
            {
                _currentEyeId = allPartAssets.EyeAsset.GetBeforeId(_currentEyeId);  
            }


            return allPartAssets.EyeAsset.GetPart<EyeConfig>(_currentEyeId);
        }

        public BeardConfig GetDefaultBeard()
        {
            _currentBeardId = allPartAssets.BeardsAsset.DefaultPart;
            return allPartAssets.BeardsAsset.GetPart<BeardConfig>(_currentBeardId);
        }

        public BeardConfig GetBeard(bool isNext)
        {
            
            if (isNext)
            {
                _currentBeardId = allPartAssets.BeardsAsset.GetNextId(_currentBeardId);
            }
            else
            {
                _currentBeardId = allPartAssets.BeardsAsset.GetBeforeId(_currentBeardId);
            }


            return allPartAssets.BeardsAsset.GetPart<BeardConfig>(_currentBeardId);
        }
    }
}