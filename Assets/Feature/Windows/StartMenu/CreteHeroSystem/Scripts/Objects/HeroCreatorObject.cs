﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Appearance;
using Feature.Windows.StartMenu.CreteHeroSystem;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace DefaultNamespace.AppearanceCreator
{
    public class HeroCreatorObject : AObject<CreteHeroSystem>
    {

        [SerializeField] private GameObject panel;

        [SerializeField] private Button closeButton;
        [SerializeField] private Button saveButton;


        [SerializeField] private AppearanceView appearance;
        [SerializeField] private HeroCreatorView heroCreatorView;
        
        

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnSetActiveWindow += SetActiveWindow;

            heroCreatorView.OnClickChangeBeard += ClickChangeBeard;
            heroCreatorView.OnClickChangeEye += ClickChangeEye;
            heroCreatorView.OnClickChangeSkin += ClickChangeSkin;
            heroCreatorView.OnNameChanged += NameChange;

            closeButton.onClick.AddListener(CloseWindow);
            saveButton.onClick.AddListener(SavedHero);
        }

        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnSetActiveWindow -= SetActiveWindow;

            heroCreatorView.OnClickChangeBeard -= ClickChangeBeard;
            heroCreatorView.OnClickChangeEye -= ClickChangeEye;
            heroCreatorView.OnClickChangeSkin -= ClickChangeSkin;
            heroCreatorView.OnNameChanged += NameChange;

            closeButton.onClick.RemoveListener(CloseWindow);
            
            saveButton.onClick.RemoveListener(SavedHero);
        }

        private void SavedHero()
        {
            Controller.ClickCreateHero();
        }

        private void NameChange(string newName)
        {
            bool isActive = !string.IsNullOrEmpty(newName);
            saveButton.interactable = isActive;

            Controller.SetHeroName(newName);
        }

        private void CloseWindow()
        {
            panel.SetActive(false);
        }


        private void SetActiveWindow(bool isActive)
        {
            
            panel.SetActive(isActive);
            if (isActive)
            {
                SetSkin(Controller.GetDefaultSkin());
                SetEye(Controller.GetDefaultEye());
                SetBeard(Controller.GetDefaultBeard());
            }
        }
        
  

        private void ClickChangeSkin(bool isNext)
        {
            var skin = Controller.GetSkin(isNext);
            SetSkin(skin);
        }
        
        
        private void SetSkin(SkinConfig skin)
        {
            appearance.SetSkin(skin);
            heroCreatorView.SetSkin(skin);
        }

        private void ClickChangeEye(bool isNext)
        {
            var eye = Controller.GetEye(isNext);
            SetEye(eye);
        }
        
        private void SetEye(EyeConfig eye)
        {
            appearance.SetEye(eye);
            heroCreatorView.SetEye(eye);
        }
        


        private void ClickChangeBeard(bool isNext)
        {
            var beard = Controller.GetBeard(isNext);
            SetBeard(beard);
        }


        private void SetBeard(BeardConfig beard)
        {
            appearance.SetBeard(beard);
            heroCreatorView.SetBeard(beard);
        }


        
        
    }
}