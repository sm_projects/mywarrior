﻿using System;
using Architecture.MCOV;
using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.AppearanceCreator
{
    public class HeroCreatorView : AView
    {

        [SerializeField]
        private InputField heroName;
        
        [Header("Skin")]
        [SerializeField] private Button nextSkinButton;
        [SerializeField] private Button beforeSkinButton;
        [SerializeField] private Text skinName;
        
        [Header("Eye")]
        [SerializeField] private Button nextEyeButton;
        [SerializeField] private Button beforeEyeButton;
        [SerializeField] private Text eyeName;
        
        [Header("Beard")]
        [SerializeField] private Button nextBeardButton;
        [SerializeField] private Button beforeBeardButton;
        [SerializeField] private Text beardName;

        public event Action<bool> OnClickChangeSkin;
        public event Action<bool> OnClickChangeEye;
        public event Action<bool> OnClickChangeBeard;
        public event Action<string> OnNameChanged;
        
        
        protected override void Subscribe()
        {
            base.Subscribe();
            nextSkinButton.onClick.AddListener(ClickNextSkin);
            beforeSkinButton.onClick.AddListener(ClickBeforeSkin);
            
            nextEyeButton.onClick.AddListener(ClickNextEye);
            beforeEyeButton.onClick.AddListener(ClickBeforeEye);
            
            nextBeardButton.onClick.AddListener(ClickNextBeard);
            beforeBeardButton.onClick.AddListener(ClickBeforeBeard);

            heroName.onValueChanged.AddListener(ChangeName);
        }

        protected override void Unsubscribe()
        {
            base.Unsubscribe();
            nextSkinButton.onClick.RemoveListener(ClickNextSkin);
            beforeSkinButton.onClick.RemoveListener(ClickBeforeSkin);
            
            nextEyeButton.onClick.RemoveListener(ClickNextEye);
            beforeEyeButton.onClick.RemoveListener(ClickBeforeEye);
            
            nextBeardButton.onClick.RemoveListener(ClickNextBeard);
            beforeBeardButton.onClick.RemoveListener(ClickBeforeBeard);
            
            heroName.onValueChanged.RemoveListener(ChangeName);
        }

        public void SetSkin(SkinConfig skin)
        {
            SetText(skinName, skin);
        }
        
        public void SetBeard(BeardConfig beard)
        {
            SetText(beardName, beard);
        }
        
        public void SetEye(EyeConfig eye)
        {
            SetText(eyeName, eye);
        }

        private void SetText(Text textObject, PartConfig part)
        {
            if (part == null)
                textObject.text = "";
            else
                textObject.text = part.NamePart;
        }

        private void ChangeName(string name)
        {
            OnNameChanged?.Invoke(name);
        }

        private void ClickNextSkin()
        {
            OnClickChangeSkin?.Invoke(true);
        }

        private void ClickBeforeSkin()
        {
            OnClickChangeSkin?.Invoke(false);
        }
        
        private void ClickNextEye()
        {
            OnClickChangeEye?.Invoke(true);
        }

        private void ClickBeforeEye()
        {
            OnClickChangeEye?.Invoke(false);
        }
        
        private void ClickNextBeard()
        {
            OnClickChangeBeard?.Invoke(true);
        }

        private void ClickBeforeBeard()
        {
            OnClickChangeBeard?.Invoke(false);
        }
        
        
    }
}