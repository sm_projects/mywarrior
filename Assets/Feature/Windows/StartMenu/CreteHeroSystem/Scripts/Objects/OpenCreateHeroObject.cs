﻿using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.StartMenu.CreteHeroSystem
{
    public class OpenCreateHeroObject : AObject<CreteHeroSystem>
    {
        [SerializeField]
        private Button openWindow;

        public override void Subscribe()
        {
            base.Subscribe();
            openWindow.onClick.AddListener(OpenWindow);
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            openWindow.onClick.RemoveListener(OpenWindow);
        }
        
        private void OpenWindow()
        {
            Controller.ClickOpenWindow();
        }
    }
}