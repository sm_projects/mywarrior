﻿using System;
using System.Collections.Generic;
using Architecture.ControllerManager;
using Architecture.DataManager;
using Architecture.ModelManager;
using Feature.SceneLoader.Scripts;

namespace Feature.Windows.StartMenu.LoadHeroesSystem
{
    public class LoadHeroesSystem : AController, IModelProvider<SceneLoaderModel>
    {
        public event Action<List<WorldData>> OnRepaintHeroes;
        
        public event Action<WorldData> OnChangeSelected;

        private WorldManager WorldManager => WorldManager.Manager;

        private SceneLoaderModel _sceneModel;
        public void SetModel(SceneLoaderModel model)
        {
            _sceneModel = model;
        }
        
        public override void Subscribe()
        {
            base.Subscribe();
            _sceneModel.OnLoadStartScene += StartScene;
            WorldManager.OnChangeWorlds += StartScene;
            WorldManager.OnChangeSelectedWorld += ChangeSelected;
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            _sceneModel.OnLoadStartScene -= StartScene;
            WorldManager.OnChangeWorlds -= StartScene;
            WorldManager.OnChangeSelectedWorld -= ChangeSelected;
        }

        private void ChangeSelected()
        {
            var world = WorldManager.GetSelectedWorld();
            OnChangeSelected?.Invoke(world);
        }
        
        private void StartScene()
        {
            var worlds = WorldManager.GetListWorlds();
            OnRepaintHeroes?.Invoke(worlds);
        }
        
    }
}