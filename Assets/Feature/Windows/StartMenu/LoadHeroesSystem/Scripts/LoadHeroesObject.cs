﻿using System;
using System.Collections.Generic;
using Architecture.DataManager;
using Architecture.ObjectManager;
using Feature.Windows.StartGameWindow.Scripts;
using UnityEngine;

namespace Feature.Windows.StartMenu.LoadHeroesSystem
{
    public class LoadHeroesObject: AObject<LoadHeroesSystem>
    {
        [SerializeField]
        private Transform content;

        [SerializeField]
        private HeroLoadedView prefab;

        private Dictionary<string, HeroLoadedView> _views = new Dictionary<string, HeroLoadedView>();

        public override void Subscribe()
        {
            base.Subscribe();
            Controller.OnRepaintHeroes += RepaintHeroes;
            Controller.OnChangeSelected += RepaintSelected;
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            Controller.OnRepaintHeroes += RepaintHeroes;
            Controller.OnChangeSelected += RepaintSelected;
        }
        
        private void RepaintSelected(WorldData world)
        {
            foreach (var view in _views.Values)
            {
                view.SetSelect(false);
            }
            
            _views[world.WorldId].SetSelect(true);
        }
        
        
        private void RepaintHeroes(List<WorldData> heroes)
        {
            for (int i = 0; i < content.childCount; i++)
            {
                Destroy(content.GetChild(i).gameObject);
            }
            _views.Clear();

            foreach (var hero in heroes)
            {
                var view = Instantiate(prefab, content);
                view.Initialize(hero);
                _views.Add(hero.WorldId, view);
            }
        }
    }
}