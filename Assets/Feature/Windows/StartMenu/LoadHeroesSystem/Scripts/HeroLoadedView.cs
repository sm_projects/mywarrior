﻿using System;
using Architecture.DataManager;
using Architecture.MCOV;
using Architecture.ObjectManager;
using DefaultNamespace.Appearance;
using Feature.Windows.StartMenu.SelectedHeroSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.StartGameWindow.Scripts
{
    public class HeroLoadedView : AView
    {

        [SerializeField]
        private Text nameText;

        [SerializeField]
        private GameObject selectedPanel;

        [SerializeField]
        private SelectHeroButtonObject selectedObject;
        
        

        
        public void Initialize(WorldData hero)
        {
            nameText.text = hero.WorldId;
            
            selectedObject.Initialize(hero.WorldId);
            
            selectedPanel.SetActive(false);

            
        }

        public void SetSelect(bool isSelected)
        {
            selectedPanel.SetActive(isSelected);
        }
        

    }
}