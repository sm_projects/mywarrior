﻿using Architecture.ControllerManager;
using Architecture.DataManager;

namespace Feature.Windows.StartMenu.SelectedHeroSystem
{
    public class SelectedHeroSystem : AController
    {
        private WorldManager WorldManager => WorldManager.Manager;
        
        public void ClickSelect(string keyHero)
        {
            WorldManager.SelectedWorld(keyHero);
        }
    }
}