﻿using Architecture.ObjectManager;
using UnityEngine;
using UnityEngine.UI;

namespace Feature.Windows.StartMenu.SelectedHeroSystem
{
    public class SelectHeroButtonObject : AObject<SelectedHeroSystem>
    {
        [SerializeField]
        private Button selectButton;

        private string _keyHero;

        
        public override void Subscribe()
        {
            base.Subscribe();
            selectButton.onClick.AddListener(ClickSelect);
        }



        public override void Unsubscribe()
        {
            base.Unsubscribe();
            selectButton.onClick.RemoveListener(ClickSelect);
        }

       
        public void Initialize(string keyHero)
        {
            _keyHero = keyHero;
        }
        
        private void ClickSelect()
        {
            Controller.ClickSelect(_keyHero);
        }
    }
}