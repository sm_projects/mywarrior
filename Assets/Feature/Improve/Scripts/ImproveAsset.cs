﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Feature.Windows.Improve.Scripts.Configs
{
    [CreateAssetMenu(menuName = "Feature/Improve/ImproveAsset")]
    public class ImproveAsset : ScriptableObject
    {
        public List<ImproveInfo> ListImproveInfo;
    }

    [Serializable]
    public class ImproveInfo
    {
        public int ItemConfigId;
        public int ItemResultConfigId;
        public List<int> ListItemsForImprove;
        public List<int> ListCurrencyForImprove;
    }
}