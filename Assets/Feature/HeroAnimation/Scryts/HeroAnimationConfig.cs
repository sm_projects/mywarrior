﻿using System;
using System.Collections.Generic;

[Serializable]
public class HeroAnimationConfig
{
    public string NameConfig;
    public List<string> BoolActive;
    public List<string> BoolNoActive;
}