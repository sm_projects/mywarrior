﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/HeroAnimation/HeroAnimationAssets")]
public class HeroAnimationAssets : ScriptableObject
{
    public List<HeroAnimationConfig> HeroAnimations;

    public string NameMoveForward;
    public string NameMoveBack;

    public HeroAnimationConfig GetAnimationConfig(string nameConfig)
    {
        foreach (var config in HeroAnimations)
        {
            if (config.NameConfig == nameConfig)
                return config;
        }

        return null;
    }
}