﻿using System;
using Feature.Fight.Systems.FightCollisionSystem.SkillCollision;

[Serializable]
public class SkillCollisionConfig
{
    public string SkillName;
    public SkillCollisionView Prefab;
}