﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/SkillCollision/SkillCollisionAssets")]
public class SkillCollisionAssets : ScriptableObject
{
    public List<SkillCollisionConfig> Configs;

    public SkillCollisionConfig Default;
    public SkillCollisionConfig GetConfig(string skillName)
    {
        foreach (var config in Configs)
        {
            if (config.SkillName == skillName)
                return config;
        }

        return Default;
    }
}