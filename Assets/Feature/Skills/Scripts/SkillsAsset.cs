﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Feature.Skills.Scripts
{
    [CreateAssetMenu(menuName = "Feature/Skill/SkillsAsset")]
    public class SkillsAsset: ScriptableObject
    {
        public List<SkillConfig> ListSkills;

        public SkillConfig MoveLeft;

        public SkillConfig MoveRght;
        
        
        public SkillConfig GetSkill(string nameSkill)
        {
            foreach (var skillConfig in ListSkills)
            {
                if (skillConfig.SkillName == nameSkill)
                    return skillConfig;
            }

            return null;
        }
    }

    
    [Serializable]
    public class SkillConfig
    {
        public string SkillName;
        public string SkillNameKey;
        public string SkillDescriptionKey;
        public Sprite Icon;

        public bool IsReadyInStart;
        public float TimeToReady = 5;

        
        public List<string> BlockedEffectNames;
        public List<string> DownEffectNames;
        public List<string> UpEffectNames;
        
        
        public bool IsYourself;

        public PositionSkillView PositionSkillView;
    }

    public enum PositionSkillView
    {
        Move,
        MainSkills,
        PetSkills
    }
}