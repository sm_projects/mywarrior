﻿using System.Collections.Generic;
using Architecture.AssetsManager;
using UnityEngine;

[CreateAssetMenu(menuName = "Feature/Component/ComponentAssets")]
public class ComponentAssets : AViewAsset
{
    public List<ComponentConfig> DefaultComponents;
    
    public ComponentConfig GetComponentConfig(Component component)
    {
        foreach (var config in DefaultComponents)
        {
            if (config.Component == component)
                return config;
        }

        return null;
    }

}