﻿using System;
using UnityEngine.Serialization;

[Serializable]
public class ComponentConfig
{
    public Component Component;

    public float DefaultValue;

    public StartValue StartValue;

    public string KeyName;
}

public enum Component
{
    Velocity,
    Speed,
    Health, 
    DamageAdded,

}

public enum StartValue
{
    Zero,
    MathValue,
    DefaultValue
}