﻿using System;
using UnityEngine.Serialization;
using Utilits;

namespace Feature.Stats.Scripts
{
    [Serializable]
    public class ComponentModificator
    {
        public Component Component;
        public MathType MathType;
        public float Value;
    }
}