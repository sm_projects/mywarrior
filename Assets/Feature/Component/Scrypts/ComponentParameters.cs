﻿using System;

[Serializable]
public class ComponentParameters
{
    public event Action<ComponentParameters> OnChangeValue; 

    public ComponentConfig ComponentConfig;

    public float Value;
    
    public void ChangeComponent()
    {
        OnChangeValue?.Invoke(this);
    }



}